<?php

namespace SoukTel\Knowledge;

class Tag
{
    /**
     * $knowledge object.
     */
    protected $knowledge;
    /**
     * $tag object.
     */
    protected $tag;

    /**
     * Tag constructor.
     * @param Interfaces\KnowledgeRepositoryInterface $knowledge
     * @param Interfaces\TagRepositoryInterface $tag
     */
    public function __construct(\SoukTel\Knowledge\Interfaces\KnowledgeRepositoryInterface $knowledge,
                                \SoukTel\Knowledge\Interfaces\TagRepositoryInterface $tag)
    {
        $this->knowledge = $knowledge;
        $this->tag = $tag;
    }

    public function create(Array $attributes)
    {
        return $this->tag->create($attributes);
    }

    /**
     * Returns count of knowledge.
     *
     * @param array $filter
     *
     * @return int
     */
    public function count($type)
    {
        $knowledges = $this->$type->all();
        return count($knowledges);
    }

    /**
     * Returns tag of knowledge.
     * @return array
     */
    public function getTag()
    {
        $array = [];
        $tags = $this->tag->scopeQuery(function ($query) {
            return $query->orderBy('name')->whereStatus('show');
        })->all();

        foreach ($tags as $key => $tag) {
            $array[$tag['id']] = ucfirst($tag['name']);
        }

        return $array;

    }

    /**
     * @return string
     */
    public function viewTags()
    {
        $this->knowledge->pushCriteria(new \SoukTel\Knowledge\Repositories\Criteria\TagPublicCriteria());
        $tags = $this->tag->scopeQuery(function ($query) {
            return $query->orderBy('name');
        })->all();

        return view('knowledge::public.tag.index', compact('tags'))->render();
    }

    /**
     * get related knowledges
     * @param tag_id
     * @return array
     */
    public function getRelated($cid)
    {
        $this->knowledge->pushCriteria(new \SoukTel\Knowledge\Repositories\Criteria\KnowledgePublicCriteria());
        $knowledges = $this->knowledge->scopeQuery(function ($query) use ($cid) {
            return $query->orderBy('id', 'DESC')->whereTag_id($cid)->take(2);
        })->all();

        return $knowledges;
    }

    /**
     * get related knowledges
     * @param tag_id
     * @return array
     */
    public function recentKnowledge()
    {
        $this->knowledge->pushCriteria(new \SoukTel\Knowledge\Repositories\Criteria\KnowledgePublicCriteria());
        $knowledges = $this->knowledge->scopeQuery(function ($query) {
            return $query->orderBy('id', 'DESC')->take(5);
        })->all();

        return $knowledges;
    }

    /**
     * get related knowledges
     * @param tag_id
     * @return array
     */
    public function getCount($cid)
    {
        $this->knowledge->pushCriteria(new \SoukTel\Knowledge\Repositories\Criteria\KnowledgePublicCriteria());
        $knowledges = $this->knowledge->scopeQuery(function ($query) use ($cid) {
            return $query->orderBy('id', 'DESC')->whereTag_id($cid);
        })->all();

        return count($knowledges);
    }

    /**
     * Returns count of knowledge.
     *
     * @param array $filter
     *
     * @return int
     */
    public function countKnowledges()
    {
        $this->knowledge->pushCriteria(new \SoukTel\Knowledge\Repositories\Criteria\KnowledgePublicCriteria());
        $knowledges = $this->knowledge->scopeQuery(function ($query) {
            return $query->whereStatus('show');
        })->all();
        return count($knowledges);
    }

}
