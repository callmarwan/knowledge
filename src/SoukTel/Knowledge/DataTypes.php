<?php

namespace SoukTel\Knowledge;

class DataTypes
{
    /**
     * $knowledge object.
     */
    protected $knowledge;
    /**
     * $country object.
     */
    protected $data_types;

    /**
     * Constructor.
     */
    public function __construct(\SoukTel\Knowledge\Interfaces\KnowledgeRepositoryInterface $knowledge,
                                \SoukTel\Knowledge\Interfaces\DataTypeRepositoryInterface $data_types)
    {
        $this->knowledge = $knowledge;
        $this->data_types = $data_types;
		//
    }
}
