<?php

namespace SoukTel\Knowledge;

class Document
{
    /**
     * $knowledge object.
     */
    protected $knowledge;
    /**
     * $document object.
     */
    protected $document;

    /**
     * Constructor.
     */
    public function __construct(\SoukTel\Knowledge\Interfaces\KnowledgeRepositoryInterface $knowledge,
                                \SoukTel\Knowledge\Interfaces\DocumentRepositoryInterface $document)
    {
        $this->knowledge = $knowledge;
        $this->document = $document;
    }

    public function log_download(\SoukTel\Knowledge\Models\Document $document)
    {
        $activity = activityLog($document->knowledge, 'activity.download', 'download');

        $activity->meta()->createMany([
            [
                'meta_key' => 'knowledge',
                'meta_value' => $document->knowledge->id,
            ],
            [
                'meta_key' => 'locale',
                'meta_value' => $document->locale,
            ],
            [
                'meta_key' => 'document',
                'meta_value' => $document->original_name,
            ],
            [
                'meta_key' => 'knowledge_title',
                'meta_value' => $document->knowledge->title,
            ],
        ]);
    }
}
