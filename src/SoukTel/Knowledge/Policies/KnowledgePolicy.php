<?php

namespace SoukTel\Knowledge\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use SoukTel\Knowledge\Models\Knowledge;

class KnowledgePolicy
{
    use HandlesAuthorization;

    /**
     * Determine if the given user can view the knowledge.
     *
     * @param User $user
     * @param Knowledge $knowledge
     *
     * @return bool
     */
    public function view(User $user, Knowledge $knowledge)
    {
        if ($user->canDo('knowledge.knowledge.view') && $user->is('admin')) {
            return true;
        }

        return true;//$user->id === $knowledge->user_id;
    }

    /**
     * Determine if the given user can create a knowledge.
     *
     * @param User $user
     * @param Knowledge $knowledge
     *
     * @return bool
     */
    public function create(User $user)
    {
        return  $user->canDo('knowledge.knowledge.create');
    }

    /**
     * Determine if the given user can update the given knowledge.
     *
     * @param User $user
     * @param Knowledge $knowledge
     *
     * @return bool
     */
    public function update(User $user, Knowledge $knowledge)
    {
        if ($user->canDo('knowledge.knowledge.update') && $user->is('admin')) {
            return true;
        }

        return true;//$user->id === $knowledge->user_id;
    }

    /**
     * Determine if the given user can delete the given knowledge.
     *
     * @param User $user
     * @param Knowledge $knowledge
     *
     * @return bool
     */
    public function destroy(User $user, Knowledge $knowledge)
    {
        if ($user->canDo('knowledge.knowledge.delete') && $user->is('admin')) {
            return true;
        }

        return true;//$user->id === $knowledge->user_id;
    }

    /**
     * Determine if the user can perform a given action ve.
     *
     * @param [type] $user    [description]
     * @param [type] $ability [description]
     *
     * @return [type] [description]
     */
    public function before($user, $ability)
    {
        if ($user->isSuperUser()) {
            return true;
        }
    }
}
