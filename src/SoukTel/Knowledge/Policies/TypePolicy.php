<?php

namespace SoukTel\Knowledge\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use SoukTel\Knowledge\Models\Type;

class TypePolicy
{
    use HandlesAuthorization;

    /**
     * Determine if the given user can view the type.
     *
     * @param User $user
     * @param Type $type
     *
     * @return bool
     */
    public function view(User $user, Type $type)
    {
        if ($user->canDo('knowledge.type.view') && $user->is('admin')) {
            return true;
        }

        return true;//$user->id === $type->user_id;
    }

    /**
     * Determine if the given user can create a type.
     *
     * @param User $user
     * @param Type $type
     *
     * @return bool
     */
    public function create(User $user)
    {
        return $user->canDo('knowledge.type.create');
    }

    /**
     * Determine if the given user can update the given type.
     *
     * @param User $user
     * @param Type $type
     *
     * @return bool
     */
    public function update(User $user, Type $type)
    {
        if ($user->canDo('knowledge.type.update') && $user->is('admin')) {
            return true;
        }

        return true;//$user->id === $type->user_id;
    }

    /**
     * Determine if the given user can delete the given type.
     *
     * @param User $user
     * @param Type $type
     *
     * @return bool
     */
    public function destroy(User $user, Type $type)
    {
        if ($user->canDo('knowledge.type.delete') && $user->is('admin')) {
            return true;
        }

        return true;//$user->id === $type->user_id;
    }

    /**
     * Determine if the user can perform a given action ve.
     *
     * @param [type] $user    [description]
     * @param [type] $ability [description]
     *
     * @return [type] [description]
     */
    public function before($user, $ability)
    {
        if ($user->isSuperUser()) {
            return true;
        }
    }
}
