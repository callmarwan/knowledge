<?php

namespace SoukTel\Knowledge\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use SoukTel\Knowledge\Models\DataType;

class DataTypePolicy
{
    use HandlesAuthorization;

    /**
     * Determine if the given user can view the data_type.
     *
     * @param User $user
     * @param DataType $data_type
     *
     * @return bool
     */
    public function view(User $user, DataType $data_type)
    {
        if ($user->canDo('knowledge.data_type.view') && $user->is('admin')) {
            return true;
        }

        return true;// $user->id === $data_type->user_id;
    }

    /**
     * Determine if the given user can create a data_type.
     *
     * @param User $user
     * @param DataType $data_type
     *
     * @return bool
     */
    public function create(User $user)
    {
        return $user->canDo('knowledge.data_type.create');
    }

    /**
     * Determine if the given user can update the given data_type.
     *
     * @param User $user
     * @param DataType $data_type
     *
     * @return bool
     */
    public function update(User $user, DataType $data_type)
    {
        if ($user->canDo('knowledge.data_type.update') && $user->is('admin')) {
            return true;
        }

        return true;// $user->id === $data_type->user_id;
    }

    /**
     * Determine if the given user can delete the given data_type.
     *
     * @param User $user
     * @param DataType $data_type
     *
     * @return bool
     */
    public function destroy(User $user, DataType $data_type)
    {
        if ($user->canDo('knowledge.data_type.delete') && $user->is('admin')) {
            return true;
        }

        return true;// $user->id === $data_type->user_id;
    }

    /**
     * Determine if the user can perform a given action ve.
     *
     * @param [type] $user    [description]
     * @param [type] $ability [description]
     *
     * @return [type] [description]
     */
    public function before($user, $ability)
    {
        if ($user->isSuperUser()) {
            return true;
        }
    }
}
