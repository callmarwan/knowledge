<?php

namespace SoukTel\Knowledge\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use SoukTel\Knowledge\Models\Tag;

class TagPolicy
{
    use HandlesAuthorization;

    /**
     * Determine if the given user can view the tag.
     *
     * @param User $user
     * @param Tag $tag
     *
     * @return bool
     */
    public function view(User $user, Tag $tag)
    {
        if ($user->canDo('knowledge.tag.view') && $user->is('admin')) {
            return true;
        }

        return true;//$user->id === $tag->user_id;
    }

    /**
     * Determine if the given user can create a tag.
     *
     * @param User $user
     * @param Tag $tag
     *
     * @return bool
     */
    public function create(User $user)
    {
        return $user->canDo('knowledge.tag.create');
    }

    /**
     * Determine if the given user can update the given tag.
     *
     * @param User $user
     * @param Tag $tag
     *
     * @return bool
     */
    public function update(User $user, Tag $tag)
    {
        if ($user->canDo('knowledge.tag.update') && $user->is('admin')) {
            return true;
        }

        return true;//$user->id === $tag->user_id;
    }

    /**
     * Determine if the given user can delete the given tag.
     *
     * @param User $user
     * @param Tag $tag
     *
     * @return bool
     */
    public function destroy(User $user, Tag $tag)
    {
        if ($user->canDo('knowledge.tag.delete') && $user->is('admin')) {
            return true;
        }

        return true;//$user->id === $tag->user_id;
    }

    /**
     * Determine if the user can perform a given action ve.
     *
     * @param [type] $user    [description]
     * @param [type] $ability [description]
     *
     * @return [type] [description]
     */
    public function before($user, $ability)
    {
        if ($user->isSuperUser()) {
            return true;
        }
    }
}
