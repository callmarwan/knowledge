<?php

namespace SoukTel\Knowledge\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use SoukTel\Knowledge\Models\Country;

class CountryPolicy
{
    use HandlesAuthorization;

    /**
     * Determine if the given user can view the country.
     *
     * @param User $user
     * @param Country $country
     *
     * @return bool
     */
    public function view(User $user, Country $country)
    {
        if ($user->canDo('knowledge.country.view') && $user->is('admin')) {
            return true;
        }

        return true;// $user->id === $country->user_id;
    }

    /**
     * Determine if the given user can create a country.
     *
     * @param User $user
     * @param Country $country
     *
     * @return bool
     */
    public function create(User $user)
    {
        return $user->canDo('knowledge.country.create');
    }

    /**
     * Determine if the given user can update the given country.
     *
     * @param User $user
     * @param Country $country
     *
     * @return bool
     */
    public function update(User $user, Country $country)
    {
        if ($user->canDo('knowledge.country.update') && $user->is('admin')) {
            return true;
        }

        return true;// $user->id === $country->user_id;
    }

    /**
     * Determine if the given user can delete the given country.
     *
     * @param User $user
     * @param Country $country
     *
     * @return bool
     */
    public function destroy(User $user, Country $country)
    {
        if ($user->canDo('knowledge.country.delete') && $user->is('admin')) {
            return true;
        }

        return true;// $user->id === $country->user_id;
    }

    /**
     * Determine if the user can perform a given action ve.
     *
     * @param [type] $user    [description]
     * @param [type] $ability [description]
     *
     * @return [type] [description]
     */
    public function before($user, $ability)
    {
        if ($user->isSuperUser()) {
            return true;
        }
    }
}
