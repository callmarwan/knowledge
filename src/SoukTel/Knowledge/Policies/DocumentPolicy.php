<?php

namespace SoukTel\Knowledge\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use SoukTel\Knowledge\Models\Document;

class DocumentPolicy
{
    use HandlesAuthorization;

    /**
     * Determine if the given user can view the document.
     *
     * @param User $user
     * @param Document $document
     *
     * @return bool
     */
    public function view(User $user, Document $document)
    {
        if ($user->canDo('knowledge.document.view') && $user->is('admin')) {
            return true;
        }

        return true;// $user->id === $document->user_id;
    }

    /**
     * Determine if the given user can create a document.
     *
     * @param User $user
     * @param Document $document
     *
     * @return bool
     */
    public function create(User $user)
    {
        return $user->canDo('knowledge.document.create');
    }

    /**
     * Determine if the given user can update the given document.
     *
     * @param User $user
     * @param Document $document
     *
     * @return bool
     */
    public function update(User $user, Document $document)
    {
        if ($user->canDo('knowledge.document.update') && $user->is('admin')) {
            return true;
        }

        return true;// $user->id === $document->user_id;
    }

    /**
     * Determine if the given user can delete the given document.
     *
     * @param User $user
     * @param Document $document
     *
     * @return bool
     */
    public function destroy(User $user, Document $document)
    {
        if ($user->canDo('knowledge.document.delete') && $user->is('admin')) {
            return true;
        }

        return true;// $user->id === $document->user_id;
    }

    /**
     * Determine if the user can perform a given action ve.
     *
     * @param [type] $user    [description]
     * @param [type] $ability [description]
     *
     * @return [type] [description]
     */
    public function before($user, $ability)
    {
        if ($user->isSuperUser()) {
            return true;
        }
    }
}
