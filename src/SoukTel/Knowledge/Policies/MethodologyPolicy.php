<?php

namespace SoukTel\Knowledge\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use SoukTel\Knowledge\Models\Methodology;

class MethodologyPolicy
{
    use HandlesAuthorization;

    /**
     * Determine if the given user can view the methodology.
     *
     * @param User $user
     * @param Methodology $methodology
     *
     * @return bool
     */
    public function view(User $user, Methodology $methodology)
    {
        if ($user->canDo('knowledge.methodology.view') && $user->is('admin')) {
            return true;
        }

        return true;// $user->id === $methodology->user_id;
    }

    /**
     * Determine if the given user can create a methodology.
     *
     * @param User $user
     * @param Methodology $methodology
     *
     * @return bool
     */
    public function create(User $user)
    {
        return $user->canDo('knowledge.methodology.create');
    }

    /**
     * Determine if the given user can update the given methodology.
     *
     * @param User $user
     * @param Methodology $methodology
     *
     * @return bool
     */
    public function update(User $user, Methodology $methodology)
    {
        if ($user->canDo('knowledge.methodology.update') && $user->is('admin')) {
            return true;
        }

        return true;// $user->id === $methodology->user_id;
    }

    /**
     * Determine if the given user can delete the given methodology.
     *
     * @param User $user
     * @param Methodology $methodology
     *
     * @return bool
     */
    public function destroy(User $user, Methodology $methodology)
    {
        if ($user->canDo('knowledge.methodology.delete') && $user->is('admin')) {
            return true;
        }

        return true;// $user->id === $methodology->user_id;
    }

    /**
     * Determine if the user can perform a given action ve.
     *
     * @param [type] $user    [description]
     * @param [type] $ability [description]
     *
     * @return [type] [description]
     */
    public function before($user, $ability)
    {
        if ($user->isSuperUser()) {
            return true;
        }
    }
}
