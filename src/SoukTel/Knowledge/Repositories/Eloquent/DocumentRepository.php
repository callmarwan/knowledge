<?php

namespace SoukTel\Knowledge\Repositories\Eloquent;

use SoukTel\Knowledge\Interfaces\DocumentRepositoryInterface;
use Litepie\Repository\Eloquent\BaseRepository;

class DocumentRepository extends BaseRepository implements DocumentRepositoryInterface
{
    /**
     * Booting the repository.
     *
     * @return null
     */
    public function boot()
    {
        $this->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'));
    }

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        $this->fieldSearchable = config('package.knowledge.document.search');
        return config('package.knowledge.document.model');
    }
}
