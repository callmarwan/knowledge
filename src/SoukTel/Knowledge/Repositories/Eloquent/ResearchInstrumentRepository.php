<?php

namespace SoukTel\Knowledge\Repositories\Eloquent;

use SoukTel\Knowledge\Interfaces\ResearchInstrumentRepositoryInterface;
use SoukTel\Knowledge\Interfaces\TagRepositoryInterface;
use Litepie\Repository\Eloquent\BaseRepository;

class ResearchInstrumentRepository extends BaseRepository implements ResearchInstrumentRepositoryInterface
{
    /**
     * Booting the repository.
     *
     * @return null
     */
    public function boot()
    {
        $this->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'));
    }

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        $this->fieldSearchable = config('package.knowledge.research_instrument.search');
        return config('package.knowledge.research_instrument.model');
    }
}
