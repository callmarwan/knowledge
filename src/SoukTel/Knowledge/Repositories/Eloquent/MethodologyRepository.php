<?php

namespace SoukTel\Knowledge\Repositories\Eloquent;

use SoukTel\Knowledge\Interfaces\MethodologyRepositoryInterface;
use Litepie\Repository\Eloquent\BaseRepository;

class MethodologyRepository extends BaseRepository implements MethodologyRepositoryInterface
{
    /**
     * Booting the repository.
     *
     * @return null
     */
    public function boot()
    {
        $this->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'));
    }

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        $this->fieldSearchable = config('package.knowledge.methodology.search');
        return config('package.knowledge.methodology.model');
    }
}
