<?php

namespace SoukTel\Knowledge\Repositories\Eloquent;

use SoukTel\Knowledge\Interfaces\KnowledgeRepositoryInterface;
use Litepie\Repository\Eloquent\BaseRepository;

class KnowledgeRepository extends BaseRepository implements KnowledgeRepositoryInterface
{
    /**
     * Booting the repository.
     *
     * @return null
     */
    public function boot()
    {
        $this->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'));
    }

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        $this->fieldSearchable = config('package.knowledge.knowledge.search');
        return config('package.knowledge.knowledge.model');
    }
}
