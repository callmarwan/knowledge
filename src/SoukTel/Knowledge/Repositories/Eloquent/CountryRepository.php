<?php

namespace SoukTel\Knowledge\Repositories\Eloquent;

use SoukTel\Knowledge\Interfaces\CountryRepositoryInterface;
use Litepie\Repository\Eloquent\BaseRepository;

class CountryRepository extends BaseRepository implements CountryRepositoryInterface
{
    /**
     * Booting the repository.
     *
     * @return null
     */
    public function boot()
    {
        $this->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'));
    }

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        $this->fieldSearchable = config('package.knowledge.country.search');
        return config('package.knowledge.country.model');
    }
}
