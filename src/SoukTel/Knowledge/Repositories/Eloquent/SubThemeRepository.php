<?php

namespace SoukTel\Knowledge\Repositories\Eloquent;

use Litepie\Repository\Eloquent\BaseRepository;
use SoukTel\Knowledge\Interfaces\SubThemeRepositoryInterface;

class SubThemeRepository extends BaseRepository implements SubThemeRepositoryInterface
{
    /**
     * Booting the repository.
     *
     * @return null
     */
    public function boot()
    {
        $this->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'));
    }

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        $this->fieldSearchable = config('package.knowledge.sub_theme.search');
        return config('package.knowledge.sub_theme.model');
    }
}
