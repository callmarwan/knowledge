<?php

namespace SoukTel\Knowledge\Repositories\Eloquent;

use SoukTel\Knowledge\Interfaces\DataTypeRepositoryInterface;
use Litepie\Repository\Eloquent\BaseRepository;

class DataTypeRepository extends BaseRepository implements DataTypeRepositoryInterface
{
    /**
     * Booting the repository.
     *
     * @return null
     */
    public function boot()
    {
        $this->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'));
    }

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        $this->fieldSearchable = config('package.knowledge.data_type.search');
        return config('package.knowledge.data_type.model');
    }
}
