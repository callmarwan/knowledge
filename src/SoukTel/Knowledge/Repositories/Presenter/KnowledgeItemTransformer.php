<?php

namespace SoukTel\Knowledge\Repositories\Presenter;

use League\Fractal\TransformerAbstract;

class KnowledgeItemTransformer extends TransformerAbstract
{
    public function transform(\SoukTel\Knowledge\Models\Knowledge $knowledge)
    {
        return [
            'id' => $knowledge->getRouteKey(),
            'title' => $knowledge->title,
            'published' => ($knowledge->published == 1) ? trans('cms.published') : trans('cms.unpublished'),
            'published_at' => $knowledge->getOriginal('published_at') ?: '-',
            'description' => $knowledge->description,
            'author' => $knowledge->author,
            'status' => $knowledge->status ?: '-',
            'slug' => $knowledge->slug,
        ];
    }
}
