<?php

namespace SoukTel\Knowledge\Repositories\Presenter;

use League\Fractal\TransformerAbstract;
use Hashids;

class ResearchInstrumentItemTransformer extends TransformerAbstract
{
    public function transform(\SoukTel\Knowledge\Models\ResearchInstrument $researchInstrument)
    {
        return [
            'id' => $researchInstrument->getRouteKey(),
            'name' => ucfirst($researchInstrument->name),
            'status' => ucfirst($researchInstrument->status),
            'slug' => $researchInstrument->slug,
        ];
    }
}