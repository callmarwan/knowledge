<?php

namespace SoukTel\Knowledge\Repositories\Presenter;

use League\Fractal\TransformerAbstract;

class KnowledgeListTransformer extends TransformerAbstract
{
    public function transform(\SoukTel\Knowledge\Models\Knowledge $knowledge)
    {
        $statuses = trans('knowledge.status_options');
        return [
            'id' => $knowledge->getRouteKey(),
            'title' => str_limit(strip_tags($knowledge->title), 50),
            'published' => ($knowledge->published == 1) ? trans('cms.published') : trans('cms.unpublished'),
            'published_at' => $knowledge->getOriginal('published_at') ?: '-',
            'created_at' => $knowledge->getOriginal('created_at'),
            'description' => $knowledge->description,
            'author_id' => $knowledge->author->name, //TODO add this again
            'status' => isset($statuses[$knowledge->getOriginal('status')]) ? $statuses[$knowledge->getOriginal('status')] : '-',
            'type' => $knowledge->type,
            'research_date' => format_date($knowledge->research_date,'mm/yyyy'),
            'slug' => $knowledge->slug,
            'categories' => $this->getCategories($knowledge),
            'tags' => $this->getTags($knowledge),
            'research' => $knowledge->research ? '<i class="fa fa-check text-success"><span class="hidden">1</span></i>' : '-<span class="hidden">0</span>',
        ];
    }

    private function getCategories($knowledge)
    {
        $temp = '';
        $list = $knowledge->categories;

        foreach ($list as $value) {
            $temp .= '<span class="label label-primary" style="margin-right: 2px;">' . ucfirst($value->name) . '</span>';
        }

        return $temp;
    }

    private function getTags($knowledge)
    {
        $temp = '';
        $list = $knowledge->tags;

        if (sizeof($list) <= 0)
            return '-';
        foreach ($list as $value) {
            $temp .= '<span class="label label-info" style="margin-right: 2px;">' . ucfirst($value->name) . '</span>';
        }
        return $temp;
    }
}
