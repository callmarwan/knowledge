<?php

namespace SoukTel\Knowledge\Repositories\Presenter;

use League\Fractal\TransformerAbstract;
use Hashids;

class SubThemeItemTransformer extends TransformerAbstract
{
    public function transform(\SoukTel\Knowledge\Models\SubTheme $sub_theme)
    {
        return [
            'id' => $sub_theme->getRouteKey(),
            'name' => ucfirst($sub_theme->name),
            'status' => ucfirst($sub_theme->status),
            'slug' => $sub_theme->slug,
        ];
    }
}