<?php

namespace SoukTel\Knowledge\Repositories\Presenter;

use League\Fractal\TransformerAbstract;

class TagListTransformer extends TransformerAbstract
{
    public function transform(\SoukTel\Knowledge\Models\Tag $tag)
    {
        return [
            'id' => $tag->getRouteKey(),
            'name' => ucfirst($tag->name),
            'status' => ucfirst($tag->status),
            'slug' => $tag->slug,
        ];
    }
}
