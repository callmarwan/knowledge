<?php

namespace SoukTel\Knowledge\Repositories\Presenter;

use League\Fractal\TransformerAbstract;
use Hashids;

class CategoryItemTransformer extends TransformerAbstract
{
    public function transform(\SoukTel\Knowledge\Models\Category $category)
    {
        return [
            'id' => $category->getRouteKey(),
            'name' => ucfirst($category->name),
            'status' => ucfirst($category->status),
            'slug' => $category->slug,
        ];
    }
}