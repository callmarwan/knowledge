<?php

namespace SoukTel\Knowledge\Repositories\Presenter;

use Litepie\Repository\Presenter\FractalPresenter;

class ResearchInstrumentListPresenter extends FractalPresenter {

    /**
     * Prepare data to present
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ResearchInstrumentListTransformer();
    }
}