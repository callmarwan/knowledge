<?php

namespace SoukTel\Knowledge\Repositories\Presenter;

use League\Fractal\TransformerAbstract;
use Hashids;

class DocumentItemTransformer extends TransformerAbstract
{
    public function transform(\SoukTel\Knowledge\Models\Document $document)
    {
        return [
            'id' => $document->getRouteKey(),
        ];
    }
}