<?php

namespace SoukTel\Knowledge\Repositories\Presenter;

use League\Fractal\TransformerAbstract;

class DataTypeListTransformer extends TransformerAbstract
{
    public function transform(\SoukTel\Knowledge\Models\DataType $dataType)
    {
        return [
            'id' => $dataType->getRouteKey(),
            'name' => $dataType->name,
        ];
    }
}
