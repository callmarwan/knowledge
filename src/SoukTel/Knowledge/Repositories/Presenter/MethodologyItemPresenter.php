<?php

namespace SoukTel\Knowledge\Repositories\Presenter;

use Litepie\Repository\Presenter\FractalPresenter;

class MethodologyItemPresenter extends FractalPresenter
{

    /**
     * Prepare data to present
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new MethodologyItemTransformer();
    }
}