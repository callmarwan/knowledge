<?php

namespace SoukTel\Knowledge\Repositories\Presenter;

use League\Fractal\TransformerAbstract;

class MethodologyListTransformer extends TransformerAbstract
{
    public function transform(\SoukTel\Knowledge\Models\Methodology $methodology)
    {
        return [
            'id' => $methodology->getRouteKey(),
            'name' => $methodology->name,
        ];
    }
}
