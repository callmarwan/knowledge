<?php

namespace SoukTel\Knowledge\Repositories\Presenter;

use League\Fractal\TransformerAbstract;

class DocumentListTransformer extends TransformerAbstract
{
    public function transform(\SoukTel\Knowledge\Models\Document $document)
    {
        return [
            'id' => $document->getRouteKey(),
        ];
    }
}
