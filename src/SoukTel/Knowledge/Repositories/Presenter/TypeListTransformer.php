<?php

namespace SoukTel\Knowledge\Repositories\Presenter;

use League\Fractal\TransformerAbstract;

class TypeListTransformer extends TransformerAbstract
{
    public function transform(\SoukTel\Knowledge\Models\Type $type)
    {
        return [
            'id' => $type->getRouteKey(),
            'name' => $type->name,
            'slug' => $type->slug,
            'status' => $type->status,
        ];
    }
}
