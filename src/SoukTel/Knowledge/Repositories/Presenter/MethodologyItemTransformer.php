<?php

namespace SoukTel\Knowledge\Repositories\Presenter;

use League\Fractal\TransformerAbstract;
use Hashids;

class MethodologyItemTransformer extends TransformerAbstract
{
    public function transform(\SoukTel\Knowledge\Models\Methodology $methodology)
    {
        return [
            'id' => $methodology->getRouteKey(),
            'name' => $methodology->name,
        ];
    }
}