<?php

namespace SoukTel\Knowledge\Repositories\Presenter;

use League\Fractal\TransformerAbstract;

class CategoryListTransformer extends TransformerAbstract
{
    public function transform(\SoukTel\Knowledge\Models\Category $category)
    {
        $statuses = trans('knowledge_category.status_options');
        return [
            'id' => $category->getRouteKey(),
            'name' => ucfirst($category->name),
            'status' => isset($statuses[$category->getOriginal('status')]) ? $statuses[$category->getOriginal('status')] : '-',
            'slug' => $category->slug,
            'in_research' => $category->in_research == 1 ? '<i class="fa fa-check text-success"><span class="hidden">1</span></i>' : '-<span class="hidden">0</span>',
            'in_document' => $category->in_document == 1 ? '<i class="fa fa-check text-success"><span class="hidden">1</span></i>' : '-<span class="hidden">0</span>',
        ];
    }
}
