<?php

namespace SoukTel\Knowledge\Repositories\Presenter;

use League\Fractal\TransformerAbstract;

class SubThemeListTransformer extends TransformerAbstract
{
    public function transform(\SoukTel\Knowledge\Models\SubTheme $sub_theme)
    {
        $statuses = trans('knowledge_category.status_options');
        return [
            'id' => $sub_theme->getRouteKey(),
            'name' => ucfirst($sub_theme->name),
            'status' => isset($statuses[$sub_theme->getOriginal('status')]) ? $statuses[$sub_theme->getOriginal('status')] : '-',
            'slug' => $sub_theme->slug,
            'category_id' => $sub_theme->category ? $sub_theme->category->name : '-',
        ];
    }
}
