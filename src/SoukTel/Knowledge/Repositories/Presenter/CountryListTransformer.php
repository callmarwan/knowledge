<?php

namespace SoukTel\Knowledge\Repositories\Presenter;

use League\Fractal\TransformerAbstract;

class CountryListTransformer extends TransformerAbstract
{
    public function transform(\SoukTel\Knowledge\Models\Country $country)
    {
        $statuses = trans('knowledge_country.status_options');

        return [
            'id' => $country->getRouteKey(),
            'name' => ucfirst($country->name),
            'code' => $country->code,
            'full_name' => $country->full_name,
//            'iso3' => $country->iso3,
//            'number' => $country->number,
//            'continent_code' => $country->continent_code,
//            'continent' => $country->continent,
//            'display_order' => $country->display_order,
            'status' => isset($statuses[$country->getOriginal('status')]) ? $statuses[$country->getOriginal('status')] : '-',
        ];
    }
}
