<?php

namespace SoukTel\Knowledge\Repositories\Presenter;

use League\Fractal\TransformerAbstract;
use Hashids;

class CountryItemTransformer extends TransformerAbstract
{
    public function transform(\SoukTel\Knowledge\Models\Country $country)
    {
        return [
            'id' => $country->getRouteKey(),
            'name' => ucfirst($country->name),
            'code' => $country->code,
            'full_name' => $country->full_name,
//            'iso3' => $country->iso3,
//            'number' => $country->number,
//            'continent_code' => $country->continent_code,
//            'continent' => $country->continent,
//            'display_order' => $country->display_order,
            'status' => ucfirst($country->status),
        ];
    }
}