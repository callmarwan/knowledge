<?php

namespace SoukTel\Knowledge;

class ResearchInstrument
{
    /**
     * $knowledge object.
     */
    protected $knowledge;
    /**
     * $country object.
     */
    protected $research_instrument;

    /**
     * Constructor.
     */
    public function __construct(\SoukTel\Knowledge\Interfaces\KnowledgeRepositoryInterface $knowledge,
                                \SoukTel\Knowledge\Interfaces\ResearchInstrumentRepositoryInterface $research_instrument)
    {
        $this->knowledge = $knowledge;
        $this->research_instrument = $research_instrument;
    }
}
