<?php

namespace SoukTel\Knowledge\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Litepie\Database\Model;
use Litepie\Database\Traits\Slugger;
use Litepie\Filer\Traits\Filer;
use Litepie\Hashids\Traits\Hashids;
use Litepie\Repository\Traits\PresentableTrait;
use Litepie\Revision\Traits\Revision;
use Litepie\Trans\Traits\Trans;
use Litepie\User\Models\Role;
use Litepie\User\Traits\UserModel;

class Knowledge extends Model
{
    use Filer, Hashids, Slugger, Trans, Revision, PresentableTrait, UserModel;

    /**
     * Configuartion for the model.
     *
     * @var array
     */
    protected $config = 'package.knowledge.knowledge';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany('SoukTel\Knowledge\Models\Category', 'knowledge_category', 'knowledge_id', 'category_id');
    }

    public function sub_themes()
    {
        return $this->belongsToMany('SoukTel\Knowledge\Models\SubTheme', 'knowledge_sub_theme', 'knowledge_id', 'sub_theme_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany('SoukTel\Knowledge\Models\Tag', 'knowledge_tag', 'knowledge_id', 'tag_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function research_instruments()
    {
        return $this->belongsToMany('SoukTel\Knowledge\Models\ResearchInstrument', 'knowledge_research_instrument', 'knowledge_id', 'research_instrument_id');
    }

    public function methodologies()
    {
        return $this->belongsToMany('SoukTel\Knowledge\Models\Methodology', 'knowledge_methodology', 'knowledge_id', 'methodology_id');
    }

    public function data_types()
    {
        return $this->belongsToMany('SoukTel\Knowledge\Models\DataType', 'knowledge_data_type', 'knowledge_id', 'data_type_id');
    }

    public function type()
    {
        return $this->belongsTo('SoukTel\Knowledge\Models\Type');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function countries()
    {
        return $this->belongsToMany('SoukTel\Knowledge\Models\Country', 'knowledge_country', 'knowledge_id', 'country_id');
    }

    public function sharingCountries()
    {
        return $this->belongsToMany('SoukTel\Knowledge\Models\Country', 'country_sharing', 'knowledge_id', 'country_id')->withTimestamps();
    }

    public function sharingRoles()
    {
        return $this->belongsToMany(Role::class, 'role_sharing', 'knowledge_id', 'role_id')->withTimestamps();
    }

    public function sharingUsers()
    {
        return $this->belongsToMany(User::class, 'user_sharing', 'knowledge_id', 'user_id')->withTimestamps();
    }

    public function documents()
    {
        return $this->hasMany('SoukTel\Knowledge\Models\Document');
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'author_id');
    }

    public function getPublishedAttribute()
    {
        return isset($this->attributes['published']) && $this->attributes['published'] == 1 ? trans('knowledge.label.published') : trans('knowledge.label.not_published');
    }


    public function getUploadedLocales()
    {
        return $this->documents()->pluck('locale')->toArray();
    }

    public function getResearchDateAttribute($value)
    {
        return Carbon::parse($value)->format('m/Y');
    }

    public function scopePublished($query)
    {
        return $query->wherePublished(1);
    }

    public function scopeApproved($query)
    {
        return $query->whereStatus('Approved');
    }
}
