<?php

namespace SoukTel\Knowledge\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Litepie\Database\Model;
use Litepie\Database\Traits\Slugger;
use Litepie\Filer\Traits\Filer;
use Litepie\Hashids\Traits\Hashids;
use Litepie\Repository\Traits\PresentableTrait;
use Litepie\Revision\Traits\Revision;
use Litepie\Trans\Traits\Trans;
use Litepie\User\Traits\UserModel;

class Document extends Model
{
    use Hashids, Revision, PresentableTrait, UserModel;

    /**
     * Configuartion for the model.
     *
     * @var array
     */
    protected $config = 'package.knowledge.document';


    public function getFileTypeAttribute()
    {
        $type = $this->attributes['file_type'];
        $mime_types = config('package.knowledge.document.mimes_icons');

        return isset($mime_types[$type]) ? $mime_types[$type] : 'fa fa-sticky-note-o';
    }

    public function knowledge()
    {
        return $this->belongsTo(Knowledge::class);
    }

    public function getFullPath()
    {
        return storage_path() . $this->attributes['file_path'] . '/' . $this->attributes['file_name'];
    }
}
