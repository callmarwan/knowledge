<?php

namespace SoukTel\Knowledge\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Litepie\Database\Model;
use Litepie\Database\Traits\Slugger;
use Litepie\Filer\Traits\Filer;
use Litepie\Hashids\Traits\Hashids;
use Litepie\Repository\Traits\PresentableTrait;
use Litepie\Revision\Traits\Revision;
use Litepie\Trans\Traits\Trans;
use Litepie\User\Traits\UserModel;

class Type extends Model
{
    use Filer, SoftDeletes, Hashids, Slugger, Revision, PresentableTrait, UserModel, Trans;

    /**
     * Configuartion for the model.
     *
     * @var array
     */
    protected $config = 'package.knowledge.type';
}
