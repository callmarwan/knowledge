<?php

namespace SoukTel\Knowledge\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Litepie\Database\Model;
use Litepie\Database\Traits\Slugger;
use Litepie\Filer\Traits\Filer;
use Litepie\Hashids\Traits\Hashids;
use Litepie\Repository\Traits\PresentableTrait;
use Litepie\Revision\Traits\Revision;
use Litepie\Trans\Traits\Trans;
use Litepie\User\Traits\UserModel;

class Country extends Model
{
    use Filer, Hashids, Trans, Revision, PresentableTrait, UserModel;

    public $timestamps = false;
    /**
     * Configuartion for the model.
     *
     * @var array
     */
    protected $config = 'package.knowledge.country';

    public function scopeStatus($query)
    {
        return $query->whereStatus('show');
    }


    /**
     * @param string $status
     * @return array
     */
    public static function countriesList($status = 'show')
    {
        $country = new Country();
        $temp = [];
        $list = $country->where(function ($query) use ($status) {
            if ($status == 'all') {
                // return all the list and ignore status
                return $query;
            }
            return $query->whereStatus($status);
        })->get();

        foreach ($list as $key => $value) {
            $temp[$value->id] = ucfirst($value->name);
        }

        return $temp;
    }
}
