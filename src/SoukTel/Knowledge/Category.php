<?php

namespace SoukTel\Knowledge;

class Category
{
    /**
     * $knowledge object.
     */
    protected $knowledge;
    /**
     * $category object.
     */
    protected $category;

    /**
     * Constructor.
     */
    public function __construct(\SoukTel\Knowledge\Interfaces\KnowledgeRepositoryInterface $knowledge,
                                \SoukTel\Knowledge\Interfaces\CategoryRepositoryInterface $category)
    {
        $this->knowledge = $knowledge;
        $this->category = $category;
    }

    /**
     * Returns count of knowledge.
     *
     * @param array $filter
     *
     * @return int
     */
    public function count($type)
    {
        $knowledges = $this->$type->all();
        return count($knowledges);
    }

    /**
     * Returns category of knowledge.
     * @return array
     */
    public function getCategory()
    {
        $array = [];
        $categorys = $this->category->scopeQuery(function ($query) {
            return $query->orderBy('name')->whereStatus('show');
        })->all();

        foreach ($categorys as $key => $category) {
            $array[$category['id']] = ucfirst($category['name']);
        }

        return $array;

    }

    /**
     * get categorys for public side
     * @param type|string $view
     * @return type
     */
    public function viewCategorys()
    {
        $this->knowledge->pushCriteria(new \SoukTel\Knowledge\Repositories\Criteria\CategoryPublicCriteria());
        $categorys = $this->category->scopeQuery(function ($query) {
            return $query->orderBy('name');
        })->all();

        return view('knowledge::public.category.index', compact('categorys'))->render();
    }

    /**
     * get related knowledges
     * @param category_id
     * @return array
     */
    public function getRelated($cid)
    {
        $this->knowledge->pushCriteria(new \SoukTel\Knowledge\Repositories\Criteria\KnowledgePublicCriteria());
        $knowledges = $this->knowledge->scopeQuery(function ($query) use ($cid) {
            return $query->orderBy('id', 'DESC')->whereCategory_id($cid)->take(2);
        })->all();

        return $knowledges;
    }

    /**
     * get related knowledges
     * @param category_id
     * @return array
     */
    public function recentKnowledge()
    {
        $this->knowledge->pushCriteria(new \SoukTel\Knowledge\Repositories\Criteria\KnowledgePublicCriteria());
        $knowledges = $this->knowledge->scopeQuery(function ($query) {
            return $query->orderBy('id', 'DESC')->take(5);
        })->all();

        return $knowledges;
    }

    /**
     * get related knowledges
     * @param category_id
     * @return array
     */
    public function getCount($cid)
    {
        $this->knowledge->pushCriteria(new \SoukTel\Knowledge\Repositories\Criteria\KnowledgePublicCriteria());
        $knowledges = $this->knowledge->scopeQuery(function ($query) use ($cid) {
            return $query->orderBy('id', 'DESC')->whereCategory_id($cid);
        })->all();

        return count($knowledges);
    }

    /**
     * Returns count of knowledge.
     *
     * @param array $filter
     *
     * @return int
     */
    public function countKnowledges()
    {
        $this->knowledge->pushCriteria(new \SoukTel\Knowledge\Repositories\Criteria\KnowledgePublicCriteria());
        $knowledges = $this->knowledge->scopeQuery(function ($query) {
            return $query->whereStatus('show');
        })->all();
        return count($knowledges);
    }


}
