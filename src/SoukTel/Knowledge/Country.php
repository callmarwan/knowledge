<?php

namespace SoukTel\Knowledge;

class Country
{
    /**
     * $knowledge object.
     */
    protected $knowledge;
    /**
     * $country object.
     */
    protected $country;

    /**
     * Constructor.
     */
    public function __construct(\SoukTel\Knowledge\Interfaces\KnowledgeRepositoryInterface $knowledge,
                                \SoukTel\Knowledge\Interfaces\CountryRepositoryInterface $country)
    {
        $this->knowledge = $knowledge;
        $this->country = $country;
    }
}
