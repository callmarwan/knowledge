<?php

namespace SoukTel\Knowledge\Providers;

use Illuminate\Support\ServiceProvider;

class KnowledgeServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        // Load view
        $this->loadViewsFrom(__DIR__ . '/../../../../resources/views', 'knowledge');

        // Load translation
        $this->loadTranslationsFrom(__DIR__ . '/../../../../resources/lang', 'knowledge');

        // Call pblish redources function
        $this->publishResources();

        include __DIR__ . '/../Http/routes.php';
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // Bind facade
        $this->app->bind('knowledge', function ($app) {
            return $this->app->make('SoukTel\Knowledge\Knowledge');
        });
        $this->app->bind('document', function ($app) {
            return $this->app->make('SoukTel\Knowledge\Document');
        });

// Bind Knowledge to repository
        $this->app->bind(
            \SoukTel\Knowledge\Interfaces\KnowledgeRepositoryInterface::class,
            \SoukTel\Knowledge\Repositories\Eloquent\KnowledgeRepository::class
        );
        // Bind Category to repository
        $this->app->bind(
            \SoukTel\Knowledge\Interfaces\CategoryRepositoryInterface::class,
            \SoukTel\Knowledge\Repositories\Eloquent\CategoryRepository::class
        );
        // Bind Tag to repository
        $this->app->bind(
            \SoukTel\Knowledge\Interfaces\TagRepositoryInterface::class,
            \SoukTel\Knowledge\Repositories\Eloquent\TagRepository::class
        );

        // Bind ResearchInstrument to repository
        $this->app->bind(
            \SoukTel\Knowledge\Interfaces\ResearchInstrumentRepositoryInterface::class,
            \SoukTel\Knowledge\Repositories\Eloquent\ResearchInstrumentRepository::class
        );


        // Bind Country to repository
        $this->app->bind(
            \SoukTel\Knowledge\Interfaces\CountryRepositoryInterface::class,
            \SoukTel\Knowledge\Repositories\Eloquent\CountryRepository::class
        );

        // Bind Methodology to repository
        $this->app->bind(
            \SoukTel\Knowledge\Interfaces\MethodologyRepositoryInterface::class,
            \SoukTel\Knowledge\Repositories\Eloquent\MethodologyRepository::class
        );
        // Bind Types to repository
        $this->app->bind(
            \SoukTel\Knowledge\Interfaces\TypeRepositoryInterface::class,
            \SoukTel\Knowledge\Repositories\Eloquent\TypeRepository::class
        );
        // Bind Methodology to repository
        $this->app->bind(
            \SoukTel\Knowledge\Interfaces\DataTypeRepositoryInterface::class,
            \SoukTel\Knowledge\Repositories\Eloquent\DataTypeRepository::class
        );
        // Bind Methodology to repository
        $this->app->bind(
            \SoukTel\Knowledge\Interfaces\SubThemeRepositoryInterface::class,
            \SoukTel\Knowledge\Repositories\Eloquent\SubThemeRepository::class
        );

        // Bind Document to repository
        $this->app->bind(
            \SoukTel\Knowledge\Interfaces\DocumentRepositoryInterface::class,
            \SoukTel\Knowledge\Repositories\Eloquent\DocumentRepository::class
        );

        $this->app->register(\SoukTel\Knowledge\Providers\AuthServiceProvider::class);
        $this->app->register(\SoukTel\Knowledge\Providers\EventServiceProvider::class);
        $this->app->register(\SoukTel\Knowledge\Providers\RouteServiceProvider::class);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['knowledge'];
    }

    /**
     * Publish resources.
     *
     * @return void
     */
    private function publishResources()
    {
        // Publish configuration file
        $this->publishes([__DIR__ . '/../../../../config/config.php' => config_path('package/knowledge.php')], 'config');

        // Publish admin view
        $this->publishes([__DIR__ . '/../../../../resources/views' => base_path('resources/views/vendor/knowledge')], 'view');

        // Publish language files
        $this->publishes([__DIR__ . '/../../../../resources/lang' => base_path('resources/lang/vendor/knowledge')], 'lang');

        // Publish migrations
        $this->publishes([__DIR__ . '/../../../../database/migrations/' => base_path('database/migrations')], 'migrations');

        // Publish seeds
        $this->publishes([__DIR__ . '/../../../../database/seeds/' => base_path('database/seeds')], 'seeds');

        // Publish public
        $this->publishes([__DIR__ . '/../../../../public/' => public_path('/')], 'uploads');
    }
}
