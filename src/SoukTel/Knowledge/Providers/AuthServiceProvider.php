<?php

namespace SoukTel\Knowledge\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the package.
     *
     * @var array
     */
    protected $policies = [
        // Bind Knowledge policy
        \SoukTel\Knowledge\Models\Knowledge::class => \SoukTel\Knowledge\Policies\KnowledgePolicy::class,
// Bind Category policy
        \SoukTel\Knowledge\Models\Category::class => \SoukTel\Knowledge\Policies\CategoryPolicy::class,
// Bind Tag policy
        \SoukTel\Knowledge\Models\Tag::class => \SoukTel\Knowledge\Policies\TagPolicy::class,
// Bind Country policy
        \SoukTel\Knowledge\Models\Country::class => \SoukTel\Knowledge\Policies\CountryPolicy::class,
// Bind Document policy
        \SoukTel\Knowledge\Models\Document::class => \SoukTel\Knowledge\Policies\DocumentPolicy::class,
    ];

    /**
     * Register any package authentication / authorization services.
     *
     * @param \Illuminate\Contracts\Auth\Access\Gate $gate
     *
     * @return void
     */
    public function boot(GateContract $gate)
    {
        parent::registerPolicies($gate);
    }
}
