<?php

namespace SoukTel\Knowledge\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;
use SoukTel\Knowledge\Models\Knowledge;
use Request;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'SoukTel\Knowledge\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param   \Illuminate\Routing\Router $router
     * @return void
     */
    public function boot(Router $router)
    {
        parent::boot($router);

        if (Request::is('knowledge/knowledge/*')) {
            $router->bind('knowledge', function ($id) {
                $knowledge = $this->app->make('\SoukTel\Knowledge\Interfaces\KnowledgeRepositoryInterface');
                return $knowledge->findorNew($id);
            });
        }

        if (Request::is('*/knowledge/knowledge/*')) {
            $router->bind('knowledge', function ($id) {
                $knowledge = $this->app->make('\SoukTel\Knowledge\Interfaces\KnowledgeRepositoryInterface');
                return $knowledge->findorNew($id);
            });
        }
        if (Request::is('*/knowledge/category/*')) {
            $router->bind('category', function ($id) {
                $category = $this->app->make('\SoukTel\Knowledge\Interfaces\CategoryRepositoryInterface');
                return $category->findorNew($id);
            });
        }
        if (Request::is('*/knowledge/tag/*')) {
            $router->bind('tag', function ($id) {
                $tag = $this->app->make('\SoukTel\Knowledge\Interfaces\TagRepositoryInterface');
                return $tag->findorNew($id);
            });
        }
        if (Request::is('*/knowledge/country/*')) {
            $router->bind('country', function ($id) {
                $country = $this->app->make('\SoukTel\Knowledge\Interfaces\CountryRepositoryInterface');
                return $country->findorNew($id);
            });
        }
        if (Request::is('*/knowledge/sub_theme/*')) {
            $router->bind('sub_theme', function ($id) {
                $sub_theme = $this->app->make('\SoukTel\Knowledge\Interfaces\SubThemeRepositoryInterface');
                return $sub_theme->findorNew($id);
            });
        }
        if (Request::is('*/knowledge/type/*')) {
            $router->bind('type', function ($id) {
                $type = $this->app->make('\SoukTel\Knowledge\Interfaces\TypeRepositoryInterface');
                return $type->findorNew($id);
            });
        }
        if (Request::is('*/knowledge/research_instrument/*')) {
            $router->bind('research_instrument', function ($id) {
                $research_instrument = $this->app->make('\SoukTel\Knowledge\Interfaces\ResearchInstrumentRepositoryInterface');
                return $research_instrument->findorNew($id);
            });
        }
        if (Request::is('*/knowledge/document/*')) {
            $router->bind('document', function ($id) {
                $country = $this->app->make('\SoukTel\Knowledge\Interfaces\DocumentRepositoryInterface');
                return $country->findorNew($id);
            });
        }

    }

    /**
     * Define the routes for the application.
     *
     * @param \Illuminate\Routing\Router $router
     *
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require __DIR__ . '/../Http/routes.php';
        });
    }
}
