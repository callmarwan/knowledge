<?php

namespace SoukTel\Knowledge\Http\Requests;

use App\Http\Requests\Request as FormRequest;
use Illuminate\Http\Request;

class CountryAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Request $request)
    {
        $country = $this->route('country');

        if (is_null($country)) {
            // Determine if the user is authorized to access country module,
            return $request->user('admin.web')->canDo('knowledge.country.view');
        }

        if ($request->isMethod('POST') || $request->is('*/create')) {
            // Determine if the user is authorized to create an entry,
            return $request->user('admin.web')->can('create', $country);
        }

        if ($request->isMethod('PUT') || $request->isMethod('PATCH') || $request->is('*/edit')) {
            // Determine if the user is authorized to update an entry,
            return $request->user('admin.web')->can('update', $country);
        }

        if ($request->isMethod('DELETE')) {
            // Determine if the user is authorized to delete an entry,
            return $request->user('admin.web')->can('delete', $country);
        }

        // Determine if the user is authorized to view the module.
        return $request->user('admin.web')->can('view', $country);

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {

        if ($request->isMethod('POST')) {
            // validation rule for create request.
            return [
                'code' => 'required',
                'name' => 'required',
                'full_name' => 'required',
                'lat'=>'required',
                'lng'=>'required',
//                'iso3' => 'required',
//                'number' => 'required',
//                'continent_code' => 'required',
//                'continent' => 'required',
//                'display_order' => 'required',
                'status' => 'required'
            ];
        }

        if ($request->isMethod('PUT') || $request->isMethod('PATCH')) {
            // Validation rule for update request.
            return [
                'code' => 'required',
                'name' => 'required',
                'full_name' => 'required',
                'lat'=>'required',
                'lng'=>'required',
//                'iso3' => 'required',
//                'number' => 'required',
//                'continent_code' => 'required',
//                'continent' => 'required',
//                'display_order' => 'required',
                'status' => 'required'
            ];
        }

        // Default validation rule.
        return [

        ];
    }

}
