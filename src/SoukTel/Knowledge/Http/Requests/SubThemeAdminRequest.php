<?php

namespace SoukTel\Knowledge\Http\Requests;

use App\Http\Requests\Request as FormRequest;
use Illuminate\Http\Request;

class SubThemeAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Request $request)
    {
        $sub_theme = $this->route('sub_theme');

        if (is_null($sub_theme)) {
            // Determine if the user is authorized to access sub_theme module,
            return $request->user('admin.web')->canDo('knowledge.sub_theme.view');
        }

        if ($request->isMethod('POST') || $request->is('*/create')) {
            // Determine if the user is authorized to create an entry,
            return $request->user('admin.web')->canDo('knowledge.sub_theme.create');
        }

        if ($request->isMethod('PUT') || $request->isMethod('PATCH') || $request->is('*/edit')) {
            // Determine if the user is authorized to update an entry,
            return $request->user('admin.web')->canDo('knowledge.sub_theme.update');
        }

        if ($request->isMethod('DELETE')) {
            // Determine if the user is authorized to delete an entry,
            return $request->user('admin.web')->canDo('knowledge.sub_theme.delete');
        }

        // Determine if the user is authorized to view the module.
        return $request->user('admin.web')->canDo('knowledge.sub_theme.view');

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {

        if ($request->isMethod('POST')) {
            // validation rule for create request.
            return [
                'name' => 'required',
                'status' => 'required',
            ];
        }

        if ($request->isMethod('PUT') || $request->isMethod('PATCH')) {
            // Validation rule for update request.
            return [
                'name' => 'required',
                'status' => 'required',
            ];
        }

        // Default validation rule.
        return [

        ];
    }

}
