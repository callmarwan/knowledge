<?php

namespace SoukTel\Knowledge\Http\Requests;

use App\Http\Requests\Request as FormRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Lavalite\Page\Http\Controllers\PageAdminWebController;

class KnowledgeUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Request $request)
    {
        $knowledge = $this->route('knowledge');

        if (is_null($knowledge)) {
            // Determine if the user is authorized to access knowledge module,
            return $request->user('web')->canDo('knowledge.knowledge.view');
        }

        if (($request->isMethod('POST') || $request->is('*/create'))) {
            // Determine if the user is authorized to create an entry,
            return $request->user('web')->canDo('knowledge.knowledge.create');
        }

        if ($request->isMethod('PUT') || $request->isMethod('PATCH') || $request->is('*/edit')) {
            if ($knowledge->author_id == user('web')->id) {
                // Determine if the user is authorized to update an entry,
                return $request->user('web')->canDo('knowledge.knowledge.update');
            }
            return false;
        }

        if ($request->isMethod('DELETE')) {
            if ($knowledge->author_id == user('web')->id) {
                // Determine if the user is authorized to delete an entry,
                return $request->user('web')->canDo('knowledge.knowledge.delete');
            }
            return false;
        }

        if (
            (
                ($knowledge->sharingCountries()->where('id', user('web')->country_id)->first()
                    || $knowledge->sharingRoles()->whereIn('id', user('web')->getRoles->pluck('id')->toArray())->first()
                    || $knowledge->sharingUsers()->where('id', user('web')->id)->first())
                && in_array($knowledge->status, ['Approved', 'Unmoderated'])
                && $knowledge->getOriginal('published') == 1
            )
            || $knowledge->author_id == user('web')->id
        ) {
            // Determine if the user is authorized to view the module.
            return $request->user('web')->canDo('knowledge.knowledge.view');
        } else {
            return false;
        }

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $rules = [];
        if ($request->isMethod('POST')) {
            // validation rule for create request.
            $rules = [
                'title' => 'required',
                'type' => 'required',
                'themes' => 'required',
                'description' => 'required',
                'countries' => 'required',
            ];
            $write = $request->get('write');
            if ($request->is('*research*')){
                $rules = array_merge($rules , ['sub_themes' => 'required', 'methodologies' => 'required']);
            }
            else{
                $rules = array_merge($rules , ['tags' => 'required']);
            }

            foreach (config('trans.locales') as $code => $language) {
                $rules['document_' . $code] = str_replace('required|', '', config('package.knowledge.document.validation_rule'));
            }

            if ($request->is('*research*')) {
                $rules['research_author'] = 'required';
                $rules['research_institution'] = 'required';
                $rules['research_date'] = 'required|date|before:' . Carbon::tomorrow();
            }


            if (isset($write)) {
                $rules['body'] = 'required';
            }
        }

        if ($request->isMethod('PUT') || $request->isMethod('PATCH')) {
            if (isset($request->share_only) && $request->share_only == 'true') {
                $rules = [

                ];
            } else {
                // Validation rule for update request.
                $rules = [
                    'title' => 'required',
                    'type' => 'required',
                    'themes' => 'required',
                    'description' => 'required',
                    'countries' => 'required',
                ];
                $write = $request->get('write');
                if ($request->is('*research*')){
                    $rules = array_merge($rules , ['sub_themes' => 'required', 'methodologies' => 'required']);
                }
                else{
                    $rules = array_merge($rules , ['tags' => 'required']);
                }


                foreach (config('trans.locales') as $code => $language) {
                    if ($this->hasFile('document_' . $code)) {
                        $file = $this->file('document_' . $code);

                        if ($file->getClientMimeType() == 'audio/mp3') {
                            continue;
                        }
                    }
                    $rules['document_' . $code] = str_replace('required|', '', config('package.knowledge.document.validation_rule'));
                }

                if ($request->is('*research*')) {
                    $rules['research_author'] = 'required';
                    $rules['research_institution'] = 'required';
                    $rules['research_date'] = 'required|date|before:' . Carbon::tomorrow();
                }

                $write = $request->get('write');

                if (isset($write)) {
                    $rules['body'] = 'required';
                }
            }
        }

        // Default validation rule.
        return $rules;
    }

    protected function getValidatorInstance()
    {
        if ($this->isMethod('POST') || $this->isMethod('PUT')) {
            $data = $this->all();
            if ($this->is('*research*')) {
                if (!empty($data['research_date'])) {
                    $data['research_date'] = Carbon::createFromFormat('m/Y', $data['research_date'])->format('Y-m-01');
                }
            }
            $this->getInputSource()->replace($data);
        }

        return parent::getValidatorInstance();
    }

    public function attributes()
    {
        return [
            'type'  => trans('knowledge.label.type_of_resource'),
            'countries' => trans('knowledge.label.countries'),
        ];
    }

}
