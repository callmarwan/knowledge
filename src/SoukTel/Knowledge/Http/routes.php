<?php

// Admin web routes  for knowledge
Route::group(['prefix' => trans_setlocale() . '/admin/knowledge'], function () {
    Route::post('knowledge/status/{knowledge}', 'SoukTel\Knowledge\Http\Controllers\KnowledgeAdminController@update');
    Route::resource('knowledge', 'SoukTel\Knowledge\Http\Controllers\KnowledgeAdminController');
    Route::get('knowledge/load-documents/{knowledge}', 'SoukTel\Knowledge\Http\Controllers\KnowledgeAdminController@loadDocs');
});

// Admin API routes  for knowledge
Route::group(['prefix' => trans_setlocale() . 'api/v1/admin/knowledge'], function () {
    Route::resource('knowledge', 'SoukTel\Knowledge\Http\Controllers\KnowledgeAdminApiController');
});

// User web routes for knowledge
Route::group(['prefix' => trans_setlocale() . '/user/knowledge'], function () {
    Route::resource('knowledge', 'SoukTel\Knowledge\Http\Controllers\KnowledgeUserController');
    Route::get('knowledge/download-document/{knowledge}', 'SoukTel\Knowledge\Http\Controllers\KnowledgeUserController@download');
    Route::post('knowledge/publish/{knowledge}', 'SoukTel\Knowledge\Http\Controllers\KnowledgeUserController@publish');
    Route::post('knowledge/unpublish/{knowledge}', 'SoukTel\Knowledge\Http\Controllers\KnowledgeUserController@unpublish');

    Route::get('research', 'SoukTel\Knowledge\Http\Controllers\KnowledgeUserController@index');
    Route::get('knowledge/create/research', 'SoukTel\Knowledge\Http\Controllers\KnowledgeUserController@create');
    Route::post('knowledge/research', 'SoukTel\Knowledge\Http\Controllers\KnowledgeUserController@store');
    Route::put('knowledge/research/{knowledge}', 'SoukTel\Knowledge\Http\Controllers\KnowledgeUserController@update');
    Route::get('knowledge/research/{knowledge}/edit', 'SoukTel\Knowledge\Http\Controllers\KnowledgeUserController@edit');

    Route::get('load-knowledge/{type?}/{research?}', 'SoukTel\Knowledge\Http\Controllers\KnowledgeUserController@getKnowledgeList');
    Route::get('knowledge/load-attachments/{knowledge}', 'SoukTel\Knowledge\Http\Controllers\KnowledgeUserController@loadAttachments');
    Route::get('knowledge/load-share-form/{knowledge}', 'SoukTel\Knowledge\Http\Controllers\KnowledgeUserController@loadShareForm');
    Route::delete('document/{document}', 'SoukTel\Knowledge\Http\Controllers\DocumentUserController@destroy');
    Route::get('document/download/{document}', 'SoukTel\Knowledge\Http\Controllers\DocumentUserController@download');
});

// User API routes for knowledge
Route::group(['prefix' => trans_setlocale() . 'api/v1/user/knowledge'], function () {
    Route::resource('knowledge', 'SoukTel\Knowledge\Http\Controllers\KnowledgeUserApiController');
});

// Public web routes for knowledge
Route::group(['prefix' => trans_setlocale() . '/knowledge'], function () {
    Route::get('/', 'SoukTel\Knowledge\Http\Controllers\KnowledgeController@index');
    Route::get('knowledge/download-document/{knowledge}/{document_id?}/{post_id?}', 'SoukTel\Knowledge\Http\Controllers\KnowledgeController@download');
    Route::get('load-knowledge/{research?}', 'SoukTel\Knowledge\Http\Controllers\KnowledgeController@getKnowledgeList');
});

// Public API routes for knowledge
Route::group(['prefix' => trans_setlocale() . 'api/v1/knowledges'], function () {
    Route::get('/', 'SoukTel\Knowledge\Http\Controllers\KnowledgeApiController@index');
    Route::get('/{slug?}', 'SoukTel\Knowledge\Http\Controllers\KnowledgeApiController@show');
});

// Admin web routes  for category
Route::group(['prefix' => trans_setlocale() . '/admin/knowledge'], function () {
    Route::resource('category', 'SoukTel\Knowledge\Http\Controllers\CategoryAdminController');
});

// Admin API routes  for category
Route::group(['prefix' => trans_setlocale() . 'api/v1/admin/knowledge'], function () {
    Route::resource('category', 'SoukTel\Knowledge\Http\Controllers\CategoryAdminApiController');
});

// User web routes for category
Route::group(['prefix' => trans_setlocale() . '/user/knowledge'], function () {
    Route::get('category_sub_themes', 'SoukTel\Knowledge\Http\Controllers\CategoryUserController@getSubThemes');
    Route::resource('category', 'SoukTel\Knowledge\Http\Controllers\CategoryUserController');

});

// User API routes for category
Route::group(['prefix' => trans_setlocale() . 'api/v1/user/knowledge'], function () {
    Route::resource('category', 'SoukTel\Knowledge\Http\Controllers\CategoryUserApiController');
});

// Public web routes for category
Route::group(['prefix' => trans_setlocale() . '/knowledge'], function () {
    Route::get('/category', 'SoukTel\Knowledge\Http\Controllers\CategoryController@index');
    Route::get('/category/{slug?}', 'SoukTel\Knowledge\Http\Controllers\CategoryController@show');
});

// Public API routes for category
Route::group(['prefix' => trans_setlocale() . 'api/v1/knowledges'], function () {
    Route::get('/', 'SoukTel\Knowledge\Http\Controllers\CategoryApiController@index');
    Route::get('/{slug?}', 'SoukTel\Knowledge\Http\Controllers\CategoryApiController@show');
});


// Admin web routes  for tag
Route::group(['prefix' => trans_setlocale() . '/admin/knowledge'], function () {
    Route::resource('tag', 'SoukTel\Knowledge\Http\Controllers\TagAdminController');
});


// Admin API routes  for tag
Route::group(['prefix' => trans_setlocale() . 'api/v1/admin/knowledge'], function () {
    Route::resource('tag', 'SoukTel\Knowledge\Http\Controllers\TagAdminApiController');
});

// User web routes for tag
Route::group(['prefix' => trans_setlocale() . '/user/knowledge'], function () {
    Route::resource('tag', 'SoukTel\Knowledge\Http\Controllers\TagUserController');
});

// User API routes for tag
Route::group(['prefix' => trans_setlocale() . 'api/v1/user/knowledge'], function () {
    Route::resource('tag', 'SoukTel\Knowledge\Http\Controllers\TagUserApiController');
});

// Public web routes for tag
Route::group(['prefix' => trans_setlocale() . '/knowledge'], function () {
    Route::get('/tag', 'SoukTel\Knowledge\Http\Controllers\TagController@index');
    Route::get('/tag/{slug?}', 'SoukTel\Knowledge\Http\Controllers\TagController@show');
});

// Public API routes for tag
Route::group(['prefix' => trans_setlocale() . 'api/v1/knowledges'], function () {
    Route::get('/', 'SoukTel\Knowledge\Http\Controllers\TagApiController@index');
    Route::get('/{slug?}', 'SoukTel\Knowledge\Http\Controllers\TagApiController@show');
});


// Admin web routes  for country
Route::group(['prefix' => trans_setlocale() . '/admin/knowledge'], function () {
    Route::get('category_sub_themes', 'SoukTel\Knowledge\Http\Controllers\CategoryAdminController@getSubThemes');
    Route::resource('country', 'SoukTel\Knowledge\Http\Controllers\CountryAdminController');
});

// Admin API routes  for country
Route::group(['prefix' => trans_setlocale() . 'api/v1/admin/knowledge'], function () {
    Route::resource('country', 'SoukTel\Knowledge\Http\Controllers\CountryAdminApiController');
});

// User web routes for country
Route::group(['prefix' => trans_setlocale() . '/user/knowledge'], function () {
    Route::resource('country', 'SoukTel\Knowledge\Http\Controllers\CountryUserController');
});

// User API routes for country
Route::group(['prefix' => trans_setlocale() . 'api/v1/user/knowledge'], function () {
    Route::resource('country', 'SoukTel\Knowledge\Http\Controllers\CountryUserApiController');
});

// Public web routes for country
Route::group(['prefix' => trans_setlocale() . '/knowledge'], function () {
    Route::get('/country', 'SoukTel\Knowledge\Http\Controllers\CountryController@index');
    Route::get('/country/{slug?}', 'SoukTel\Knowledge\Http\Controllers\CountryController@show');
});

// Public API routes for country
Route::group(['prefix' => trans_setlocale() . 'api/v1/knowledges'], function () {
    Route::get('/', 'SoukTel\Knowledge\Http\Controllers\CountryApiController@index');
    Route::get('/{slug?}', 'SoukTel\Knowledge\Http\Controllers\CountryApiController@show');
});


// Admin web routes  for country
Route::group(['prefix' => trans_setlocale() . '/admin/knowledge'], function () {
    Route::post('document', 'SoukTel\Knowledge\Http\Controllers\DocumentAdminController@store');
    Route::delete('document/{document}', 'SoukTel\Knowledge\Http\Controllers\DocumentAdminController@destroy');
    Route::get('document/download/{document}', 'SoukTel\Knowledge\Http\Controllers\DocumentAdminController@download');
});



// Admin web routes  for tag
Route::group(['prefix' => trans_setlocale() . '/admin/knowledge'], function () {
    Route::resource('research_instrument', 'SoukTel\Knowledge\Http\Controllers\ResearchInstrumentAdminController');
});


// Admin API routes  for ResearchInstrument
Route::group(['prefix' => trans_setlocale() . 'api/v1/admin/knowledge'], function () {
    Route::resource('research_instrument', 'SoukTel\Knowledge\Http\Controllers\ResearchInstrumentAdminApiController');
});

// User web routes for research_instruments
Route::group(['prefix' => trans_setlocale() . '/user/knowledge'], function () {
    Route::resource('research_instrument', 'SoukTel\Knowledge\Http\Controllers\ResearchInstrumentUserController');
});

// User API routes for research_instruments
Route::group(['prefix' => trans_setlocale() . 'api/v1/user/knowledge'], function () {
    Route::resource('research_instrument', 'SoukTel\Knowledge\Http\Controllers\ResearchInstrumentUserApiController');
});

// Public web routes for research_instruments
Route::group(['prefix' => trans_setlocale() . '/knowledge'], function () {
    Route::get('/research_instrument', 'SoukTel\Knowledge\Http\Controllers\ResearchInstrumentController@index');
    Route::get('/research_instrument/{slug?}', 'SoukTel\Knowledge\Http\Controllers\ResearchInstrumentController@show');
});

// Public API routes for research_instruments
Route::group(['prefix' => trans_setlocale() . 'api/v1/knowledges'], function () {
    Route::get('/', 'SoukTel\Knowledge\Http\Controllers\ResearchInstrumentApiController@index');
    Route::get('/{slug?}', 'SoukTel\Knowledge\Http\Controllers\ResearchInstrumentApiController@show');
});


// Admin web routes  for tag
Route::group(['prefix' => trans_setlocale() . '/admin/knowledge'], function () {
    Route::resource('sub_theme', 'SoukTel\Knowledge\Http\Controllers\SubThemeAdminController');
});


// Admin API routes  for sub_themes
Route::group(['prefix' => trans_setlocale() . 'api/v1/admin/knowledge'], function () {
    Route::resource('sub_theme', 'SoukTel\Knowledge\Http\Controllers\SubThemeAdminApiController');
});

// User web routes for sub_themes
Route::group(['prefix' => trans_setlocale() . '/user/knowledge'], function () {
    Route::resource('sub_theme', 'SoukTel\Knowledge\Http\Controllers\SubThemeUserController');
});

// User API routes for sub_themes
Route::group(['prefix' => trans_setlocale() . 'api/v1/user/knowledge'], function () {
    Route::resource('sub_theme', 'SoukTel\Knowledge\Http\Controllers\SubThemeUserApiController');
});

// Public web routes for sub_themes
Route::group(['prefix' => trans_setlocale() . '/knowledge'], function () {
    Route::get('/sub_theme', 'SoukTel\Knowledge\Http\Controllers\SubThemeController@index');
    Route::get('/sub_theme/{slug?}', 'SoukTel\Knowledge\Http\Controllers\SubThemeController@show');
});


// Admin web routes  for tag
Route::group(['prefix' => trans_setlocale() . '/admin/knowledge'], function () {
    Route::resource('type', 'SoukTel\Knowledge\Http\Controllers\TypeAdminController');
});


