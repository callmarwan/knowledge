<?php

namespace SoukTel\Knowledge\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use SoukTel\Knowledge\Models\Knowledge;

class KnowledgeBaseController extends BaseController
{
    protected function filterQuery($query, $attributes)
    {
        try {
            if (isset($attributes['from_date']) && !empty($attributes['from_date'])) {
                $from_date = Carbon::createFromFormat('m/Y', $attributes['from_date'])->format('Y-m-01');
                $query = $query->whereDate('research_date', '>=', $from_date);
            }

            if (isset($attributes['to_date']) && !empty($attributes['to_date'])) {
                $to_date = Carbon::createFromFormat('m/Y', $attributes['to_date'])->format('Y-m-31');
                $query = $query->whereDate('research_date', '<=', $to_date);
            }

            if (isset($attributes['tag']) && !empty($attributes['tag'])) {
                $tag = $attributes['tag'];
                $query = $query->whereHas('tags', function ($query) use ($tag) {
                    $query->whereIn('knowledge_tag.tag_id', $tag);
                });
            }
            if (isset($attributes['category']) && !empty($attributes['category'])) {
                $category = $attributes['category'];
                $query = $query->whereHas('categories', function ($query) use ($category) {
                    $query->whereIn('knowledge_category.category_id', $category);
                });
            }
            if (isset($attributes['country']) && !empty($attributes['country'])) {
                $country = $attributes['country'];
                $query = $query->whereHas('countries', function ($query) use ($country) {
                    $query->whereIn('knowledge_country.country_id', $country);
                });
            }

            if (isset($attributes['type']) && !empty($attributes['type'])) {
                $query = $query->whereType($attributes['type']);
            }

            if (isset($attributes['search']) && !empty($attributes['search'])) {
                $search_expression = $this->getSearchExpression($attributes['search']);
                $query = $query->whereRaw("(
                match(title,description,research_author,research_institution) against (" . \DB::getPdo()->quote($search_expression) . " in boolean mode)
                or match(title,description,body,research_author,research_institution) against (" . \DB::getPdo()->quote($search_expression) . " in boolean mode))
                ");
            }
            return $query;
        } catch (\Exception $e) {
            abort(400);
        }
    }

    private function removeSpecialCharacters($word)
    {
        return preg_replace("/[\#\$\%\&\'\(\)\*\+\,\-\.\/\:\;\<\=\>\?\@\[\]\^\_\`\{\|\}\~]/", '', $word);
    }

    protected function getSearchExpression($search_term)
    {
        $exp = explode(' ', trim(strip_tags($search_term)));

        $search_expression = '';
        $counter = 1;

        foreach ($exp as $e) {
            $e = $this->removeSpecialCharacters($e);
            if (strlen($e) >= 2) {
                $search_expression .= "+$e*";
                if ($counter + 1 == count($exp))
                    $search_expression .= ' ';
                $counter++;
            }
        }
        return $search_expression;
    }


    protected function sendSharingEmail(Knowledge $knowledge, $users = null)
    {
        if ($users) {
            $emails = $knowledge->sharingUsers()->whereIn('id', $users)->pluck('email')->toArray();
        } else {
            $emails = $knowledge->sharingUsers()->pluck('email')->toArray();
        }

        Mail::queue('knowledge::user.knowledge.email.sharing_email',
            ['knowledge' => $knowledge], function ($message) use ($emails) {
                $message->bcc($emails)
                    ->subject(trans('user_knowledge.sharing_email_subject'));
            });
    }

    protected function sendPublishEmail(Knowledge $knowledge)
    {
        $emails = User::whereHas('roles', function ($query) {
            $query->whereIn('id', config('user.admin_roles_ids'));
        })->pluck('email')->toArray();

        Mail::queue('knowledge::user.knowledge.email.publish_email',
            ['knowledge' => $knowledge], function ($message) use ($emails) {
                $message->to($emails)
                    ->subject(trans('user_knowledge.publish_email_subject'));
            });
    }
}
