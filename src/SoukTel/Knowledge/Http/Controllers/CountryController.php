<?php

namespace SoukTel\Knowledge\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use SoukTel\Knowledge\Interfaces\CountryRepositoryInterface;

class CountryController extends BaseController
{

    /**
     * Constructor.
     *
     * @param type \SoukTel\Country\Interfaces\CountryRepositoryInterface $country
     *
     * @return type
     */
    public function __construct(CountryRepositoryInterface $country)
    {
        $this->middleware('web');
        $this->setupTheme(config('theme.themes.public.theme'), config('theme.themes.public.layout'));
        $this->repository = $country;
        parent::__construct();
    }

    /**
     * Show country's list.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function index()
    {
        $countrys = $this->repository->scopeQuery(function ($query) {
            return $query->orderBy('id', 'DESC');
        })->paginate();

        return $this->theme->of('knowledge::public.country.index', compact('countrys'))->render();
    }

    /**
     * Show country.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function show($slug)
    {
        $country = $this->repository->scopeQuery(function ($query) use ($slug) {
            return $query->orderBy('id', 'DESC')
                ->where('slug', $slug);
        })->first(['*']);

        return $this->theme->of('knowledge::public.country.show', compact('country'))->render();
    }
}
