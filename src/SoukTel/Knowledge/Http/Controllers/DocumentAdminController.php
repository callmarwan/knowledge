<?php

namespace SoukTel\Knowledge\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use Form;
use SoukTel\Knowledge\Http\Requests\DocumentAdminRequest;
use SoukTel\Knowledge\Interfaces\DocumentRepositoryInterface;
use SoukTel\Knowledge\Models\Document;
use SoukTel\Knowledge\Models\Knowledge;

/**
 * Admin web controller class.
 */
class DocumentAdminController extends BaseController
{
    /**
     * The authentication guard that should be used.
     *
     * @var string
     */
    public $guard = 'admin.web';

    /**
     * Initialize document controller.
     *
     * @param type DocumentRepositoryInterface $document
     *
     * @return type
     */
    public $home = 'admin';

    public function __construct(DocumentRepositoryInterface $document)
    {
        $this->middleware('web');
        $this->middleware('auth:admin.web');
        $this->setupTheme(config('theme.themes.admin.theme'), config('theme.themes.admin.layout'));
        $this->repository = $document;
        parent::__construct();
    }

    public function store(DocumentAdminRequest $request)
    {
        try {
            $attributes['created_by'] = user_id('admin.web');

            $attributes['locale'] = $request->get('locale');

            $attributes['knowledge_id'] = $request->get('knowledge_id');

            $knowledge = Knowledge::find($attributes['knowledge_id']);

            if ($request->hasFile('file')) {
                $attributes['original_name'] = $request->file('file')->getClientOriginalName();

                $attributes['file_type'] = $request->file('file')->getClientMimeType();

                $destination_path = '/app/documents/knowledge_' . $knowledge->id;

                $attributes['file_name'] = hash('sha256', microtime()) . '.' . $request->file('file')->getClientOriginalExtension();

                $attributes['file_path'] = $destination_path;

                $request->file('file')->move(storage_path() . $destination_path, $attributes['file_name']);
            }

            $document = $this->repository->create($attributes);

            $knowledge = Knowledge::find($attributes['knowledge_id']);

            return response()->json([
                'message' => trans('messages.success.updated', ['Module' => trans('knowledge_document.name')]),
                'code' => 204,
                'redirect' => trans_url('/admin/knowledge/knowledge/' . $knowledge->getRouteKey()),
            ], 201);

        } catch (Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'code' => 400,
            ], 400);
        }
    }

    /**
     * @param DocumentAdminRequest $request
     * @param Document $document
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(DocumentAdminRequest $request, Document $document)
    {

        try {
            $knowledge = $document->knowledge;
            @unlink($document->getFullPath());
            $t = $document->delete();
            return response()->json([
                'message' => trans('messages.success.deleted', ['Module' => trans('knowledge_document.name')]),
                'code' => 204,
                'redirect' => trans_url('admin/knowledge/knowledge/load-documents/' . $knowledge->getRouteKey()),
            ], 201);

        } catch (Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'code' => 400,
                'redirect' => trans_url('/admin/knowledge/document/' . $document->getRouteKey()),
            ], 400);
        }
    }

    public function download(Document $document)
    {
        return response()->download($document->getFullPath(), $document->original_name);
    }
}
