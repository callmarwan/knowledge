<?php

namespace SoukTel\Knowledge\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use Form;
use SoukTel\Knowledge\Http\Requests\TypeAdminRequest;
use SoukTel\Knowledge\Interfaces\TypeRepositoryInterface;
use SoukTel\Knowledge\Models\Type;

/**
 * Admin web controller class.
 */
class TypeAdminController extends BaseController
{
    /**
     * The authentication guard that should be used.
     *
     * @var string
     */
    public $guard = 'admin.web';

    /**
     * Initialize type controller.
     *
     * @param type TypeRepositoryInterface $type
     *
     * @return type
     */
    public $home = 'admin';

    public function __construct(TypeRepositoryInterface $type)
    {
        $this->middleware('web');
        $this->middleware('auth:admin.web');
        $this->setupTheme(config('theme.themes.admin.theme'), config('theme.themes.admin.layout'));
        $this->repository = $type;
        parent::__construct();
    }

    /**
     * Display a list of type.
     *
     * @return Response
     */
    public function index(TypeAdminRequest $request)
    {
        $pageLimit = $request->input('pageLimit');
        if ($request->wantsJson()) {
            $types = $this->repository
                ->setPresenter('\\SoukTel\\Knowledge\\Repositories\\Presenter\\TypeListPresenter')
                ->scopeQuery(function ($query) {
                    return $query->orderBy('id', 'DESC');
                })->paginate($pageLimit);

            $types['recordsTotal'] = $types['meta']['pagination']['total'];
            $types['recordsFiltered'] = $types['meta']['pagination']['total'];
            $types['request'] = $request->all();
            return response()->json($types, 200);

        }

        $this->theme->prependTitle(trans('knowledge_type.names') . ' :: ');
        return $this->theme->of('knowledge::admin.type.index')->render();
    }

    /**
     * @param TypeAdminRequest $request
     * @param Type $type
     * @return \Illuminate\Http\Response
     */
    public function show(TypeAdminRequest $request, Type $type)
    {
        if (!$type->exists) {
            return response()->view('knowledge::admin.type.new', compact('type'));
        }

        Form::populate($type);
        return response()->view('knowledge::admin.type.show', compact('type'));
    }

    /**
     * @param TypeAdminRequest $request
     * @return \Illuminate\Http\Response
     */
    public function create(TypeAdminRequest $request)
    {

        $type = $this->repository->newInstance([]);

        Form::populate($type);

        return response()->view('knowledge::admin.type.create', compact('type'));

    }

    /**
     * @param TypeAdminRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(TypeAdminRequest $request)
    {
        try {
            $attributes = $request->all();

            $type = $this->repository->create($attributes);

            return response()->json([
                'message' => trans('messages.success.updated', ['Module' => trans('knowledge_type.name')]),
                'code' => 204,
                'redirect' => trans_url('/admin/knowledge/type/' . $type->getRouteKey())
            ], 201);


        } catch (Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'code' => 400,
            ], 400);
        }
    }

    /**
     * @param TypeAdminRequest $request
     * @param Type $type
     * @return \Illuminate\Http\Response
     */
    public function edit(TypeAdminRequest $request, Type $type)
    {
        Form::populate($type);
        return response()->view('knowledge::admin.type.edit', compact('type'));
    }

    /**
     * @param TypeAdminRequest $request
     * @param Type $type
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(TypeAdminRequest $request, Type $type)
    {
        try {

            $attributes = $request->all();

            $type->update($attributes);

            return response()->json([
                'message' => trans('messages.success.updated', ['Module' => trans('knowledge_type.name')]),
                'code' => 204,
                'redirect' => trans_url('/admin/knowledge/type/' . $type->getRouteKey())
            ], 201);

        } catch (Exception $e) {

            return response()->json([
                'message' => $e->getMessage(),
                'code' => 400,
                'redirect' => trans_url('/admin/knowledge/type/' . $type->getRouteKey()),
            ], 400);

        }
    }

    /**
     * @param TypeAdminRequest $request
     * @param Type $type
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(TypeAdminRequest $request, Type $type)
    {

        try {

            $t = $type->delete();

            return response()->json([
                'message' => trans('messages.success.deleted', ['Module' => trans('knowledge_type.name')]),
                'code' => 204,
                'redirect' => trans_url('/admin/knowledge/type/0'),
            ], 201);

        } catch (Exception $e) {

            return response()->json([
                'message' => $e->getMessage(),
                'code' => 400,
                'redirect' => trans_url('/admin/knowledge/type/' . $type->getRouteKey()),
            ], 400);
        }
    }
}
