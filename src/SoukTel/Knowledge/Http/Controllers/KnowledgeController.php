<?php

namespace SoukTel\Knowledge\Http\Controllers;

use Illuminate\Http\Request;
use SoukTel\Forum\Models\Post;
use SoukTel\Knowledge\Interfaces\CategoryRepositoryInterface;
use SoukTel\Knowledge\Interfaces\KnowledgeRepositoryInterface;
use SoukTel\Knowledge\Models\Knowledge;

class KnowledgeController extends KnowledgeBaseController
{

    /**
     * KnowledgeController constructor.
     * @param KnowledgeRepositoryInterface $knowledge
     * @param CategoryRepositoryInterface $category
     */
    public function __construct(KnowledgeRepositoryInterface $knowledge, CategoryRepositoryInterface $category)
    {
        $this->middleware('web');
        $this->setupTheme(config('theme.themes.public.theme'), config('theme.themes.public.layout'));
        $this->repository = $knowledge;
        $this->category = $category;
        parent::__construct();
    }

    /**
     * @return mixed
     */
    protected function index()
    {
        $this->theme->asset()->container('footer')->add('atvimg', 'packages/atvImg/atvImg-min.js');
        $this->theme->asset()->container('footer')->add('isotop', 'packages/isotope/isotope.pkgd.min.js');

        $knowledges = $this->repository
            ->pushCriteria(new \SoukTel\Knowledge\Repositories\Criteria\KnowledgePublicCriteria())
            ->scopeQuery(function ($query) {
                return $query->with('category')->orderBy('title', 'ASC');
            })->all();

        $categorys = $this->category
            ->pushCriteria(new \SoukTel\Knowledge\Repositories\Criteria\CategoryPublicCriteria())
            ->scopeQuery(function ($query) {
                return $query->orderBy('name', 'ASC');
            })->all();

        return $this->theme->of('knowledge::public.knowledge.index', compact('knowledges', 'categorys'))->render();
    }

    public function download(Knowledge $knowledge, $document_id = 0, $post_id = 0)
    {
        $can_download = $this->canDownload($knowledge, $post_id);

        if (!$can_download) {
            abort(404);
        }

        if ($knowledge->documents()->count()) {
            if ($document_id) {
                $document = $knowledge->documents()->whereId($document_id)->first();
            } else {
                $document = $knowledge->documents()->whereLocale(\App::getLocale())->first();
            }
            if (!$document) {
                $document = $knowledge->documents()->first();
            }

            $knowledge->downloads += 1;

            \SoukTel\Knowledge\Facades\Document::log_download($document);

            $knowledge->save();
            return response()->download($document->getFullPath(), $document->original_name);
        }
    }

    private function canDownload($knowledge, $post_id = 0)
    {
        if (user('admin.web')) {
            return true;
        }
        $user = user('web');

        $can_download = false;

        $published = in_array($knowledge->status, ['Approved', 'Unmoderated']) && $knowledge->getOriginal('published') == 1;
        $public = $knowledge->getOriginal('public') == 1;

        if ($published && $public) {
            // knowledge valid status, published and public
            $can_download = true;
        } elseif ($user) {
            if ($knowledge->author_id == $user->id) {
                // knowledge author
                $can_download = true;
            } elseif ($post_id) {
                $post = Post::find($post_id);
                if ($post) {
                    $forum = $post->forum;
                    $can_download = $forum->users->contains($user->id);
                }
            } elseif (($knowledge->sharingCountries()->where('id', $user->country_id)->first()
                    || $knowledge->sharingRoles()->whereIn('id', $user->getRoles->pluck('id')->toArray())->first()
                    || $knowledge->sharingUsers()->where('id', $user->id)->first()) && $published
            ) {
                // knowledge shared with user
                $can_download = true;
            }
        }

        return $can_download;
    }


    public function getKnowledgeList(Request $request, $research = false)
    {
        $attributes = $request->all();

        $page_count = config('package.knowledge.knowledge.perPage');

        $view = 'knowledge::user.knowledge.partial.documents_list';

        if ($research && $research == 'research') {
            $research = 1;
            $view = 'knowledge::user.knowledge.partial.research_list';
        }

        $knowledge_list = Knowledge::where(function ($query) use ($research, $attributes) {
            $query = $this->filterQuery($query, $attributes);
            $query = $query->whereIn('status', ['Approved', 'Unmoderated'])
                ->wherePublic(1)->whereResearch($research)->wherePublished(1);

            return $query;
        });

        if (isset($attributes['search']) && !empty($attributes['search'])) {
            $search_expression = $this->getSearchExpression($attributes['search']);

            $knowledge_list = $knowledge_list->selectRaw("knowledge.*,
                match(title,description,research_author,research_institution) against (" . \DB::getPdo()->quote($search_expression) . " in boolean mode) as title_search,
                match(title,description,body,research_author,research_institution) against (" . \DB::getPdo()->quote($search_expression) . " in boolean mode) as full_search
                ")->orderBy('title_search', 'desc')->orderBy('full_search', 'desc')->paginate($page_count);
        } else {
            $knowledge_list = $knowledge_list->orderBy('knowledge.id', 'desc')->paginate($page_count);
        }
        $public = true;
        return response()->view($view, compact('knowledge_list', 'public'));
    }
}
