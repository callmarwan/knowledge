<?php

namespace SoukTel\Knowledge\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use Form;
use SoukTel\Knowledge\Http\Requests\TagAdminRequest;
use SoukTel\Knowledge\Interfaces\TagRepositoryInterface;
use SoukTel\Knowledge\Models\Tag;

/**
 * Admin web controller class.
 */
class TagAdminController extends BaseController
{
    /**
     * The authentication guard that should be used.
     *
     * @var string
     */
    public $guard = 'admin.web';

    /**
     * Initialize tag controller.
     *
     * @param type TagRepositoryInterface $tag
     *
     * @return type
     */
    public $home = 'admin';

    public function __construct(TagRepositoryInterface $tag)
    {
        $this->middleware('web');
        $this->middleware('auth:admin.web');
        $this->setupTheme(config('theme.themes.admin.theme'), config('theme.themes.admin.layout'));
        $this->repository = $tag;
        parent::__construct();
    }

    /**
     * Display a list of tag.
     *
     * @return Response
     */
    public function index(TagAdminRequest $request)
    {
        $pageLimit = $request->input('pageLimit');
        if ($request->wantsJson()) {
            $tags = $this->repository
                ->setPresenter('\\SoukTel\\Knowledge\\Repositories\\Presenter\\TagListPresenter')
                ->scopeQuery(function ($query) {
                    return $query->orderBy('id', 'DESC');
                })->paginate($pageLimit);

            $tags['recordsTotal'] = $tags['meta']['pagination']['total'];
            $tags['recordsFiltered'] = $tags['meta']['pagination']['total'];
            $tags['request'] = $request->all();
            return response()->json($tags, 200);

        }

        $this->theme->prependTitle(trans('knowledge_tag.names') . ' :: ');
        return $this->theme->of('knowledge::admin.tag.index')->render();
    }

    /**
     * @param TagAdminRequest $request
     * @param Tag $tag
     * @return \Illuminate\Http\Response
     */
    public function show(TagAdminRequest $request, Tag $tag)
    {
        if (!$tag->exists) {
            return response()->view('knowledge::admin.tag.new', compact('tag'));
        }

        Form::populate($tag);
        return response()->view('knowledge::admin.tag.show', compact('tag'));
    }

    /**
     * @param TagAdminRequest $request
     * @return \Illuminate\Http\Response
     */
    public function create(TagAdminRequest $request)
    {

        $tag = $this->repository->newInstance([]);

        Form::populate($tag);

        return response()->view('knowledge::admin.tag.create', compact('tag'));

    }

    /**
     * @param TagAdminRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(TagAdminRequest $request)
    {
        try {
            $attributes = $request->all();

            $tag = $this->repository->create($attributes);

            return response()->json([
                'message' => trans('messages.success.updated', ['Module' => trans('knowledge_tag.name')]),
                'code' => 204,
                'redirect' => trans_url('/admin/knowledge/tag/' . $tag->getRouteKey())
            ], 201);


        } catch (Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'code' => 400,
            ], 400);
        }
    }

    /**
     * @param TagAdminRequest $request
     * @param Tag $tag
     * @return \Illuminate\Http\Response
     */
    public function edit(TagAdminRequest $request, Tag $tag)
    {
        Form::populate($tag);
        return response()->view('knowledge::admin.tag.edit', compact('tag'));
    }

    /**
     * @param TagAdminRequest $request
     * @param Tag $tag
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(TagAdminRequest $request, Tag $tag)
    {
        try {

            $attributes = $request->all();

            $tag->update($attributes);

            return response()->json([
                'message' => trans('messages.success.updated', ['Module' => trans('knowledge_tag.name')]),
                'code' => 204,
                'redirect' => trans_url('/admin/knowledge/tag/' . $tag->getRouteKey())
            ], 201);

        } catch (Exception $e) {

            return response()->json([
                'message' => $e->getMessage(),
                'code' => 400,
                'redirect' => trans_url('/admin/knowledge/tag/' . $tag->getRouteKey()),
            ], 400);

        }
    }

    /**
     * @param TagAdminRequest $request
     * @param Tag $tag
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(TagAdminRequest $request, Tag $tag)
    {

        try {

            $t = $tag->delete();

            return response()->json([
                'message' => trans('messages.success.deleted', ['Module' => trans('knowledge_tag.name')]),
                'code' => 204,
                'redirect' => trans_url('/admin/knowledge/tag/0'),
            ], 201);

        } catch (Exception $e) {

            return response()->json([
                'message' => $e->getMessage(),
                'code' => 400,
                'redirect' => trans_url('/admin/knowledge/tag/' . $tag->getRouteKey()),
            ], 400);
        }
    }
}
