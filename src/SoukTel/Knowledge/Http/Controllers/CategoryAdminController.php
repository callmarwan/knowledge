<?php

namespace SoukTel\Knowledge\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use App\Http\Requests\Request;
use Form;
use SoukTel\Knowledge\Http\Requests\CategoryAdminRequest;
use SoukTel\Knowledge\Interfaces\CategoryRepositoryInterface;
use SoukTel\Knowledge\Models\Category;
use SoukTel\Knowledge\Models\SubTheme;

/**
 * Admin web controller class.
 */
class CategoryAdminController extends BaseController
{
    /**
     * The authentication guard that should be used.
     *
     * @var string
     */
    public $guard = 'admin.web';

    /**
     * Initialize category controller.
     *
     * @param type CategoryRepositoryInterface $category
     *
     * @return type
     */
    public $home = 'admin';

    public function __construct(CategoryRepositoryInterface $category)
    {
        $this->middleware('web');
        $this->middleware('auth:admin.web');
        $this->setupTheme(config('theme.themes.admin.theme'), config('theme.themes.admin.layout'));
        $this->repository = $category;
        parent::__construct();
    }

    /**
     * Display a list of category.
     *
     * @return Response
     */
    public function index(CategoryAdminRequest $request)
    {
        $pageLimit = $request->input('pageLimit');
        if ($request->wantsJson()) {
            $categorys = $this->repository
                ->setPresenter('\\SoukTel\\Knowledge\\Repositories\\Presenter\\CategoryListPresenter')
                ->scopeQuery(function ($query) {
                    return $query->orderBy('id', 'DESC');
                })->paginate($pageLimit);

            $categorys['recordsTotal'] = $categorys['meta']['pagination']['total'];
            $categorys['recordsFiltered'] = $categorys['meta']['pagination']['total'];
            $categorys['request'] = $request->all();
            return response()->json($categorys, 200);

        }

        $this->theme->prependTitle(trans('knowledge_category.names') . ' :: ');
        return $this->theme->of('knowledge::admin.category.index')->render();
    }

    /**
     * @param CategoryAdminRequest $request
     * @param Category $category
     * @return \Illuminate\Http\Response
     */
    public function show(CategoryAdminRequest $request, Category $category)
    {
        if (!$category->exists) {
            return response()->view('knowledge::admin.category.new', compact('category'));
        }

        Form::populate($category);
        return response()->view('knowledge::admin.category.show', compact('category'));
    }

    /**
     * @param CategoryAdminRequest $request
     * @return \Illuminate\Http\Response
     */
    public function create(CategoryAdminRequest $request)
    {

        $category = $this->repository->newInstance([]);

        Form::populate($category);

        return response()->view('knowledge::admin.category.create', compact('category'));

    }

    /**
     * @param CategoryAdminRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CategoryAdminRequest $request)
    {
        try {
            $attributes = $request->all();
            $attributes['user_id'] = user_id('admin.web');
            $attributes['user_type'] = user_type();
            $category = $this->repository->create($attributes);

            return response()->json([
                'message' => trans('messages.success.updated', ['Module' => trans('knowledge_category.name')]),
                'code' => 204,
                'redirect' => trans_url('/admin/knowledge/category/' . $category->getRouteKey())
            ], 201);


        } catch (Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'code' => 400,
            ], 400);
        }
    }

    /**
     * @param CategoryAdminRequest $request
     * @param Category $category
     * @return \Illuminate\Http\Response
     */
    public function edit(CategoryAdminRequest $request, Category $category)
    {
        Form::populate($category);
        return response()->view('knowledge::admin.category.edit', compact('category'));
    }

    /**
     * @param CategoryAdminRequest $request
     * @param Category $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CategoryAdminRequest $request, Category $category)
    {
        try {

            $attributes = $request->all();

            $category->update($attributes);

            return response()->json([
                'message' => trans('messages.success.updated', ['Module' => trans('knowledge_category.name')]),
                'code' => 204,
                'redirect' => trans_url('/admin/knowledge/category/' . $category->getRouteKey())
            ], 201);

        } catch (Exception $e) {

            return response()->json([
                'message' => $e->getMessage(),
                'code' => 400,
                'redirect' => trans_url('/admin/knowledge/category/' . $category->getRouteKey()),
            ], 400);

        }
    }

    /**
     * @param CategoryAdminRequest $request
     * @param Category $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(CategoryAdminRequest $request, Category $category)
    {

        try {

            $t = $category->delete();

            return response()->json([
                'message' => trans('messages.success.deleted', ['Module' => trans('knowledge_category.name')]),
                'code' => 204,
                'redirect' => trans_url('/admin/knowledge/category/0'),
            ], 201);

        } catch (Exception $e) {

            return response()->json([
                'message' => $e->getMessage(),
                'code' => 400,
                'redirect' => trans_url('/admin/knowledge/category/' . $category->getRouteKey()),
            ], 400);
        }
    }
    public function getSubThemes(\Illuminate\Http\Request $request){
        $attributes = $request->all();

        $themesIds = isset($attributes['ids']) ? $attributes['ids'] : [];
        $subThemes = SubTheme::whereIn('category_id' , $themesIds)->get()->pluck('name', 'id' );
        return response()->json($subThemes->toArray(), 200);
    }

}
