<?php

namespace SoukTel\Knowledge\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use SoukTel\Knowledge\Interfaces\CountryRepositoryInterface;
use SoukTel\Knowledge\Repositories\Presenter\CountryItemTransformer;

/**
 * Pubic API controller class.
 */
class CountryApiController extends BaseController
{
   
    
    /**
     * Constructor.
     *
     * @param type \SoukTel\Country\Interfaces\CountryRepositoryInterface $country
     *
     * @return type
     */
    public function __construct(CountryRepositoryInterface $country)
    {
        $this->middleware('api');
        $this->repository = $country;
        parent::__construct();
    }

    /**
     * Show country's list.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function index()
    {
        $countrys = $this->repository
            ->setPresenter('\\SoukTel\\Knowledge\\Repositories\\Presenter\\CountryListPresenter')
            ->scopeQuery(function($query){
                return $query->orderBy('id','DESC');
            })->paginate();

        $countrys['code'] = 2000;
        return response()->json($countrys)
                ->setStatusCode(200, 'INDEX_SUCCESS');
    }

    /**
     * Show country.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function show($slug)
    {
        $country = $this->repository
            ->scopeQuery(function($query) use ($slug) {
            return $query->orderBy('id','DESC')
                         ->where('slug', $slug);
        })->first(['*']);

        if (!is_null($country)) {
            $country         = $this->itemPresenter($module, new CountryItemTransformer);
            $country['code'] = 2001;
            return response()->json($country)
                ->setStatusCode(200, 'SHOW_SUCCESS');
        } else {
            return response()->json([])
                ->setStatusCode(400, 'SHOW_ERROR');
        }

    }
}
