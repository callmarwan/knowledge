<?php

namespace SoukTel\Knowledge\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use Form;
use SoukTel\Knowledge\Http\Requests\ResearchInstrumentAdminRequest;
use SoukTel\Knowledge\Interfaces\ResearchInstrumentRepositoryInterface;
use SoukTel\Knowledge\Models\ResearchInstrument;

/**
 * Admin web controller class.
 */
class ResearchInstrumentAdminController extends BaseController
{
    /**
     * The authentication guard that should be used.
     *
     * @var string
     */
    public $guard = 'admin.web';

    /**
     * Initialize research_instrument controller.
     *
     * @param type ResearchInstrumentRepositoryInterface $research_instrument
     *
     * @return type
     */
    public $home = 'admin';

    public function __construct(ResearchInstrumentRepositoryInterface $research_instrument)
    {
        $this->middleware('web');
        $this->middleware('auth:admin.web');
        $this->setupTheme(config('theme.themes.admin.theme'), config('theme.themes.admin.layout'));
        $this->repository = $research_instrument;
        parent::__construct();
    }

    /**
     * Display a list of research_instrument.
     *
     * @return Response
     */
    public function index(ResearchInstrumentAdminRequest $request)
    {
        $pageLimit = $request->input('pageLimit');
        if ($request->wantsJson()) {
            $research_instruments = $this->repository
                ->setPresenter('\\SoukTel\\Knowledge\\Repositories\\Presenter\\ResearchInstrumentListPresenter')
                ->scopeQuery(function ($query) {
                    return $query->orderBy('id', 'DESC');
                })->paginate($pageLimit);

            $research_instruments['recordsTotal'] = $research_instruments['meta']['pagination']['total'];
            $research_instruments['recordsFiltered'] = $research_instruments['meta']['pagination']['total'];
            $research_instruments['request'] = $request->all();
            return response()->json($research_instruments, 200);

        }

        $this->theme->prependTitle(trans('knowledge_research_instrument.names') . ' :: ');
        return $this->theme->of('knowledge::admin.research_instrument.index')->render();
    }

    /**
     * @param ResearchInstrumentAdminRequest $request
     * @param ResearchInstrument $research_instrument
     * @return \Illuminate\Http\Response
     */
    public function show(ResearchInstrumentAdminRequest $request, ResearchInstrument $research_instrument)
    {
        if (!$research_instrument->exists) {
            return response()->view('knowledge::admin.research_instrument.new', compact('research_instrument'));
        }

        Form::populate($research_instrument);
        return response()->view('knowledge::admin.research_instrument.show', compact('research_instrument'));
    }

    /**
     * @param ResearchInstrumentAdminRequest $request
     * @return \Illuminate\Http\Response
     */
    public function create(ResearchInstrumentAdminRequest $request)
    {

        $research_instrument = $this->repository->newInstance([]);

        Form::populate($research_instrument);

        return response()->view('knowledge::admin.research_instrument.create', compact('research_instrument'));

    }

    /**
     * @param ResearchInstrumentAdminRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ResearchInstrumentAdminRequest $request)
    {
        try {
            $attributes = $request->all();

            $research_instrument = $this->repository->create($attributes);

            return response()->json([
                'message' => trans('messages.success.updated', ['Module' => trans('knowledge_research_instrument.name')]),
                'code' => 204,
                'redirect' => trans_url('/admin/knowledge/research_instrument/' . $research_instrument->getRouteKey())
            ], 201);


        } catch (Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'code' => 400,
            ], 400);
        }
    }

    /**
     * @param ResearchInstrumentAdminRequest $request
     * @param ResearchInstrument $research_instrument
     * @return \Illuminate\Http\Response
     */
    public function edit(ResearchInstrumentAdminRequest $request, ResearchInstrument $research_instrument)
    {
        Form::populate($research_instrument);
        return response()->view('knowledge::admin.research_instrument.edit', compact('research_instrument'));
    }

    /**
     * @param ResearchInstrumentAdminRequest $request
     * @param ResearchInstrument $research_instrument
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ResearchInstrumentAdminRequest $request, ResearchInstrument $research_instrument)
    {
        try {

            $attributes = $request->all();

            $research_instrument->update($attributes);

            return response()->json([
                'message' => trans('messages.success.updated', ['Module' => trans('knowledge_research_instrument.name')]),
                'code' => 204,
                'redirect' => trans_url('/admin/knowledge/research_instrument/' . $research_instrument->getRouteKey())
            ], 201);

        } catch (Exception $e) {

            return response()->json([
                'message' => $e->getMessage(),
                'code' => 400,
                'redirect' => trans_url('/admin/knowledge/research_instrument/' . $research_instrument->getRouteKey()),
            ], 400);

        }
    }

    /**
     * @param ResearchInstrumentAdminRequest $request
     * @param ResearchInstrument $research_instrument
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(ResearchInstrumentAdminRequest $request, ResearchInstrument $research_instrument)
    {

        try {

            $t = $research_instrument->delete();

            return response()->json([
                'message' => trans('messages.success.deleted', ['Module' => trans('knowledge_research_instrument.name')]),
                'code' => 204,
                'redirect' => trans_url('/admin/knowledge/research_instrument/0'),
            ], 201);

        } catch (Exception $e) {

            return response()->json([
                'message' => $e->getMessage(),
                'code' => 400,
                'redirect' => trans_url('/admin/knowledge/research_instrument/' . $research_instrument->getRouteKey()),
            ], 400);
        }
    }
}
