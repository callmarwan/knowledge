<?php

namespace SoukTel\Knowledge\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use SoukTel\Knowledge\Interfaces\KnowledgeRepositoryInterface;
use SoukTel\Knowledge\Repositories\Presenter\KnowledgeItemTransformer;

/**
 * Pubic API controller class.
 */
class KnowledgeApiController extends BaseController
{
     /**
     * Constructor.
     *
     * @param type \SoukTel\Knowledge\Interfaces\KnowledgeRepositoryInterface $knowledge
     *
     * @return type
     */
    public function __construct(KnowledgeRepositoryInterface $knowledge)
    {
        $this->middleware('api');
         $this->repository = $knowledge;
        parent::__construct();
    }

    /**
     * Show knowledge's list.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function index()
    {
        $knowledges = $this->repository
            ->setPresenter('\\SoukTel\\Knowledge\\Repositories\\Presenter\\KnowledgeListPresenter')
            ->scopeQuery(function($query){
                return $query->orderBy('id','DESC');
            })->paginate();

        $knowledges['code'] = 2000;
        return response()->json($knowledges)
                ->setStatusCode(200, 'INDEX_SUCCESS');
    }

    /**
     * Show knowledge.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function show($slug)
    {
        $knowledge = $this->repository
            ->scopeQuery(function($query) use ($slug) {
            return $query->orderBy('id','DESC')
                         ->where('slug', $slug);
        })->first(['*']);

        if (!is_null($knowledge)) {
            $knowledge         = $this->itemPresenter($module, new KnowledgeItemTransformer);
            $knowledge['code'] = 2001;
            return response()->json($knowledge)
                ->setStatusCode(200, 'SHOW_SUCCESS');
        } else {
            return response()->json([])
                ->setStatusCode(400, 'SHOW_ERROR');
        }

    }
}
