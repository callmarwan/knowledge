<?php

namespace SoukTel\Knowledge\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use SoukTel\Knowledge\Http\Requests\CountryAdminApiRequest;
use SoukTel\Knowledge\Interfaces\CountryRepositoryInterface;
use SoukTel\Knowledge\Models\Country;

/**
 * Admin API controller class.
 */
class CountryAdminApiController extends BaseController
{


    /**
     * Initialize country controller.
     *
     * @param type CountryRepositoryInterface $country
     *
     * @return type
     */
    protected $guard = 'admin.api';

    public function __construct(CountryRepositoryInterface $country)
    {
        $this->middleware('api');
        $this->middleware('jwt.auth:admin.api');
        $this->setupTheme(config('theme.themes.admin.theme'), config('theme.themes.admin.layout'));
        $this->repository = $country;
        parent::__construct();
    }

    /**
     * Display a list of country.
     *
     * @return json
     */
    public function index(CountryAdminApiRequest $request)
    {
        $countrys = $this->repository
            ->setPresenter('\\SoukTel\\Knowledge\\Repositories\\Presenter\\CountryListPresenter')
            ->scopeQuery(function ($query) {
                return $query->orderBy('id', 'DESC');
            })->all();
        $countrys['code'] = 2000;
        return response()->json($countrys)
            ->setStatusCode(200, 'INDEX_SUCCESS');

    }

    /**
     * Display country.
     *
     * @param Request $request
     * @param Model   Country
     *
     * @return Json
     */
    public function show(CountryAdminApiRequest $request, Country $country)
    {
        $country = $country->presenter();
        $country['code'] = 2001;
        return response()->json($country)
            ->setStatusCode(200, 'SHOW_SUCCESS');;

    }

    /**
     * Show the form for creating a new country.
     *
     * @param Request $request
     *
     * @return json
     */
    public function create(CountryAdminApiRequest $request, Country $country)
    {
        $country = $country->presenter();
        $country['code'] = 2002;
        return response()->json($country)
            ->setStatusCode(200, 'CREATE_SUCCESS');

    }

    /**
     * @param CountryAdminApiRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function store(CountryAdminApiRequest $request)
    {
        try {
            $attributes = $request->all();
            $attributes['user_id'] = user_id('admin.api');
            $attributes['user_type'] = user_type();
            $country = $this->repository->create($attributes);
            $country = $country->presenter();
            $country['code'] = 2004;

            return response()->json($country)
                ->setStatusCode(201, 'STORE_SUCCESS');
        } catch (Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'code' => 4004,
            ])->setStatusCode(400, 'STORE_ERROR');;
        }
    }

    /**
     * Show country for editing.
     *
     * @param Request $request
     * @param Model $country
     *
     * @return json
     */
    public function edit(CountryAdminApiRequest $request, Country $country)
    {
        $country = $country->presenter();
        $country['code'] = 2003;
        return response()->json($country)
            ->setStatusCode(200, 'EDIT_SUCCESS');;
    }

    /**
     * Update the country.
     *
     * @param Request $request
     * @param Model $country
     *
     * @return json
     */
    public function update(CountryAdminApiRequest $request, Country $country)
    {
        try {

            $attributes = $request->all();

            $country->update($attributes);
            $country = $country->presenter();
            $country['code'] = 2005;

            return response()->json($country)
                ->setStatusCode(201, 'UPDATE_SUCCESS');


        } catch (Exception $e) {

            return response()->json([
                'message' => $e->getMessage(),
                'code' => 4005,
            ])->setStatusCode(400, 'UPDATE_ERROR');

        }
    }

    /**
     * Remove the country.
     *
     * @param Request $request
     * @param Model $country
     *
     * @return json
     */
    public function destroy(CountryAdminApiRequest $request, Country $country)
    {

        try {

            $t = $country->delete();

            return response()->json([
                'message' => trans('messages.success.delete', ['Module' => trans('knowledge_country.name')]),
                'code' => 2006
            ])->setStatusCode(202, 'DESTROY_SUCCESS');

        } catch (Exception $e) {

            return response()->json([
                'message' => $e->getMessage(),
                'code' => 4006,
            ])->setStatusCode(400, 'DESTROY_ERROR');
        }
    }
}
