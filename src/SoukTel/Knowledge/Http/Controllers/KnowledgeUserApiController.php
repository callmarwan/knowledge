<?php

namespace SoukTel\Knowledge\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use SoukTel\Knowledge\Http\Requests\KnowledgeUserApiRequest;
use SoukTel\Knowledge\Interfaces\KnowledgeRepositoryInterface;
use SoukTel\Knowledge\Models\Knowledge;

/**
 * User API controller class.
 */
class KnowledgeUserApiController extends BaseController
{
    /**
     * Initialize knowledge controller.
     *
     * @param type KnowledgeRepositoryInterface $knowledge
     *
     * @return type
     */
    protected $guard = 'api';

    public function __construct(KnowledgeRepositoryInterface $knowledge)
    {
        $this->middleware('api');
        $this->middleware('jwt.auth:api');
        $this->setupTheme(config('theme.themes.user.theme'), config('theme.themes.user.layout'));
         $this->repository = $knowledge;
        parent::__construct();
    }

    /**
     * Display a list of knowledge.
     *
     * @return json
     */
    public function index(KnowledgeUserApiRequest $request)
    {
        $knowledges  = $this->repository
            ->pushCriteria(new \SoukTel\Knowledge\Repositories\Criteria\KnowledgeUserCriteria())
            ->setPresenter('\\SoukTel\\Knowledge\\Repositories\\Presenter\\KnowledgeListPresenter')
            ->scopeQuery(function($query){
                return $query->orderBy('id','DESC');
            })->all();
        $knowledges['code'] = 2000;
        return response()->json($knowledges) 
            ->setStatusCode(200, 'INDEX_SUCCESS');

    }

    /**
     * Display knowledge.
     *
     * @param Request $request
     * @param Model   Knowledge
     *
     * @return Json
     */
    public function show(KnowledgeUserApiRequest $request, Knowledge $knowledge)
    {

        if ($knowledge->exists) {
            $knowledge         = $knowledge->presenter();
            $knowledge['code'] = 2001;
            return response()->json($knowledge)
                ->setStatusCode(200, 'SHOW_SUCCESS');;
        } else {
            return response()->json([])
                ->setStatusCode(400, 'SHOW_ERROR');
        }

    }

    /**
     * Show the form for creating a new knowledge.
     *
     * @param Request $request
     *
     * @return json
     */
    public function create(KnowledgeUserApiRequest $request, Knowledge $knowledge)
    {
        $knowledge         = $knowledge->presenter();
        $knowledge['code'] = 2002;
        return response()->json($knowledge)
            ->setStatusCode(200, 'CREATE_SUCCESS');
    }

    /**
     * Create new knowledge.
     *
     * @param Request $request
     *
     * @return json
     */
    public function store(KnowledgeUserApiRequest $request)
    {
        try {
            $attributes             = $request->all();
            $attributes['user_id']  = user_id('admin.api');
            $attributes['user_type'] = user_type();
            $knowledge          = $this->repository->create($attributes);
            $knowledge          = $knowledge->presenter();
            $knowledge['code']  = 2004;

            return response()->json($knowledge)
                ->setStatusCode(201, 'STORE_SUCCESS');
        } catch (Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'code'    => 4004,
            ])->setStatusCode(400, 'STORE_ERROR');
        }

    }

    /**
     * Show knowledge for editing.
     *
     * @param Request $request
     * @param Model   $knowledge
     *
     * @return json
     */
    public function edit(KnowledgeUserApiRequest $request, Knowledge $knowledge)
    {
        if ($knowledge->exists) {
            $knowledge         = $knowledge->presenter();
            $knowledge['code'] = 2003;
            return response()-> json($knowledge)
                ->setStatusCode(200, 'EDIT_SUCCESS');;
        } else {
            return response()->json([])
                ->setStatusCode(400, 'SHOW_ERROR');
        }
    }

    /**
     * Update the knowledge.
     *
     * @param Request $request
     * @param Model   $knowledge
     *
     * @return json
     */
    public function update(KnowledgeUserApiRequest $request, Knowledge $knowledge)
    {
        try {

            $attributes = $request->all();

            $knowledge->update($attributes);
            $knowledge         = $knowledge->presenter();
            $knowledge['code'] = 2005;

            return response()->json($knowledge)
                ->setStatusCode(201, 'UPDATE_SUCCESS');


        } catch (Exception $e) {

            return response()->json([
                'message'  => $e->getMessage(),
                'code'     => 4005,
            ])->setStatusCode(400, 'UPDATE_ERROR');

        }
    }

    /**
     * Remove the knowledge.
     *
     * @param Request $request
     * @param Model   $knowledge
     *
     * @return json
     */
    public function destroy(KnowledgeUserApiRequest $request, Knowledge $knowledge)
    {

        try {

            $t = $knowledge->delete();

            return response()->json([
                'message'  => trans('messages.success.delete', ['Module' => trans('knowledge.name')]),
                'code'     => 2006
            ])->setStatusCode(202, 'DESTROY_SUCCESS');

        } catch (Exception $e) {

            return response()->json([
                'message'  => $e->getMessage(),
                'code'     => 4006,
            ])->setStatusCode(400, 'DESTROY_ERROR');
        }
    }
}
