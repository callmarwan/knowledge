<?php

namespace SoukTel\Knowledge\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use SoukTel\Knowledge\Http\Requests\TagAdminApiRequest;
use SoukTel\Knowledge\Interfaces\TagRepositoryInterface;
use SoukTel\Knowledge\Models\Tag;

/**
 * Admin API controller class.
 */
class TagAdminApiController extends BaseController
{


    /**
     * Initialize tag controller.
     *
     * @param type TagRepositoryInterface $tag
     *
     * @return type
     */
    protected $guard = 'admin.api';

    public function __construct(TagRepositoryInterface $tag)
    {
        $this->middleware('api');
        $this->middleware('jwt.auth:admin.api');
        $this->setupTheme(config('theme.themes.admin.theme'), config('theme.themes.admin.layout'));
        $this->repository = $tag;
        parent::__construct();
    }

    /**
     * Display a list of tag.
     *
     * @return json
     */
    public function index(TagAdminApiRequest $request)
    {
        $tags = $this->repository
            ->setPresenter('\\SoukTel\\Knowledge\\Repositories\\Presenter\\TagListPresenter')
            ->scopeQuery(function ($query) {
                return $query->orderBy('id', 'DESC');
            })->all();
        $tags['code'] = 2000;
        return response()->json($tags)
            ->setStatusCode(200, 'INDEX_SUCCESS');

    }

    /**
     * Display tag.
     *
     * @param Request $request
     * @param Model   Tag
     *
     * @return Json
     */
    public function show(TagAdminApiRequest $request, Tag $tag)
    {
        $tag = $tag->presenter();
        $tag['code'] = 2001;
        return response()->json($tag)
            ->setStatusCode(200, 'SHOW_SUCCESS');;

    }

    /**
     * Show the form for creating a new tag.
     *
     * @param Request $request
     *
     * @return json
     */
    public function create(TagAdminApiRequest $request, Tag $tag)
    {
        $tag = $tag->presenter();
        $tag['code'] = 2002;
        return response()->json($tag)
            ->setStatusCode(200, 'CREATE_SUCCESS');

    }

    /**
     * Create new tag.
     *
     * @param Request $request
     *
     * @return json
     */
    public function store(TagAdminApiRequest $request)
    {
        try {
            $attributes = $request->all();
            $attributes['user_id'] = user_id('admin.api');
            $attributes['user_type'] = user_type();
            $tag = $this->repository->create($attributes);
            $tag = $tag->presenter();
            $tag['code'] = 2004;

            return response()->json($tag)
                ->setStatusCode(201, 'STORE_SUCCESS');
        } catch (Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'code' => 4004,
            ])->setStatusCode(400, 'STORE_ERROR');
        }
    }

    /**
     * Show tag for editing.
     *
     * @param Request $request
     * @param Model $tag
     *
     * @return json
     */
    public function edit(TagAdminApiRequest $request, Tag $tag)
    {
        $tag = $tag->presenter();
        $tag['code'] = 2003;
        return response()->json($tag)
            ->setStatusCode(200, 'EDIT_SUCCESS');;
    }

    /**
     * Update the tag.
     *
     * @param Request $request
     * @param Model $tag
     *
     * @return json
     */
    public function update(TagAdminApiRequest $request, Tag $tag)
    {
        try {

            $attributes = $request->all();

            $tag->update($attributes);
            $tag = $tag->presenter();
            $tag['code'] = 2005;

            return response()->json($tag)
                ->setStatusCode(201, 'UPDATE_SUCCESS');


        } catch (Exception $e) {

            return response()->json([
                'message' => $e->getMessage(),
                'code' => 4005,
            ])->setStatusCode(400, 'UPDATE_ERROR');

        }
    }

    /**
     * Remove the tag.
     *
     * @param Request $request
     * @param Model $tag
     *
     * @return json
     */
    public function destroy(TagAdminApiRequest $request, Tag $tag)
    {

        try {

            $t = $tag->delete();

            return response()->json([
                'message' => trans('messages.success.delete', ['Module' => trans('knowledge_tag.name')]),
                'code' => 2006
            ])->setStatusCode(202, 'DESTROY_SUCCESS');

        } catch (Exception $e) {

            return response()->json([
                'message' => $e->getMessage(),
                'code' => 4006,
            ])->setStatusCode(400, 'DESTROY_ERROR');
        }
    }
}
