<?php

namespace SoukTel\Knowledge\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use SoukTel\Knowledge\Http\Requests\CountryUserApiRequest;
use SoukTel\Knowledge\Interfaces\CountryRepositoryInterface;
use SoukTel\Knowledge\Models\Country;

/**
 * User API controller class.
 */
class CountryUserApiController extends BaseController
{

    /**
     * Initialize country controller.
     *
     * @param type CountryRepositoryInterface $country
     *
     * @return type
     */
    protected $guard = 'api';

    public function __construct(CountryRepositoryInterface $country)
    {
        $this->middleware('api');
        $this->middleware('jwt.auth:api');
        $this->setupTheme(config('theme.themes.user.theme'), config('theme.themes.user.layout'));
        $this->repository = $country;
        parent::__construct();
    }

    /**
     * Display a list of country.
     *
     * @return json
     */
    public function index(CountryUserApiRequest $request)
    {
        $countrys = $this->repository
            ->pushCriteria(new \SoukTel\Knowledge\Repositories\Criteria\CountryUserCriteria())
            ->setPresenter('\\SoukTel\\Knowledge\\Repositories\\Presenter\\CountryListPresenter')
            ->scopeQuery(function ($query) {
                return $query->orderBy('id', 'DESC');
            })->all();
        $countrys['code'] = 2000;
        return response()->json($countrys)
            ->setStatusCode(200, 'INDEX_SUCCESS');

    }

    /**
     * Display country.
     *
     * @param Request $request
     * @param Model   Country
     *
     * @return Json
     */
    public function show(CountryUserApiRequest $request, Country $country)
    {

        if ($country->exists) {
            $country = $country->presenter();
            $country['code'] = 2001;
            return response()->json($country)
                ->setStatusCode(200, 'SHOW_SUCCESS');;
        } else {
            return response()->json([])
                ->setStatusCode(400, 'SHOW_ERROR');
        }

    }

    /**
     * Show the form for creating a new country.
     *
     * @param Request $request
     *
     * @return json
     */
    public function create(CountryUserApiRequest $request, Country $country)
    {
        $country = $country->presenter();
        $country['code'] = 2002;
        return response()->json($country)
            ->setStatusCode(200, 'CREATE_SUCCESS');
    }

    /**
     * Create new country.
     *
     * @param Request $request
     *
     * @return json
     */
    public function store(CountryUserApiRequest $request)
    {
        try {
            $attributes = $request->all();
            $attributes['user_id'] = user_id('admin.api');
            $attributes['user_type'] = user_type();
            $country = $this->repository->create($attributes);
            $country = $country->presenter();
            $country['code'] = 2004;

            return response()->json($country)
                ->setStatusCode(201, 'STORE_SUCCESS');
        } catch (Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'code' => 4004,
            ])->setStatusCode(400, 'STORE_ERROR');
        }

    }

    /**
     * Show country for editing.
     *
     * @param Request $request
     * @param Model $country
     *
     * @return json
     */
    public function edit(CountryUserApiRequest $request, Country $country)
    {
        if ($country->exists) {
            $country = $country->presenter();
            $country['code'] = 2003;
            return response()->json($country)
                ->setStatusCode(200, 'EDIT_SUCCESS');;
        } else {
            return response()->json([])
                ->setStatusCode(400, 'SHOW_ERROR');
        }
    }

    /**
     * Update the country.
     *
     * @param Request $request
     * @param Model $country
     *
     * @return json
     */
    public function update(CountryUserApiRequest $request, Country $country)
    {
        try {

            $attributes = $request->all();

            $country->update($attributes);
            $country = $country->presenter();
            $country['code'] = 2005;

            return response()->json($country)
                ->setStatusCode(201, 'UPDATE_SUCCESS');


        } catch (Exception $e) {

            return response()->json([
                'message' => $e->getMessage(),
                'code' => 4005,
            ])->setStatusCode(400, 'UPDATE_ERROR');

        }
    }

    /**
     * Remove the country.
     *
     * @param Request $request
     * @param Model $country
     *
     * @return json
     */
    public function destroy(CountryUserApiRequest $request, Country $country)
    {

        try {

            $t = $country->delete();

            return response()->json([
                'message' => trans('messages.success.delete', ['Module' => trans('knowledge_country.name')]),
                'code' => 2006
            ])->setStatusCode(202, 'DESTROY_SUCCESS');

        } catch (Exception $e) {

            return response()->json([
                'message' => $e->getMessage(),
                'code' => 4006,
            ])->setStatusCode(400, 'DESTROY_ERROR');
        }
    }
}
