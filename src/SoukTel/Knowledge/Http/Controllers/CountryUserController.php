<?php

namespace SoukTel\Knowledge\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use Form;
use SoukTel\Knowledge\Http\Requests\CountryUserRequest;
use SoukTel\Knowledge\Interfaces\CountryRepositoryInterface;
use SoukTel\Knowledge\Models\Country;

class CountryUserController extends BaseController
{


    /**
     * The authentication guard that should be used.
     *
     * @var string
     */
    protected $guard = 'web';

    /**
     * Initialize country controller.
     *
     * @param type CountryRepositoryInterface $country
     *
     * @return type
     */
    protected $home = 'home';

    public function __construct(CountryRepositoryInterface $country)
    {
        $this->middleware('web');
        $this->middleware('auth:web');
        $this->middleware('auth.active:web');
        $this->setupTheme(config('theme.themes.user.theme'), config('theme.themes.user.layout'));
        $this->repository = $country;
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(CountryUserRequest $request)
    {
        $this->repository->pushCriteria(new \SoukTel\Knowledge\Repositories\Criteria\CountryUserCriteria());
        $countrys = $this->repository->scopeQuery(function ($query) {
            return $query->orderBy('id', 'DESC');
        })->paginate();

        $this->theme->prependTitle(trans('knowledge_country.names') . ' :: ');

        return $this->theme->of('knowledge::user.country.index', compact('countrys'))->render();
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param Country $country
     *
     * @return Response
     */
    public function show(CountryUserRequest $request, Country $country)
    {
        Form::populate($country);

        return $this->theme->of('knowledge::user.country.show', compact('country'))->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(CountryUserRequest $request)
    {

        $country = $this->repository->newInstance([]);
        Form::populate($country);

        return $this->theme->of('knowledge::user.country.create', compact('country'))->render();
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(CountryUserRequest $request)
    {
        try {
            $attributes = $request->all();
            $attributes['user_id'] = user_id();
            $attributes['user_type'] = user_type();
            $country = $this->repository->create($attributes);

            return redirect(trans_url('/user/knowledge/country'))
                ->with('message', trans('messages.success.created', ['Module' => trans('knowledge_country.name')]))
                ->with('code', 201);

        } catch (Exception $e) {
            redirect()->back()->withInput()->with('message', $e->getMessage())->with('code', 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param Country $country
     *
     * @return Response
     */
    public function edit(CountryUserRequest $request, Country $country)
    {

        Form::populate($country);

        return $this->theme->of('knowledge::user.country.edit', compact('country'))->render();
    }

    /**
     * Update the specified resource.
     *
     * @param Request $request
     * @param Country $country
     *
     * @return Response
     */
    public function update(CountryUserRequest $request, Country $country)
    {
        try {
            $this->repository->update($request->all(), $country->getRouteKey());

            return redirect(trans_url('/user/knowledge/country'))
                ->with('message', trans('messages.success.updated', ['Module' => trans('knowledge_country.name')]))
                ->with('code', 204);

        } catch (Exception $e) {
            redirect()->back()->withInput()->with('message', $e->getMessage())->with('code', 400);
        }
    }

    /**
     * Remove the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy(CountryUserRequest $request, Country $country)
    {
        try {
            $this->repository->delete($country->getRouteKey());
            return redirect(trans_url('/user/knowledge/country'))
                ->with('message', trans('messages.success.deleted', ['Module' => trans('knowledge_country.name')]))
                ->with('code', 204);

        } catch (Exception $e) {

            redirect()->back()->withInput()->with('message', $e->getMessage())->with('code', 400);

        }
    }
}
