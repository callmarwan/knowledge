<?php

namespace SoukTel\Knowledge\Http\Controllers;

use App\User;
use Form;
use Illuminate\Support\Facades\Mail;
use SoukTel\Knowledge\Http\Requests\KnowledgeUserRequest;
use SoukTel\Knowledge\Interfaces\DocumentRepositoryInterface;
use SoukTel\Knowledge\Interfaces\KnowledgeRepositoryInterface;
use SoukTel\Knowledge\Models\Knowledge;
use SoukTel\Knowledge\Models\Tag;

class KnowledgeUserController extends KnowledgeBaseController
{

    /**
     * The authentication guard that should be used.
     *
     * @var string
     */
    protected $guard = 'web';

    /**
     * Initialize knowledge controller.
     *
     * @param type KnowledgeRepositoryInterface $knowledge
     *
     * @return type
     */
    protected $home = 'home';

    protected $document_repo;

    public function __construct(KnowledgeRepositoryInterface $knowledge, DocumentRepositoryInterface $document)
    {
        $this->middleware('web');
        $this->middleware('auth:web');
        $this->middleware('auth.active:web');
        $this->setupTheme(config('theme.themes.user.theme'), config('theme.themes.user.layout'));
        $this->repository = $knowledge;
        $this->document_repo = $document;

        parent::__construct();
    }


    private function setTitle($request)
    {
        $research = false;
        if ($request->is('*research*')) {
            $this->theme->setTitle(trans('user_knowledge.titles.research'));
            $research = true;
        } else {
            $this->theme->setTitle(trans('user_knowledge.titles.documents'));
        }

        return $research;
    }

    /**
     * @param KnowledgeUserRequest $request
     * @return mixed
     */
    public function index(KnowledgeUserRequest $request)
    {
        $research = $this->setTitle($request);

        if ($research) {
            return $this->theme->of('knowledge::user.knowledge.research')->render();
        }

        return $this->theme->of('knowledge::user.knowledge.documents')->render();
    }

    /**
     * @param KnowledgeUserRequest $request
     * @param string $type
     * @param bool $research
     * @return \Illuminate\Http\Response
     */
    public function getKnowledgeList(KnowledgeUserRequest $request, $type = 'my', $research = false)
    {
        $attributes = $request->all();

//        $page_count = 1;
        $page_count = config('package.knowledge.knowledge.perPage');
        $view = 'knowledge::user.knowledge.partial.documents_list';

        if ($research && $research == 'research') {
            $research = 1;
            $view = 'knowledge::user.knowledge.partial.research_list';
        }
        $user = user('web');

        switch ($type) {
            default:
            case 'my':
                $knowledge_list = Knowledge::where(function ($query) use ($user, $research, $attributes) {
                    $query = $this->filterQuery($query, $attributes);
                    return $query->whereAuthorId($user->id)->whereResearch($research);
                });

                if (isset($attributes['search']) && !empty($attributes['search'])) {
                    $search_expression = $this->getSearchExpression($attributes['search']);

                    $knowledge_list = $knowledge_list->selectRaw("knowledge.*,
                match(title,description,research_author,research_institution) against (" . \DB::getPdo()->quote($search_expression) . " in boolean mode) as title_search,
                match(title,description,body,research_author,research_institution) against (" . \DB::getPdo()->quote($search_expression) . " in boolean mode) as full_search
                ")->orderBy('title_search', 'desc')->orderBy('full_search', 'desc')->paginate($page_count);

                } else {
                    $knowledge_list = $knowledge_list->orderBy('knowledge.id', 'desc')->paginate($page_count);
                }

                $my_docs = true;
                return response()->view($view, compact('knowledge_list', 'my_docs'));
                break;
            case 'shared':
                $knowledge_list = Knowledge::where(function ($query) use ($user, $research, $attributes) {
                    $query = $query->where(function ($q) use ($user) {
                        $q = $q->whereHas('sharingCountries', function ($model) use ($user) {
                            $model->whereId($user->country_id);
                        });
                        $q = $q->orWhereHas('sharingRoles', function ($model) use ($user) {
                            $model->whereIn('id', $user->getRoles->pluck('id')->toArray());
                        });
                        $q = $q->orWhereHas('sharingUsers', function ($model) use ($user) {
                            $model->whereId($user->id);
                        });
                    });

                    $query = $this->filterQuery($query, $attributes);

                    $query = $query->whereIn('status', ['Approved', 'Unmoderated'])
                        ->where('author_id', '!=', $user->id)
                        ->whereResearch($research)->wherePublished(1);

                    return $query;
                });
                if (isset($attributes['search']) && !empty($attributes['search'])) {
                    $search_expression = $this->getSearchExpression($attributes['search']);

                    $knowledge_list = $knowledge_list->selectRaw("knowledge.*,
                match(title,description,research_author,research_institution) against (" . \DB::getPdo()->quote($search_expression) . " in boolean mode) as title_search,
                match(title,description,body,research_author,research_institution) against (" . \DB::getPdo()->quote($search_expression) . " in boolean mode) as full_search
                ")->orderBy('title_search', 'desc')->orderBy('full_search', 'desc')->paginate($page_count);
                } else {
                    $knowledge_list = $knowledge_list->orderBy('knowledge.id', 'desc')->paginate($page_count);
                }
                return response()->view($view, compact('knowledge_list'));
                break;
        }
    }

    public function loadAttachments(Knowledge $knowledge)
    {
        return response()->view('knowledge::user.knowledge.partial.attachments_list', compact('knowledge'));
    }

    public function loadShareForm(Knowledge $knowledge)
    {
        return response()->view('knowledge::user.knowledge.partial.share_form', compact('knowledge'));
    }

    /**
     * @param KnowledgeUserRequest $request
     * @param Knowledge $knowledge
     * @return mixed
     */
    public function show(KnowledgeUserRequest $request, Knowledge $knowledge)
    {
        $research = $this->setTitle($request);

        Form::populate($knowledge);

        return $this->theme->of('knowledge::user.knowledge.show', compact('knowledge'))->render();
    }

    /**
     * @param KnowledgeUserRequest $request
     * @return mixed
     */
    public function create(KnowledgeUserRequest $request)
    {
        $research = $this->setTitle($request);

        $write = $request->get('write');

        if (isset($write)) {
            $write = true;
        } else {
            $write = false;
        }

        $knowledge = $this->repository->newInstance([]);

        Form::populate($knowledge);
        if ($research) {
            return $this->theme->of('knowledge::user.knowledge.create_edit_research', compact('knowledge'))->render();
        }

        return $this->theme->of('knowledge::user.knowledge.create_edit', compact('knowledge', 'write'))->render();
    }

    /**
     * @param KnowledgeUserRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(KnowledgeUserRequest $request)
    {
        try {
            $attributes = $request->except('categories', 'country', 'tags', 'data_types', 'research_instruments');

            $request->categories = isset($attributes['themes']) ? $attributes['themes'] : '';
            $publish = false;

            if (isset($attributes['published']) && $attributes['published'] == 1) {

                if (user('web')->trusted) {
                    $attributes['published_at'] = \Carbon\Carbon::now();
                    $attributes['status'] = 'Unmoderated';
                } else {
                    unset($attributes['published_at']);
                    unset($attributes['published']);
                    $attributes['status'] = 'Pending';
                }

                $publish = true;
            }

            $write = $request->get('write');

            if (isset($write)) {
                $write = true;
            } else {
                $write = false;
            }

            if (!$write) {
                $has_documents = false;

                foreach (config('trans.locales') as $code => $language) {
                    if (isset($attributes['document_' . $code])) {
                        $has_documents = true;
                    }
                }

                if (!$has_documents) {
                    return response()->json([
                        'message' => trans('user_knowledge.validation.at_least_one_doc'),
                    ], 422);
                }
            }

            $attributes['author_id'] = user_id('web');

            if ($request->is('*research*')) {
                $attributes['research'] = 1;
            }

            $knowledge = $this->repository->create($attributes);

            \SoukTel\Knowledge\Facades\Knowledge::log_create($knowledge);

            $knowledge->categories()->sync($request->categories);

            $knowledge->countries()->sync($request->countries);
            $knowledge->research_instruments()->sync(!is_null($request->research_instruments) ? $request->research_instruments : []);
            $knowledge->methodologies()->sync(!is_null($request->methodologies) ? $request->methodologies : []);
            $knowledge->data_types()->sync(!is_null($request->data_types) ? $request->data_types : []);
            $knowledge->sub_themes()->sync(!is_null($request->sub_themes) ? $request->sub_themes : []);

            if (!$request->is('*research*')) {
                foreach ($request->tags as $tag) {
                    if (is_numeric($tag) && Tag::find($tag)) {
                        $knowledge->tags()->attach($tag);
                    } elseif (!is_numeric($tag)) {
                        $new_tag = Tag::create([
                            'name' => $tag,
                            'status' => 'show'
                        ]);
                        $knowledge->tags()->attach($new_tag);
                    }
                }
            }
            if (isset($attributes['sharing_country'])) {
                $knowledge->sharingCountries()->sync($attributes['sharing_country']);
            }
            if (isset($attributes['sharing_roles'])) {
                $knowledge->sharingRoles()->sync($attributes['sharing_roles']);
            }

            if (isset($attributes['sharing_users'])) {

                $knowledge->sharingUsers()->sync($attributes['sharing_users']);
                if ((user('web')->getOriginal('trusted') || $knowledge->status == 'Approved') && $knowledge->getOriginal('published') == 1) {
                    $this->sendSharingEmail($knowledge, $attributes['sharing_users']);
                }
            } else {
                $knowledge->sharingUsers()->sync([]);
            }

            if (!$write) {
                foreach (config('trans.locales') as $code => $language) {
                    if (isset($attributes['document_' . $code])) {
                        $attributes['created_by'] = user_id('web');

                        $attributes['locale'] = $code;

                        $attributes['knowledge_id'] = $knowledge->id;

                        $this->createDocument($request->file('document_' . $code), $attributes);
                    }
                }
            }

            if ($publish) {
                if (user('web')->trusted) {
                    $this->sendSharingEmail($knowledge);
                }
                $this->sendPublishEmail($knowledge);
            }

            return response()->json([
                'message' => trans('messages.success.created', ['Module' => trans('knowledge.name')]),
                'code' => 204,
                'redirect' => trans_url('user/knowledge/knowledge/' . $knowledge->getRouteKey()),
            ], 201);

        } catch (Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'code' => 400,
            ], 400);
        }
    }


    private function createDocument($file, array $attributes)
    {
        $attributes['original_name'] = $file->getClientOriginalName();

        $attributes['file_type'] = $file->getClientMimeType();

        $destination_path = '/app/documents/knowledge_' . $attributes['knowledge_id'];

        $attributes['file_name'] = hash('sha256', microtime()) . '.' . $file->getClientOriginalExtension();

        $attributes['file_path'] = $destination_path;

        $file->move(storage_path() . $destination_path, $attributes['file_name']);

        $document = $this->document_repo->create($attributes);
    }

    /**
     * @param KnowledgeUserRequest $request
     * @param Knowledge $knowledge
     * @return mixed
     */
    public function edit(KnowledgeUserRequest $request, Knowledge $knowledge)
    {
        $research = $this->setTitle($request);


        Form::populate($knowledge);

        $edit = true;

        $write = $knowledge->write ? true : false;

        if ($research) {
            return $this->theme->of('knowledge::user.knowledge.create_edit_research', compact('knowledge', 'edit'))->render();
        }

        return $this->theme->of('knowledge::user.knowledge.create_edit', compact('knowledge', 'edit', 'write'))->render();
    }

    /**
     * @param KnowledgeUserRequest $request
     * @param Knowledge $knowledge
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(KnowledgeUserRequest $request, Knowledge $knowledge)
    {
        try {
            $attributes = $request->except('categories', 'country', 'tags', 'data_types', 'research_instruments');
            $request->categories = isset($attributes['themes']) ? $attributes['themes'] : '';
            $write = $request->get('write');

            if (isset($write)) {
                $write = true;
            } else {
                $write = false;
            }

            $share_only = false;

            if (isset($attributes['share_only']) && $attributes['share_only'] == 'true') {
                $share_only = true;
            }

            if (!$share_only) {

                if (!$write) {
                    $has_documents = false;

                    foreach (config('trans.locales') as $code => $language) {
                        if (isset($attributes['document_' . $code]) || isset($attributes['document_' . $code . '_exist'])) {
                            $has_documents = true;
                            unset($attributes['document_' . $code . '_exist']);
                        }
                    }

                    if (!$has_documents) {
                        return response()->json([
                            'message' => trans('user_knowledge.validation.at_least_one_doc'),
                        ], 422);
                    }
                }

                $this->repository->update($attributes, $knowledge->getRouteKey());

                \SoukTel\Knowledge\Facades\Knowledge::log_update($knowledge);

                $knowledge->categories()->sync($request->categories);

                $knowledge->countries()->sync($request->countries);
                $knowledge->research_instruments()->sync(!is_null($request->research_instruments) ? $request->research_instruments : []);
                $knowledge->methodologies()->sync(!is_null($request->methodologies) ? $request->methodologies : []);
                $knowledge->data_types()->sync(!is_null($request->data_types) ? $request->data_types : []);
                $knowledge->sub_themes()->sync(!is_null($request->sub_themes) ? $request->sub_themes : []);

                if (isset($request->tags)) {
                    $knowledge->tags()->detach();
                    foreach ($request->tags as $tag) {

                        if (is_numeric($tag) && Tag::find($tag)) {
                            $knowledge->tags()->attach($tag);
                        } elseif (!is_numeric($tag)) {
                            $new_tag = Tag::create([
                                'name' => $tag,
                                'status' => 'show'
                            ]);
                            $knowledge->tags()->attach($new_tag);
                        }
                    }
                }
                if (!$write) {
                    foreach (config('trans.locales') as $code => $language) {
                        if (isset($attributes['document_' . $code])) {
                            $attributes['created_by'] = user_id('web');

                            $attributes['locale'] = $code;

                            $attributes['knowledge_id'] = $knowledge->id;

                            $this->createDocument($request->file('document_' . $code), $attributes);
                        }
                    }
                }
            }

            if (isset($attributes['sharing_country'])) {
                $knowledge->sharingCountries()->sync($attributes['sharing_country']);
            } else {
                $knowledge->sharingCountries()->sync([]);
            }

            if (isset($attributes['sharing_roles'])) {
                $knowledge->sharingRoles()->sync($attributes['sharing_roles']);
            } else {
                $knowledge->sharingRoles()->sync([]);
            }


            if (isset($attributes['sharing_users'])) {
                $knowledge_shared_with = $knowledge->sharingUsers()->pluck('id')->toArray();

                $knowledge->sharingUsers()->sync($attributes['sharing_users']);

                if ((user('web')->getOriginal('trusted') || $knowledge->status == 'Approved') &&
                    $knowledge->getOriginal('published') == 1
                ) {
                    $new_users = array_diff($attributes['sharing_users'], $knowledge_shared_with);
                    if (!empty($new_users)) {
                        $this->sendSharingEmail($knowledge, $new_users);
                    }
                }
            } else {
                $knowledge->sharingUsers()->sync([]);
            }

            return response()->json([
                'message' => trans('messages.success.updated', ['Module' => trans('knowledge.name')]),
                'code' => 204,
                'redirect' => trans_url('user/knowledge/knowledge/' . $knowledge->getRouteKey()),
            ], 201);

        } catch (Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'code' => 400,
            ], 400);
        }
    }

    /**
     * @param KnowledgeUserRequest $request
     * @param Knowledge $knowledge
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(KnowledgeUserRequest $request, Knowledge $knowledge)
    {
        try {
            foreach ($knowledge->documents as $document) {
                @unlink($document->getFullPath());
            }
            $this->repository->delete($knowledge->getRouteKey());
            return response()->json([
                'message' => trans('messages.success.deleted', ['Module' => trans('knowledge.name')]),
                'redirect' => url('user/knowledge/load-knowledge'),
            ], 201);

        } catch (Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    public function download(Knowledge $knowledge)
    {
        if ($knowledge->documents()->count()) {
            $document = $knowledge->documents()->whereLocale(\App::getLocale())->first();
            if (!$document) {
                $document = $knowledge->documents()->first();
            }
            return redirect('user/knowledge/document/download/' . $document->getRouteKey());
        }
    }

    public function publish(Knowledge $knowledge)
    {
        try {
            if (user('web')->trusted) {
                $knowledge->published = 1;
                $knowledge->published_at = \Carbon\Carbon::now();
                $knowledge->status = 'Unmoderated';
                $this->sendSharingEmail($knowledge);
            } else {
                $knowledge->status = 'Pending';
            }
            $knowledge->save();

            $this->sendPublishEmail($knowledge);

            return response()->json([
                'message' => trans('messages.success.updated', ['Module' => trans('knowledge.name')]),
                'code' => 204,
                'redirect' => trans_url('user/knowledge/knowledge/' . $knowledge->getRouteKey()),
            ], 201);
        } catch (Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'code' => 400,
            ], 400);
        }
    }

    public function unpublish(Knowledge $knowledge)
    {
        try {
            $knowledge->published = 0;
            $knowledge->published_at = null;
            $knowledge->status = null;
            $knowledge->save();

            return response()->json([
                'message' => trans('messages.success.updated', ['Module' => trans('knowledge.name')]),
                'code' => 204,
                'redirect' => trans_url('user/knowledge/knowledge/' . $knowledge->getRouteKey()),
            ], 201);
        } catch (Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'code' => 400,
            ], 400);
        }
    }
}
