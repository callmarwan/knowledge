<?php

namespace SoukTel\Knowledge\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use Form;
use SoukTel\Knowledge\Http\Requests\TagUserRequest;
use SoukTel\Knowledge\Interfaces\TagRepositoryInterface;
use SoukTel\Knowledge\Models\Tag;

class TagUserController extends BaseController
{
    

    /**
     * The authentication guard that should be used.
     *
     * @var string
     */
    protected $guard = 'web';

    /**
     * Initialize tag controller.
     *
     * @param type TagRepositoryInterface $tag
     *
     * @return type
     */
    protected $home = 'home';

    public function __construct(TagRepositoryInterface $tag)
    {
        $this->middleware('web');
        $this->middleware('auth:web');
        $this->middleware('auth.active:web');
        $this->setupTheme(config('theme.themes.user.theme'), config('theme.themes.user.layout'));
        $this->repository = $tag;
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(TagUserRequest $request)
    {
        $this->repository->pushCriteria(new \SoukTel\Knowledge\Repositories\Criteria\TagUserCriteria());
        $tags = $this->repository->scopeQuery(function($query){
            return $query->orderBy('id','DESC');
        })->paginate();

        $this->theme->prependTitle(trans('knowledge_tag.names').' :: ');

        return $this->theme->of('knowledge::user.tag.index', compact('tags'))->render();
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param Tag $tag
     *
     * @return Response
     */
    public function show(TagUserRequest $request, Tag $tag)
    {
        Form::populate($tag);

        return $this->theme->of('knowledge::user.tag.show', compact('tag'))->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(TagUserRequest $request)
    {

        $tag = $this->repository->newInstance([]);
        Form::populate($tag);

        return $this->theme->of('knowledge::user.tag.create', compact('tag'))->render();
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(TagUserRequest $request)
    {
        try {
            $attributes = $request->all();
            $attributes['user_id'] = user_id();
            $attributes['user_type'] = user_type();
            $tag = $this->repository->create($attributes);

            return redirect(trans_url('/user/knowledge/tag'))
                -> with('message', trans('messages.success.created', ['Module' => trans('knowledge_tag.name')]))
                -> with('code', 201);

        } catch (Exception $e) {
            redirect()->back()->withInput()->with('message', $e->getMessage())->with('code', 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param Tag $tag
     *
     * @return Response
     */
    public function edit(TagUserRequest $request, Tag $tag)
    {

        Form::populate($tag);

        return $this->theme->of('knowledge::user.tag.edit', compact('tag'))->render();
    }

    /**
     * Update the specified resource.
     *
     * @param Request $request
     * @param Tag $tag
     *
     * @return Response
     */
    public function update(TagUserRequest $request, Tag $tag)
    {
        try {
            $this->repository->update($request->all(), $tag->getRouteKey());

            return redirect(trans_url('/user/knowledge/tag'))
                ->with('message', trans('messages.success.updated', ['Module' => trans('knowledge_tag.name')]))
                ->with('code', 204);

        } catch (Exception $e) {
            redirect()->back()->withInput()->with('message', $e->getMessage())->with('code', 400);
        }
    }

    /**
     * Remove the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy(TagUserRequest $request, Tag $tag)
    {
        try {
            $this->repository->delete($tag->getRouteKey());
            return redirect(trans_url('/user/knowledge/tag'))
                ->with('message', trans('messages.success.deleted', ['Module' => trans('knowledge_tag.name')]))
                ->with('code', 204);

        } catch (Exception $e) {

            redirect()->back()->withInput()->with('message', $e->getMessage())->with('code', 400);

        }
    }
}
