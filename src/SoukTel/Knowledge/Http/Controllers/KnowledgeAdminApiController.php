<?php

namespace SoukTel\Knowledge\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use SoukTel\Knowledge\Http\Requests\KnowledgeAdminApiRequest;
use SoukTel\Knowledge\Interfaces\KnowledgeRepositoryInterface;
use SoukTel\Knowledge\Models\Knowledge;

/**
 * Admin API controller class.
 */
class KnowledgeAdminApiController extends BaseController
{
    /**
     * Initialize knowledge controller.
     *
     * @param type KnowledgeRepositoryInterface $knowledge
     *
     * @return type
     */
    protected $guard = 'admin.api';

    public function __construct(KnowledgeRepositoryInterface $knowledge)
    {
        $this->middleware('api');
        $this->middleware('jwt.auth:admin.api');
        $this->setupTheme(config('theme.themes.admin.theme'), config('theme.themes.admin.layout'));
         $this->repository = $knowledge;
        parent::__construct();
    }

    /**
     * Display a list of knowledge.
     *
     * @return json
     */
    public function index(KnowledgeAdminApiRequest $request)
    {
        $knowledges  = $this->repository
            ->setPresenter('\\SoukTel\\Knowledge\\Repositories\\Presenter\\KnowledgeListPresenter')
            ->scopeQuery(function($query){
                return $query->orderBy('id','DESC');
            })->all();
        $knowledges['code'] = 2000;
        return response()->json($knowledges) 
                         ->setStatusCode(200, 'INDEX_SUCCESS');

    }

    /**
     * Display knowledge.
     *
     * @param Request $request
     * @param Model   Knowledge
     *
     * @return Json
     */
    public function show(KnowledgeAdminApiRequest $request, Knowledge $knowledge)
    {
        $knowledge         = $knowledge->presenter();
        $knowledge['code'] = 2001;
        return response()->json($knowledge)
                         ->setStatusCode(200, 'SHOW_SUCCESS');;

    }

    /**
     * Show the form for creating a new knowledge.
     *
     * @param Request $request
     *
     * @return json
     */
    public function create(KnowledgeAdminApiRequest $request, Knowledge $knowledge)
    {
        $knowledge         = $knowledge->presenter();
        $knowledge['code'] = 2002;
        return response()->json($knowledge)
                         ->setStatusCode(200, 'CREATE_SUCCESS');

    }

    /**
     * Create new knowledge.
     *
     * @param Request $request
     *
     * @return json
     */
    public function store(KnowledgeAdminApiRequest $request)
    {
        try {
            $attributes             = $request->all();
            $attributes['user_id']  = user_id('admin.api');
            $attributes['user_type'] = user_type();
            $knowledge          = $this->repository->create($attributes);
            $knowledge          = $knowledge->presenter();
            $knowledge['code']  = 2004;

            return response()->json($knowledge)
                             ->setStatusCode(201, 'STORE_SUCCESS');
        } catch (Exception $e) {
            return response()->json([
                'message'  => $e->getMessage(),
                'code'     => 4004,
            ])->setStatusCode(400, 'STORE_ERROR');
;
        }
    }

    /**
     * Show knowledge for editing.
     *
     * @param Request $request
     * @param Model   $knowledge
     *
     * @return json
     */
    public function edit(KnowledgeAdminApiRequest $request, Knowledge $knowledge)
    {
        $knowledge         = $knowledge->presenter();
        $knowledge['code'] = 2003;
        return response()-> json($knowledge)
                        ->setStatusCode(200, 'EDIT_SUCCESS');;
    }

    /**
     * Update the knowledge.
     *
     * @param Request $request
     * @param Model   $knowledge
     *
     * @return json
     */
    public function update(KnowledgeAdminApiRequest $request, Knowledge $knowledge)
    {
        try {

            $attributes = $request->all();

            $knowledge->update($attributes);
            $knowledge         = $knowledge->presenter();
            $knowledge['code'] = 2005;

            return response()->json($knowledge)
                             ->setStatusCode(201, 'UPDATE_SUCCESS');


        } catch (Exception $e) {

            return response()->json([
                'message'  => $e->getMessage(),
                'code'     => 4005,
            ])->setStatusCode(400, 'UPDATE_ERROR');

        }
    }

    /**
     * Remove the knowledge.
     *
     * @param Request $request
     * @param Model   $knowledge
     *
     * @return json
     */
    public function destroy(KnowledgeAdminApiRequest $request, Knowledge $knowledge)
    {

        try {

            $t = $knowledge->delete();

            return response()->json([
                'message'  => trans('messages.success.delete', ['Module' => trans('knowledge.name')]),
                'code'     => 2006
            ])->setStatusCode(202, 'DESTROY_SUCCESS');

        } catch (Exception $e) {

            return response()->json([
                'message'  => $e->getMessage(),
                'code'     => 4006,
            ])->setStatusCode(400, 'DESTROY_ERROR');
        }
    }
}
