<?php

namespace SoukTel\Knowledge\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use Form;
use SoukTel\Knowledge\Http\Requests\CountryAdminRequest;
use SoukTel\Knowledge\Interfaces\CountryRepositoryInterface;
use SoukTel\Knowledge\Models\Country;

/**
 * Admin web controller class.
 */
class CountryAdminController extends BaseController
{
    /**
     * The authentication guard that should be used.
     *
     * @var string
     */
    public $guard = 'admin.web';

    /**
     * Initialize country controller.
     *
     * @param type CountryRepositoryInterface $country
     *
     * @return type
     */
    public $home = 'admin';

    public function __construct(CountryRepositoryInterface $country)
    {
        $this->middleware('web');
        $this->middleware('auth:admin.web');
        $this->setupTheme(config('theme.themes.admin.theme'), config('theme.themes.admin.layout'));
        $this->repository = $country;
        parent::__construct();
    }

    /**
     * Display a list of country.
     *
     * @return Response
     */
    public function index(CountryAdminRequest $request)
    {
        $pageLimit = $request->input('pageLimit');
        if ($request->wantsJson()) {
            $countrys = $this->repository
                ->setPresenter('\\SoukTel\\Knowledge\\Repositories\\Presenter\\CountryListPresenter')
                ->scopeQuery(function ($query) {
                    return $query;
                })->paginate($pageLimit);

            $countrys['recordsTotal'] = $countrys['meta']['pagination']['total'];
            $countrys['recordsFiltered'] = $countrys['meta']['pagination']['total'];
            $countrys['request'] = $request->all();
            return response()->json($countrys, 200);

        }

        $this->theme->prependTitle(trans('knowledge_country.names') . ' :: ');
        return $this->theme->of('knowledge::admin.country.index')->render();
    }

    /**
     * @param CountryAdminRequest $request
     * @param Country $country
     * @return \Illuminate\Http\Response
     */
    public function show(CountryAdminRequest $request, Country $country)
    {
        if (!$country->exists) {
            return response()->view('knowledge::admin.country.new', compact('country'));
        }

        Form::populate($country);
        return response()->view('knowledge::admin.country.show', compact('country'));
    }

    /**
     * @param CountryAdminRequest $request
     * @return \Illuminate\Http\Response
     */
    public function create(CountryAdminRequest $request)
    {

        $country = $this->repository->newInstance([]);

        Form::populate($country);

        return response()->view('knowledge::admin.country.create', compact('country'));

    }

    /**
     * @param CountryAdminRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CountryAdminRequest $request)
    {
        try {
            $attributes = $request->all();
            $attributes['user_id'] = user_id('admin.web');
            $attributes['user_type'] = user_type();
            $country = $this->repository->create($attributes);

            return response()->json([
                'message' => trans('messages.success.updated', ['Module' => trans('knowledge_country.name')]),
                'code' => 204,
                'redirect' => trans_url('/admin/knowledge/country/' . $country->getRouteKey())
            ], 201);


        } catch (Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'code' => 400,
            ], 400);
        }
    }

    /**
     * @param CountryAdminRequest $request
     * @param Country $country
     * @return \Illuminate\Http\Response
     */
    public function edit(CountryAdminRequest $request, Country $country)
    {
        Form::populate($country);
        return response()->view('knowledge::admin.country.edit', compact('country'));
    }

    /**
     * @param CountryAdminRequest $request
     * @param Country $country
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CountryAdminRequest $request, Country $country)
    {
        try {

            $attributes = $request->all();

            $country->update($attributes);

            return response()->json([
                'message' => trans('messages.success.updated', ['Module' => trans('knowledge_country.name')]),
                'code' => 204,
                'redirect' => trans_url('/admin/knowledge/country/' . $country->getRouteKey())
            ], 201);

        } catch (Exception $e) {

            return response()->json([
                'message' => $e->getMessage(),
                'code' => 400,
                'redirect' => trans_url('/admin/knowledge/country/' . $country->getRouteKey()),
            ], 400);

        }
    }

    /**
     * @param CountryAdminRequest $request
     * @param Country $country
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(CountryAdminRequest $request, Country $country)
    {

        try {

            $t = $country->delete();

            return response()->json([
                'message' => trans('messages.success.deleted', ['Module' => trans('knowledge_country.name')]),
                'code' => 204,
                'redirect' => trans_url('/admin/knowledge/country/0'),
            ], 201);

        } catch (Exception $e) {

            return response()->json([
                'message' => $e->getMessage(),
                'code' => 400,
                'redirect' => trans_url('/admin/knowledge/country/' . $country->getRouteKey()),
            ], 400);
        }
    }
}
