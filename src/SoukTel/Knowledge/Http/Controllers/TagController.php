<?php

namespace SoukTel\Knowledge\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use SoukTel\Knowledge\Interfaces\TagRepositoryInterface;

class TagController extends BaseController
{

    /**
     * Constructor.
     *
     * @param type \SoukTel\Tag\Interfaces\TagRepositoryInterface $tag
     *
     * @return type
     */
    public function __construct(TagRepositoryInterface $tag)
    {
        $this->middleware('web');
        $this->setupTheme(config('theme.themes.public.theme'), config('theme.themes.public.layout'));
        $this->repository = $tag;
        parent::__construct();
    }

    /**
     * Show tag's list.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function index()
    {
        $tags = $this->repository->scopeQuery(function ($query) {
            return $query->orderBy('id', 'DESC');
        })->paginate();

        return $this->theme->of('knowledge::public.tag.index', compact('tags'))->render();
    }

    /**
     * Show tag.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function show($slug)
    {
        $tag = $this->repository->scopeQuery(function ($query) use ($slug) {
            return $query->orderBy('id', 'DESC')
                ->where('slug', $slug);
        })->first(['*']);

        return $this->theme->of('knowledge::public.tag.show', compact('tag'))->render();
    }
}
