<?php

namespace SoukTel\Knowledge\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use Form;
use Illuminate\Http\Request;
use SoukTel\Knowledge\Interfaces\DocumentRepositoryInterface;
use SoukTel\Knowledge\Models\Document;

/**
 * Admin web controller class.
 */
class DocumentUserController extends BaseController
{
    /**
     * The authentication guard that should be used.
     *
     * @var string
     */
    public $guard = 'web';

    /**
     * Initialize document controller.
     *
     * @param type DocumentRepositoryInterface $document
     *
     * @return type
     */
    public $home = 'home';

    public function __construct(DocumentRepositoryInterface $document)
    {
        $this->middleware('web');
        $this->middleware('auth:web');
        $this->middleware('auth.active:web');
        $this->setupTheme(config('theme.themes.user.theme'), config('theme.themes.user.layout'));
        $this->document = $document;
        $this->theme->prependTitle(trans('knowledge.names'));
        parent::__construct();
    }

    /**
     * @param Request $request
     * @param Document $document
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, Document $document)
    {
        try {
            $knowledge = $document->knowledge;
            @unlink($document->getFullPath());
            $t = $document->delete();
            return response()->json([
                'message' => trans('messages.success.deleted', ['Module' => trans('knowledge_document.name')]),
                'code' => 204,
                'redirect' => trans_url('user/knowledge/knowledge/load-attachments/' . $knowledge->getRouteKey()),
            ], 201);

        } catch (Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'code' => 400,
                'redirect' => trans_url('/user/knowledge/document/' . $knowledge->getRouteKey()),
            ], 400);
        }
    }

    public function download(Document $document)
    {
        $document->knowledge->downloads += 1;

        \SoukTel\Knowledge\Facades\Document::log_download($document);

        $document->knowledge->save();

        return response()->download($document->getFullPath(), $document->original_name);
    }
}
