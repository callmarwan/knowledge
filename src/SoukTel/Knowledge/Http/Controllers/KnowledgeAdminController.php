<?php

namespace SoukTel\Knowledge\Http\Controllers;

use Carbon\Carbon;
use Form;
use SoukTel\Knowledge\Http\Requests\KnowledgeAdminRequest;
use SoukTel\Knowledge\Interfaces\KnowledgeRepositoryInterface;
use SoukTel\Knowledge\Models\Knowledge;
use SoukTel\Knowledge\Models\Tag;
use Illuminate\Support\Facades\Mail;

/**
 * Admin web controller class.
 */
class KnowledgeAdminController extends KnowledgeBaseController
{
    /**
     * The authentication guard that should be used.
     *
     * @var string
     */
    public $guard = 'admin.web';

    /**
     * Initialize knowledge controller.
     *
     * @param type KnowledgeRepositoryInterface $knowledge
     *
     * @return type
     */
    public $home = 'admin';

    public function __construct(KnowledgeRepositoryInterface $knowledge)
    {
        $this->middleware('web');
        $this->middleware('auth:admin.web');
        $this->setupTheme(config('theme.themes.admin.theme'), config('theme.themes.admin.layout'));
        $this->repository = $knowledge;
        parent::__construct();
    }

    /**
     * Display a list of knowledge.
     *
     * @return Response
     */
    public function index(KnowledgeAdminRequest $request)
    {
        $pageLimit = $request->input('pageLimit');

        if ($request->wantsJson()) {

            $status = $request->get('status');

            $search = $request->get('search');

            $knowledge = $this->repository
                ->setPresenter('\\SoukTel\\Knowledge\\Repositories\\Presenter\\KnowledgeListPresenter');

            if (!empty($search['category'])) {
                $category = $search['category'];
                $knowledge = $knowledge->whereHas('categories', function ($model) use ($category) {
                    $model = $model->where('id', $category);
                });
            }

            if (!empty($search['country'])) {
                $country = $search['country'];
                $knowledge = $knowledge->whereHas('countries', function ($model) use ($country) {
                    $model = $model->where('id', $country);
                });
            }


            if (!empty($search['research_instrument'])) {
                $research_instrument = $search['research_instrument'];
                $knowledge = $knowledge->whereHas('research_instruments', function ($model) use ($research_instrument) {
                    $model = $model->where('id', $research_instrument);
                });
            }

            if (!empty($search['type'])) {
                $type = $search['type'];
                $knowledge = $knowledge->scopeQuery(function ($query) use ($type){
                    $query = $query->whereType($type);
                    return $query;
                });
            }

            if (!empty($search['methodology'])) {
                $methodology = $search['methodology'];
                $knowledge = $knowledge->whereHas('methodologies', function ($model) use ($methodology) {
                    $model = $model->where('id', $methodology);
                });
            }

            if (!empty($search['tag'])) {
                $tag = $search['tag'];
                $knowledge = $knowledge->whereHas('tags', function ($model) use ($tag) {
                    $model = $model->where('id', $tag);
                });
            }

            $knowledge = $knowledge->scopeQuery(function ($query) use ($status, $search) {
                if ($status) {
                    $query = $query->whereStatus($status);
                }
                $from_data_filter = !empty($search['published_at_from']);
                $to_date_filter = !empty($search['published_at_to']);
                $published_at_query = '';
                $research_date_query = '';

                if ($from_data_filter) {
                    $from_date = Carbon::createFromFormat('m/Y', $search['published_at_from'])->format('Y-m-01');

                    $published_at_query = "(`published_at` IS NOT NULL AND date(`published_at`) >= '" . $from_date . "')";
                    $research_date_query = "(`research_date` IS NOT NULL AND date(`research_date`) >= '" . $from_date . "')";
                }
                if ($to_date_filter) {
                    $to_date = Carbon::createFromFormat('m/Y', $search['published_at_to'])->format('Y-m-31');
                    $published_at_query .= ($from_data_filter ? " AND " : "") . "(`published_at` IS NOT NULL AND date(`published_at`) <= '" . $to_date . "')";
                    $research_date_query .= ($from_data_filter ? " AND " : "") . "(`research_date` IS NOT NULL AND date(`research_date`) <= '" . $to_date . "')";
                }

                if ($from_data_filter || $to_date_filter) {
                    $query = $query->whereRaw("(($published_at_query) OR ($research_date_query))");
                }

                return $query->orderBy('id', 'desc');
            })->paginate($pageLimit);
            $knowledge['recordsTotal'] = $knowledge['meta']['pagination']['total'];
            $knowledge['recordsFiltered'] = $knowledge['meta']['pagination']['total'];
            $knowledge['request'] = $request->all();

            return response()->json($knowledge, 200);
        }

        $this->theme->prependTitle(trans('knowledge.names') . ' :: ');
        return $this->theme->of('knowledge::admin.knowledge.index')->render();
    }

    /**
     * @param KnowledgeAdminRequest $request
     * @param Knowledge $knowledge
     * @return mixed
     */
    public function show(KnowledgeAdminRequest $request, Knowledge $knowledge)
    {

        if (!$knowledge->exists) {
            return response()->view('knowledge::admin.knowledge.new', compact('knowledge'));
        }

        Form::populate($knowledge);


        return response()->view('knowledge::admin.knowledge.show', compact('knowledge'));
    }

    /**
     * @param KnowledgeAdminRequest $request
     * @return mixed
     */
    public function create(KnowledgeAdminRequest $request)
    {

        $knowledge = $this->repository->newInstance([]);

        Form::populate($knowledge);


        return response()->view('knowledge::admin.knowledge.create', compact('knowledge'));

    }

    /**
     * @param KnowledgeAdminRequest $request
     * @return mixed
     */
    public function store(KnowledgeAdminRequest $request)
    {
        try {
            $attributes['author_id'] = user_id('admin.web');
            $attributes = $request->except('categories', 'country', 'tags' ,'data_types', 'research_instruments');

            $request->categories = isset($attributes['themes']) ? $attributes['themes'] : '';

            $knowledge = $this->repository->create($attributes);

            $knowledge->categories()->sync($request->categories);

            $knowledge->countries()->sync($request->countries);
            $knowledge->research_instruments()->sync(!is_null($request->research_instruments) ? $request->research_instruments : []);
            $knowledge->methodologies()->sync(!is_null($request->methodologies) ? $request->methodologies : []);
            $knowledge->data_types()->sync(!is_null($request->data_types) ? $request->data_types : []);
            $knowledge->sub_themes()->sync(!is_null($request->sub_themes) ? $request->sub_themes : [] );

            \SoukTel\Knowledge\Facades\Knowledge::log_create($knowledge);
            if (isset($attributes['tags'])) {
                foreach ($request->tags as $tag) {
                    if (is_numeric($tag) && Tag::find($tag)) {
                        $knowledge->tags()->attach($tag);
                    } elseif (!is_numeric($tag)) {
                        $new_tag = Tag::create([
                            'name' => $tag,
                            'status' => 'show'
                        ]);
                        $knowledge->tags()->attach($new_tag);
                    }
                }
            }
            if (isset($attributes['sharing_country'])) {
                $knowledge->sharingCountries()->sync($attributes['sharing_country']);
            }
            if (isset($attributes['sharing_roles'])) {
                $knowledge->sharingRoles()->sync($attributes['sharing_roles']);
            }
            if (isset($attributes['sharing_users'])) {
                $knowledge->sharingUsers()->sync($attributes['sharing_users']);
            }

            return response()->json([
                'message' => trans('messages.success.created', ['Module' => trans('knowledge.name')]),
                'code' => 204,
                'redirect' => trans_url('/admin/knowledge/knowledge/' . $knowledge->getRouteKey()),
            ], 201);

        } catch (Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'code' => 400,
            ], 400);
        }

    }

    /**
     * @param KnowledgeAdminRequest $request
     * @param Knowledge $knowledge
     * @return mixed
     */
    public function edit(KnowledgeAdminRequest $request, Knowledge $knowledge)
    {
        Form::populate($knowledge);


        return response()->view('knowledge::admin.knowledge.edit', compact('knowledge'));
    }


    private function sendStatusEmail(Knowledge $knowledge)
    {
        Mail::queue('knowledge::admin.knowledge.email.status_email',
            ['knowledge' => $knowledge], function ($message) use ($knowledge) {
                $message->to($knowledge->author->email, $knowledge->author->name)
                    ->subject(trans('knowledge.status_email_subject'));
            });
    }

    /**
     * @param KnowledgeAdminRequest $request
     * @param Knowledge $knowledge
     * @return mixed
     */
    public function update(KnowledgeAdminRequest $request, Knowledge $knowledge)
    {
        try {
            $approval = false;

            if ($request->is('*status*')) {
                $approval = true;
            }


            $attributes = $request->all();
            $attributes['categories'] = isset($attributes['themes']) ? $attributes['themes'] : '';

            $status = $knowledge->status;

            $knowledge->update($attributes);

            \SoukTel\Knowledge\Facades\Knowledge::log_update($knowledge);

            if (!$approval) {
                if (isset($attributes['categories'])) {
                    $knowledge->categories()->sync($attributes['categories']);
                }
                if (isset($attributes['countries'])) {
                    $knowledge->countries()->sync($attributes['countries']);
                }

                $knowledge->research_instruments()->sync(!is_null($request->research_instruments) ? $request->research_instruments : []);
                $knowledge->methodologies()->sync(!is_null($request->methodologies) ? $request->methodologies : []);
                $knowledge->data_types()->sync(!is_null($request->data_types) ? $request->data_types : []);
                $knowledge->sub_themes()->sync(!is_null($request->sub_themes) ? $request->sub_themes : [] );

                if (isset($attributes['tags'])) {
                    $knowledge->tags()->detach();

                    foreach ($attributes['tags'] as $tag) {
                        if (is_numeric($tag) && Tag::find($tag)) {
                            $knowledge->tags()->attach($tag);
                        } elseif (!is_numeric($tag)) {
                            $new_tag = Tag::create([
                                'name' => $tag,
                                'status' => 'show'
                            ]);
                            $knowledge->tags()->attach($new_tag);
                        }
                    }
                }

                /**
                 * handle sharing
                 */
                if (isset($attributes['sharing_country'])) {
                    $knowledge->sharingCountries()->sync($attributes['sharing_country']);
                } else {
                    $knowledge->sharingCountries()->detach();
                }

                if (isset($attributes['sharing_roles'])) {
                    $knowledge->sharingRoles()->sync($attributes['sharing_roles']);
                } else {
                    $knowledge->sharingRoles()->detach();
                }

                if (isset($attributes['sharing_users'])) {
                    $knowledge_shared_with = $knowledge->sharingUsers()->pluck('id')->toArray();

                    $knowledge->sharingUsers()->sync($attributes['sharing_users']);

                    if (($knowledge->author->getOriginal('trusted') || $knowledge->status == 'Approved')
                        && $knowledge->getOriginal('published') == 1
                    ) {
                        $new_users = array_diff($attributes['sharing_users'], $knowledge_shared_with);
                        if (!empty($new_users)) {
                            $this->sendSharingEmail($knowledge, $new_users);
                        }
                    }
                } else {
                    $knowledge->sharingUsers()->detach();
                }
            }
            if (isset($attributes['status']) && $attributes['status'] == 'Approved' && $status != 'Approved' && $knowledge->author_id != user('admin.web')->id) {
                $knowledge->published = 1;
                $knowledge->published_at = \Carbon\Carbon::now();
                $knowledge->save();

                $this->sendSharingEmail($knowledge);
                $this->sendStatusEmail($knowledge);
            }
            if (isset($attributes['status']) && $attributes['status'] == 'Rejected' && $status != 'Rejected' && $knowledge->author_id != user('admin.web')->id) {
                $knowledge->published = 0;
                $knowledge->published_at = null;
                $knowledge->save();

                $this->sendStatusEmail($knowledge);
            }

            return response()->json([
                'message' => trans('messages.success.updated', ['Module' => trans('knowledge.name')]),
                'code' => 204,
                'redirect' => trans_url('/admin/knowledge/knowledge/' . $knowledge->getRouteKey()),
            ], 201);

        } catch (Exception $e) {

            return response()->json([
                'message' => $e->getMessage(),
                'code' => 400,
                'redirect' => trans_url('/admin/knowledge/knowledge/' . $knowledge->getRouteKey()),
            ], 400);

        }

    }

    /**
     * @param KnowledgeAdminRequest $request
     * @param Knowledge $knowledge
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(KnowledgeAdminRequest $request, Knowledge $knowledge)
    {

        try {

            $t = $knowledge->delete();

            return response()->json([
                'message' => trans('messages.success.deleted', ['Module' => trans('knowledge.name')]),
                'code' => 204,
                'redirect' => trans_url('/admin/knowledge/knowledge/0'),
            ], 201);

        } catch (Exception $e) {

            return response()->json([
                'message' => $e->getMessage(),
                'code' => 400,
                'redirect' => trans_url('/admin/knowledge/knowledge/' . $knowledge->getRouteKey()),
            ], 400);
        }

    }

    public function loadDocs(Knowledge $knowledge)
    {
        $documents = $knowledge->documents;
        return response()->view('knowledge::admin.knowledge.partial.documents_list', compact('documents'));
    }

}
