<?php

namespace SoukTel\Knowledge\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use SoukTel\Knowledge\Interfaces\TagRepositoryInterface;
use SoukTel\Knowledge\Repositories\Presenter\TagItemTransformer;

/**
 * Pubic API controller class.
 */
class TagApiController extends BaseController
{
   
    
    /**
     * Constructor.
     *
     * @param type \SoukTel\Tag\Interfaces\TagRepositoryInterface $tag
     *
     * @return type
     */
    public function __construct(TagRepositoryInterface $tag)
    {
        $this->middleware('api');
        $this->repository = $tag;
        parent::__construct();
    }

    /**
     * Show tag's list.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function index()
    {
        $tags = $this->repository
            ->setPresenter('\\SoukTel\\Knowledge\\Repositories\\Presenter\\TagListPresenter')
            ->scopeQuery(function($query){
                return $query->orderBy('id','DESC');
            })->paginate();

        $tags['code'] = 2000;
        return response()->json($tags)
                ->setStatusCode(200, 'INDEX_SUCCESS');
    }

    /**
     * Show tag.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function show($slug)
    {
        $tag = $this->repository
            ->scopeQuery(function($query) use ($slug) {
            return $query->orderBy('id','DESC')
                         ->where('slug', $slug);
        })->first(['*']);

        if (!is_null($tag)) {
            $tag         = $this->itemPresenter($module, new TagItemTransformer);
            $tag['code'] = 2001;
            return response()->json($tag)
                ->setStatusCode(200, 'SHOW_SUCCESS');
        } else {
            return response()->json([])
                ->setStatusCode(400, 'SHOW_ERROR');
        }

    }
}
