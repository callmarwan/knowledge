<?php

namespace SoukTel\Knowledge\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use Form;
use SoukTel\Knowledge\Http\Requests\SubThemeAdminRequest;
use SoukTel\Knowledge\Interfaces\SubThemeRepositoryInterface;
use SoukTel\Knowledge\Models\SubTheme;

/**
 * Admin web controller class.
 */
class SubThemeAdminController extends BaseController
{
    /**
     * The authentication guard that should be used.
     *
     * @var string
     */
    public $guard = 'admin.web';

    /**
     * Initialize sub_theme controller.
     *
     * @param type SubThemeRepositoryInterface $sub_theme
     *
     * @return type
     */
    public $home = 'admin';

    public function __construct(SubThemeRepositoryInterface $sub_theme)
    {
        $this->middleware('web');
        $this->middleware('auth:admin.web');
        $this->setupTheme(config('theme.themes.admin.theme'), config('theme.themes.admin.layout'));
        $this->repository = $sub_theme;
        parent::__construct();
    }

    /**
     * Display a list of sub_theme.
     *
     * @return Response
     */
    public function index(SubThemeAdminRequest $request)
    {
        $pageLimit = $request->input('pageLimit');
        $search = $request->get('search');

        if ($request->wantsJson()) {
            $sub_themes = $this->repository
                ->setPresenter('\\SoukTel\\Knowledge\\Repositories\\Presenter\\SubThemeListPresenter');

            if (!empty($search['category'])) {
                $category = $search['category'];
                $sub_themes = $sub_themes->whereHas('category', function ($model) use ($category) {
                    $model = $model->where('name', 'like' , '%' . $category .'%');
                });
            }
            $sub_themes = $sub_themes->scopeQuery(function ($query) {
                return $query->orderBy('id', 'DESC');
            })->paginate($pageLimit);

            $sub_themes['recordsTotal'] = $sub_themes['meta']['pagination']['total'];
            $sub_themes['recordsFiltered'] = $sub_themes['meta']['pagination']['total'];
            $sub_themes['request'] = $request->all();
            return response()->json($sub_themes, 200);

        }

        $this->theme->prependTitle(trans('knowledge_sub_theme.names') . ' :: ');
        return $this->theme->of('knowledge::admin.sub_theme.index')->render();
    }

    /**
     * @param SubThemeAdminRequest $request
     * @param SubTheme $sub_theme
     * @return \Illuminate\Http\Response
     */
    public function show(SubThemeAdminRequest $request, SubTheme $sub_theme)
    {
        if (!$sub_theme->exists) {
            return response()->view('knowledge::admin.sub_theme.new', compact('sub_theme'));
        }

        Form::populate($sub_theme);
        return response()->view('knowledge::admin.sub_theme.show', compact('sub_theme'));
    }

    /**
     * @param SubThemeAdminRequest $request
     * @return \Illuminate\Http\Response
     */
    public function create(SubThemeAdminRequest $request)
    {

        $sub_theme = $this->repository->newInstance([]);

        Form::populate($sub_theme);

        return response()->view('knowledge::admin.sub_theme.create', compact('sub_theme'));

    }

    /**
     * @param SubThemeAdminRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(SubThemeAdminRequest $request)
    {
        try {
            $attributes = $request->all();
            $attributes['user_id'] = user_id('admin.web');
            $attributes['user_type'] = user_type();


            $sub_theme = $this->repository->create($attributes);


            return response()->json([
                'message' => trans('messages.success.updated', ['Module' => trans('knowledge_sub_theme.name')]),
                'code' => 204,
                'redirect' => trans_url('/admin/knowledge/sub_theme/' . $sub_theme->getRouteKey())
            ], 201);


        } catch (Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'code' => 400,
            ], 400);
        }
    }

    /**
     * @param SubThemeAdminRequest $request
     * @param SubTheme $sub_theme
     * @return \Illuminate\Http\Response
     */
    public function edit(SubThemeAdminRequest $request, SubTheme $sub_theme)
    {
        Form::populate($sub_theme);
        return response()->view('knowledge::admin.sub_theme.edit', compact('sub_theme'));
    }

    /**
     * @param SubThemeAdminRequest $request
     * @param SubTheme $sub_theme
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(SubThemeAdminRequest $request, SubTheme $sub_theme)
    {
        try {

            $attributes = $request->all();

            $sub_theme->update($attributes);

            return response()->json([
                'message' => trans('messages.success.updated', ['Module' => trans('knowledge_sub_theme.name')]),
                'code' => 204,
                'redirect' => trans_url('/admin/knowledge/sub_theme/' . $sub_theme->getRouteKey())
            ], 201);

        } catch (Exception $e) {

            return response()->json([
                'message' => $e->getMessage(),
                'code' => 400,
                'redirect' => trans_url('/admin/knowledge/sub_theme/' . $sub_theme->getRouteKey()),
            ], 400);

        }
    }

    /**
     * @param SubThemeAdminRequest $request
     * @param SubTheme $sub_theme
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(SubThemeAdminRequest $request, SubTheme $sub_theme)
    {

        try {

            $t = $sub_theme->delete();

            return response()->json([
                'message' => trans('messages.success.deleted', ['Module' => trans('knowledge_sub_theme.name')]),
                'code' => 204,
                'redirect' => trans_url('/admin/knowledge/sub_theme/0'),
            ], 201);

        } catch (Exception $e) {

            return response()->json([
                'message' => $e->getMessage(),
                'code' => 400,
                'redirect' => trans_url('/admin/knowledge/sub_theme/' . $sub_theme->getRouteKey()),
            ], 400);
        }
    }
}
