<?php

namespace SoukTel\Knowledge;

use App\User;
use Litepie\User\Models\Role;
use SoukTel\Knowledge\Models\DataType;
use SoukTel\Knowledge\Models\Type;

class Knowledge
{
    /**
     * $knowledge object.
     */
    protected $knowledge;
    /**
     * $category object.
     */
    protected $category;

    protected $tag;
    protected $research_instrument;
    protected $methodology;
    protected $dataType;
    protected $subTheme;

    protected $country;

    /**
     * Constructor.
     */
    public function __construct(\SoukTel\Knowledge\Interfaces\KnowledgeRepositoryInterface $knowledge,
                                \SoukTel\Knowledge\Interfaces\CategoryRepositoryInterface $category,
                                \SoukTel\Knowledge\Interfaces\TagRepositoryInterface $tag,
                                \SoukTel\Knowledge\Interfaces\CountryRepositoryInterface $country,
                                \SoukTel\Knowledge\Interfaces\ResearchInstrumentRepositoryInterface $research_instrument,
                                \SoukTel\Knowledge\Interfaces\MethodologyRepositoryInterface $methodology,
                                \SoukTel\Knowledge\Interfaces\DataTypeRepositoryInterface $dataType,
                                \SoukTel\Knowledge\Interfaces\SubThemeRepositoryInterface $subTheme

    )
    {
        $this->knowledge = $knowledge;
        $this->category = $category;
        $this->tag = $tag;
        $this->country = $country;
        $this->research_instrument = $research_instrument;
        $this->methodology = $methodology;
        $this->dataType = $dataType;
        $this->subTheme = $subTheme;
    }

    /**
     * @param $type
     * @return int
     */
    public function count($type)
    {
        $knowledges = $this->$type->all();
        return count($knowledges);
    }

    /**
     * Returns category of knowledge.
     * @return array
     */
    public function getCategory()
    {
        $array = [];
        $categorys = $this->category->scopeQuery(function ($query) {
            return $query->orderBy('name')->whereStatus('show');
        })->all();

        foreach ($categorys as $key => $category) {
            $array[$category['id']] = ucfirst($category['name']);
        }

        return $array;

    }

    /**
     * get categorys for public side
     * @param type|string $view
     * @return type
     */
    public function viewCategorys()
    {
        $this->knowledge->pushCriteria(new \SoukTel\Knowledge\Repositories\Criteria\CategoryPublicCriteria());
        $categorys = $this->category->scopeQuery(function ($query) {
            return $query->orderBy('name');
        })->all();

        return view('knowledge::public.category.index', compact('categorys'))->render();
    }

    /**
     * get related knowledges
     * @param category_id
     * @return array
     */
    public function getRelated($cid)
    {
        $this->knowledge->pushCriteria(new \SoukTel\Knowledge\Repositories\Criteria\KnowledgePublicCriteria());
        $knowledges = $this->knowledge->scopeQuery(function ($query) use ($cid) {
            return $query->orderBy('id', 'DESC')->whereCategory_id($cid)->take(2);
        })->all();

        return $knowledges;
    }

    /**
     * get related knowledges
     * @param category_id
     * @return array
     */
    public function recentKnowledge()
    {
        $this->knowledge->pushCriteria(new \SoukTel\Knowledge\Repositories\Criteria\KnowledgePublicCriteria());
        $knowledges = $this->knowledge->scopeQuery(function ($query) {
            return $query->orderBy('id', 'DESC')->take(5);
        })->all();

        return $knowledges;
    }

    /**
     * get related knowledges
     * @param category_id
     * @return array
     */
    public function getCount($cid)
    {
        $this->knowledge->pushCriteria(new \SoukTel\Knowledge\Repositories\Criteria\KnowledgePublicCriteria());
        $knowledges = $this->knowledge->scopeQuery(function ($query) use ($cid) {
            return $query->orderBy('id', 'DESC')->whereCategory_id($cid);
        })->all();

        return count($knowledges);
    }

    /**
     * @return int
     */
    public function countKnowledges()
    {
        $this->knowledge->pushCriteria(new \SoukTel\Knowledge\Repositories\Criteria\KnowledgePublicCriteria());
        $knowledges = $this->knowledge->scopeQuery(function ($query) {
            return $query->whereStatus('show');
        })->all();
        return count($knowledges);
    }

    /**
     * @param string $status
     * @return array
     */
    public function countriesList($status = 'show')
    {
        $temp = [];
        $list = $this->country->scopeQuery(function ($query) use ($status) {
            if ($status == 'all') {
                // return all the list and ignore status
                return $query;
            }
            return $query->whereStatus($status);
        })->all();

        foreach ($list as $key => $value) {
            $temp[$value->id] = ucfirst($value->name);
        }

        return $temp;
    }

    public function rolesList()
    {
        $temp = [];
        $list = Role::whereNotIn('id', config('package.knowledge.knowledge.excluded_roles'))->get();

        foreach ($list as $key => $value) {
            $temp[$value->id] = ucfirst($value->name);
        }

        return $temp;
    }

    public function usersList($status = 'Active')
    {
        $name = 'nickname';
        if (user('admin.web')) {
            $name = 'name';
        }
        $temp = [];
        $list = User::join('role_user', 'role_user.user_id', '=', 'users.id')
            ->whereNotIn('role_user.role_id', config('package.knowledge.knowledge.excluded_roles'))
            ->where('users.id', '!=', user_id());

        if ($status) {
            $list = $list->whereStatus($status);
        }

        $list = $list->select('users.*')->get();

        foreach ($list as $key => $value) {
            $temp[$value->id] = strtoupper($value->$name);
        }

        return $temp;
    }

    /**
     * @param string $status
     * @return array
     */
    public function categoriesList($status = 'show', $showIn = [])
    {

        $temp = [];
        $list = $this->category->scopeQuery(function ($query) use ($status , $showIn) {
            if (!empty($showIn)){
                $query = $query->where($showIn,'=', 1);
            }

            if (strtolower($status) == 'all') {
                return $query;
            }
            return $query->whereStatus($status);
        })->all();

        foreach ($list as $key => $value) {
            $temp[$value->id] = ucfirst($value->name);
        }

        return $temp;
    }

    /**
     * @param string $status
     * @return array
     */
    public function tagsList($status = 'show')
    {
        $temp = [];
        $list = $this->tag->scopeQuery(function ($query) use ($status) {
            if (strtolower($status) == 'all') {
                return $query;
            }
            return $query->whereStatus($status);
        })->all();

        foreach ($list as $key => $value) {
            $temp[$value->id] = ucfirst($value->name);
        }

        return $temp;
    }

    /**
     * @param string $status
     * @return array
     */
    public function typesList($status = 'show')
    {
        $temp = [];
        $list = Type::where('status', $status)->get();

        foreach ($list as $key => $value) {
            $temp[$value->id] = ucfirst($value->name);
        }

        return $temp;
    }

    /**
     * @param string $status
     * @return array
     */
    public function SubThemesList($status = 'show')
    {
        $temp = [];
        $list = $this->subTheme->scopeQuery(function ($query) use ($status) {
            if (strtolower($status) == 'all') {
                return $query;
            }
            return $query->whereStatus($status);
        })->all();

        foreach ($list as $key => $value) {
            $temp[$value->id] = ucfirst($value->name);
        }

        return $temp;
    }

    /**
     * @param string $status
     * @return array
     */
    public function methodologiesList()
    {
        $temp = [];
        $list = $this->methodology->scopeQuery(function ($query) {
            return $query;
        })->all();

        foreach ($list as $key => $value) {
            $temp[$value->id] = trans('knowledge.methodology_options')[$value->id];
        }

        return $temp;
    }

    /**
     * @param string $status
     * @return array
     */
    public function dataTypesList()
    {
        $temp = [];
        $list = $this->dataType->scopeQuery(function ($query) {
            return $query;
        })->all();

        foreach ($list as $key => $value) {
            $temp[$value->id] = trans('knowledge.data_types_options')[$value->id];
        }

        return $temp;
    }

    /**
     * @param string $status
     * @return array
     */
    public function researchInstrumentsList($status = 'show')
    {
        $temp = [];
        $list = $this->research_instrument->scopeQuery(function ($query) use ($status) {
            if (strtolower($status) == 'all') {
                return $query;
            }
            return $query->whereStatus($status);
        })->all();

        foreach ($list as $key => $value) {
            $temp[$value->id] = ucfirst($value->name);
        }

        return $temp;
    }

    public function log_create(\SoukTel\Knowledge\Models\Knowledge $knowledge)
    {
        $activity = activityLog($knowledge, 'activity.knowledge.create', 'knowledge', ['action' => 'create']);

        $activity->meta()->createMany([
            [
                'meta_key' => 'type',
                'meta_value' => $knowledge->research ? 'user_knowledge.titles.research' : 'user_knowledge.titles.document',
            ],
            [
                'meta_key' => 'title',
                'meta_value' => $knowledge->title,
            ],
        ]);
    }

    public function log_update(\SoukTel\Knowledge\Models\Knowledge $knowledge)
    {
        $activity = activityLog($knowledge, 'activity.knowledge.update', 'knowledge', ['action' => 'update']);

        $activity->meta()->createMany([
            [
                'meta_key' => 'type',
                'meta_value' => $knowledge->research ? 'user_knowledge.titles.research' : 'user_knowledge.titles.document',
            ],
            [
                'meta_key' => 'title',
                'meta_value' => $knowledge->title,
            ],
        ]);
    }
}
