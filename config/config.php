<?php

 return [

    /**
     * Provider.
     */
    'provider'  => 'callmarwan',

    /*
     * Package.
     */
    'package'   => 'knowledge',

    /*
     * Modules.
     */
    'modules'   => [
        'knowledge',
        'category',
    ],
    /*
     * Image size.
     */
    'image'     => [

        'sm' => [
            'width'     => '150',
            'height'    => '150',
            'action'    => 'fit',
            'watermark' => 'img/logo/default.png',
        ],

        'md' => [
            'width'     => '370',
            'height'    => '420',
            'action'    => 'fit',
            'watermark' => 'img/logo/default.png',
        ],

        'lg' => [
            'width'     => '430',
            'height'    => '430',
            'action'    => 'fit',
            'watermark' => 'img/logo/default.png',
        ],

    ],

    'knowledge' => [
        'model'         => 'SoukTel\Knowledge\Models\Knowledge',
        'table'         => 'knowledges',
        'presenter'     => \SoukTel\Knowledge\Repositories\Presenter\KnowledgeItemPresenter::class,
        'hidden'        => [],
        'visible'       => [],
        'guarded'       => ['*'],
        'slugs'         => ['slug' => 'title'],
        'dates'         => ['deleted_at'],
        'appends'       => [],
        'fillable'      => ['user_id', 'user_type', 'upload_folder', 'title', 'details', 'category_id', 'image', 'images', 'status'],
        'translate'     => [],

        'upload_folder' => '/knowledge/knowledge',
        'uploads'       => [
            'single'   => ['image'],
            'multiple' => ['images'],
        ],
        'casts'         => [
            'image'  => 'array',
            'images' => 'array',
        ],
        'revision'      => [],
        'perPage'       => '20',
        'search'        => [
            'title' => 'like',
            'status',
        ],
    ],
    'category'  => [
        'model'         => 'SoukTel\Knowledge\Models\Category',
        'table'         => 'knowledge_categorys',
        'presenter'     => \SoukTel\Knowledge\Repositories\Presenter\CategoryItemPresenter::class,
        'hidden'        => [],
        'visible'       => [],
        'guarded'       => ['*'],
        'slugs'         => ['slug' => 'name'],
        'dates'         => ['deleted_at'],
        'appends'       => [],
        'fillable'      => ['user_id', 'user_type', 'name', 'status'],
        'translate'     => [],

        'upload_folder' => '/knowledge/category',
        'uploads'       => [
            'single'   => ['image'],
            'multiple' => ['images'],
        ],
        'casts'         => [
            'image'  => 'array',
            'images' => 'array',
        ],
        'revision'      => [],
        'perPage'       => '20',
        'search'        => [
            'title' => 'like',
            'status',
        ],
    ],
];
