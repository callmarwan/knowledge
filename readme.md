This is a Laravel 5 package that provides knowledge management facility for SoukTel framework.

## Installation

Begin by installing this package through Composer. Edit your project's `composer.json` file to require `callmarwan/knowledge`.

    "callmarwan/knowledge": "dev-master"

Next, update Composer from the Terminal:

    composer update

Once this operation completes execute below cammnds in command line to finalize installation.

```php
SoukTel\Knowledge\Providers\KnowledgeServiceProvider::class,

```

And also add it to alias

```php
'Knowledge'  => SoukTel\Knowledge\Facades\Knowledge::class,
```

Use the below commands for publishing

Migration and seeds

    php artisan vendor:publish --provider="SoukTel\Knowledge\Providers\KnowledgeServiceProvider" --tag="migrations"
    php artisan vendor:publish --provider="SoukTel\Knowledge\Providers\KnowledgeServiceProvider" --tag="seeds"

Configuration

    php artisan vendor:publish --provider="SoukTel\Knowledge\Providers\KnowledgeServiceProvider" --tag="config"

Language files

    php artisan vendor:publish --provider="SoukTel\Knowledge\Providers\KnowledgeServiceProvider" --tag="lang"

Views files

    php artisan vendor:publish --provider="SoukTel\Knowledge\Providers\KnowledgeServiceProvider" --tag="views"
    

Public folders

    php artisan vendor:publish --provider="SoukTel\Knowledge\Providers\KnowledgeServiceProvider" --tag="public"

## Usage


