<div class="blog-detail-side-category-wraper clearfix">
    <h3>Categorys</h3>
    <ul>
        <li class="{{ (Request::is('knowledge'))? 'active' : ''}}"><a href="{{trans_url('knowledge')}}">All</a><span class="cat-number">5</span></li>
        @forelse($categorys as  $category)
        <li class="{{(Request::is('*knowledge/category/'.$category->slug))? 'active' : ''}}"><a href="{{trans_url('knowledge/category')}}/{{@$category->slug}}">{{$category->name}}</a><span class="cat-number">4</span></li>
        @empty
        @endif

    </ul>
</div>
