<div class="box-header with-border">
    <h3 class="box-title"> {{ trans('cms.new') }}  {!! trans('knowledge.name') !!} </h3>
    <div class="box-tools pull-right">
        <button type="button" class="btn btn-primary btn-sm" data-action='CREATE' data-form='#knowledge-knowledge-create'
                data-load-to='#knowledge-knowledge-entry' data-datatable='#knowledge-knowledge-list'><i class="fa fa-floppy-o"></i> Save
        </button>
        <button type="button" class="btn btn-default btn-sm" data-action='CLOSE' data-load-to='#knowledge-knowledge-entry'
                data-href='{{trans_url('admin/knowledge/knowledge/0')}}'><i
                    class="fa fa-times-circle"></i> {{ trans('cms.close') }}</button>
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
</div>
<div class="box-body">
    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#info" data-toggle="tab">Knowledge</a></li>
            <li><a href="#image" data-toggle="tab">Image</a></li>
        </ul>
        {!!Form::vertical_open()
        ->id('knowledge-knowledge-create')
        ->method('POST')
        ->files('true')
        ->action(trans_url('admin/knowledge/knowledge'))!!}

        @include('knowledge::admin.knowledge.partial.entry')

        {!! Form::close() !!}
    </div>
</div>
