<div class="box-header with-border">
    <h3 class="box-title"> {{ trans('cms.view') }}   {!! trans('knowledge.name') !!}  [{!! $knowledge->title !!}]  </h3>
    <div class="box-tools pull-right">
        <button type="button" class="btn btn-success btn-sm" data-action='NEW' data-load-to='#knowledge-knowledge-entry' data-href='{{trans_url('admin/knowledge/knowledge/create')}}'><i class="fa fa-times-circle"></i> New</button>
        @if($knowledge->id )
        @if($knowledge->published == 'Yes')
            <button type="button" class="btn btn-warning btn-sm" data-action="PUBLISHED" data-load-to='#knowledge-knowledge-entry' data-href="{{trans_url('admin/knowledge/knowledge/status/'. $knowledge->getRouteKey())}}" data-value="No" data-datatable='#knowledge-knowledge-list'><i class="fa fa-times-circle"></i> Suspend</button>
        @else
            <button type="button" class="btn btn-success btn-sm" data-action="PUBLISHED" data-load-to='#knowledge-knowledge-entry' data-href="{{trans_url('admin/knowledge/knowledge/status/'. $knowledge->getRouteKey())}}" data-value="Yes" data-datatable='#knowledge-knowledge-list'><i class="fa fa-check"></i> Published</button>
        @endif
        <button type="button" class="btn btn-primary btn-sm" data-action="EDIT" data-load-to='#knowledge-knowledge-entry' data-href='{{ trans_url('/admin/knowledge/knowledge') }}/{{$knowledge->getRouteKey()}}/edit'><i class="fa fa-pencil-square"></i> Edit</button>
        <button type="button" class="btn btn-danger btn-sm" data-action="DELETE" data-load-to='#knowledge-knowledge-entry' data-datatable='#knowledge-knowledge-list' data-href='{{ trans_url('/admin/knowledge/knowledge') }}/{{$knowledge->getRouteKey()}}' >
        <i class="fa fa-times-circle"></i> Delete
        </button>
        @endif
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
</div>
<div class="box-body" >
    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#info" data-toggle="tab">Knowledge</a></li>
            <li ><a href="#image" data-toggle="tab">Image</a></li>
        </ul>
        {!!Form::vertical_open()
        ->id('knowledge-knowledge-show')
        ->method('POST')
        ->files('true')
        ->action(trans_url('admin/knowledge/knowledge'))!!}

                    @include('knowledge::admin.knowledge.partial.view')

        {!! Form::close() !!}
    </div>
</div>

