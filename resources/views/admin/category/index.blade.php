@extends('admin::curd.index')
@section('heading')
<i class="fa fa-file-text-o"></i> {!! trans('category.name') !!} <small> {!! trans('cms.manage') !!} {!! trans('category.names') !!}</small>
@stop

@section('title')
{!! trans('category.names') !!}
@stop

@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{!! trans_url('admin') !!}"><i class="fa fa-dashboard"></i> {!! trans('cms.dashboard') !!} </a></li>
    <li class="active">{!! trans('category.names') !!}</li>
</ol>
@stop

@section('entry')
<div class="box box-warning" id='knowledge-category-entry'>
</div>
@stop

@section('tools')
@stop

@section('content')
<table id="knowledge-category-list" class="table table-striped table-bordered data-table">
    <thead  class="list_head">
        <th>{!! trans('category.label.name')!!}</th>
        <th>{!! trans('category.label.status')!!}</th>
    </thead>
    <thead  class="search_bar">
        <th>{!! Form::text('search[name]')->raw()!!}</th>
        <th>{!! Form::text('search[status]')->raw()!!}</th>
    </thead>
</table>
@stop

@section('script')
<script type="text/javascript">

var oTable;
$(document).ready(function(){
    app.load('#knowledge-category-entry', '{!!trans_url('admin/knowledge/category/0')!!}');
    oTable = $('#knowledge-category-list').dataTable( {
        "bProcessing": true,
        "sDom": 'R<>rt<ilp><"clear">',
        "bServerSide": true,
        "sAjaxSource": '{!! trans_url('/admin/knowledge/category') !!}',
        "fnServerData" : function ( sSource, aoData, fnCallback ) {

            $('#knowledge-category-list .search_bar input, #knowledge-category-list .search_bar select').each(
                function(){
                    aoData.push( { 'name' : $(this).attr('name'), 'value' : $(this).val() } );
                }
            );
            app.dataTable(aoData);
            $.ajax({
                'dataType'  : 'json',
                'data'      : aoData,
                'type'      : 'GET',
                'url'       : sSource,
                'success'   : fnCallback
            });
        },

        "columns": [
            {data :'name'},
                    {data :'status'},
        ],
        "pageLength": 25
    });

    $('#knowledge-category-list tbody').on( 'click', 'tr', function () {

        oTable.$('tr.selected').removeClass('selected');
        $(this).addClass('selected');

        var d = $('#knowledge-category-list').DataTable().row( this ).data();

        $('#knowledge-category-entry').load('{!!trans_url('admin/knowledge/category')!!}' + '/' + d.id);
    });

    $("#knowledge-category-list .search_bar input, #knowledge-category-list .search_bar select").on('keyup select', function (e) {
        e.preventDefault();
        console.log(e.keyCode);
        if (e.keyCode == 13 || e.keyCode == 9) {
            oTable.api().draw();
        }
    });
});
</script>
@stop

@section('style')
@stop

