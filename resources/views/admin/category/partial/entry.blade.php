<div class='col-md-6 col-sm-6'>
    {!! Form::text('name')
    ->required()
    -> label(trans('category.label.name'))
    -> placeholder(trans('category.placeholder.name'))!!}
</div>

<div class='col-md-6 col-sm-6'>
    {!! Form::select('status')
    -> options(trans('category.status_options'))
    -> label(trans('category.label.status'))
    -> placeholder(trans('category.placeholder.status'))!!}
</div>
