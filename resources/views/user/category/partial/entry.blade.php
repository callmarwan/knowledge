				{!! Form::hidden('upload_folder')!!}
				<div class='col-md-4 col-sm-6'>
                       {!! Form::text('name')
                       -> label(trans('category.label.name'))
                       -> placeholder(trans('category.placeholder.name'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('status')
                       -> label(trans('category.label.status'))
                       -> placeholder(trans('category.placeholder.status'))!!}
                </div>

{!!   Form::actions()
->large_primary_submit('Submit')
->large_inverse_reset('Reset')
!!}
