
<div class="row">
{!! Form::hidden('upload_folder')!!}
           <div class='col-md-4 col-sm-6'>
                   {!! Form::text('title')
                   -> required()
                   -> label(trans('knowledge.label.title'))
                   -> placeholder(trans('knowledge.placeholder.title'))!!}
           </div>

            <div class='col-md-4 col-sm-6'>
                   {!! Form::select('category_id')
                   -> required()
                   -> options(Knowledge::getCategory())
                   -> label(trans('knowledge.label.category_id'))
                   -> placeholder(trans('knowledge.placeholder.category_id'))!!}
            </div>
            <div class='col-md-4 col-sm-6'>
                   {!! Form::select('status')
                   -> options(trans('knowledge.status_options'))
                   -> label(trans('knowledge.label.status'))
                   -> placeholder(trans('knowledge.placeholder.status'))!!}
            </div>

            <div class='col-md-12 col-sm-12'>
                   {!! Form::textArea('details')
                   -> addClass('html-editor')
                   -> label(trans('knowledge.label.details'))
                   -> placeholder(trans('knowledge.placeholder.details'))!!}
            </div>

            <div class='col-md-12 col-sm-12'>
            Image:
                {!!@$knowledge->fileUpload('image')!!}
                {!!@$knowledge->fileEdit('image')!!}
            </div>

            <div class='col-md-12 col-sm-12'>
            Images:
                {!!@$knowledge->fileUpload('images')!!}
                {!!@$knowledge->fileEdit('images')!!}
            </div>


</div>
