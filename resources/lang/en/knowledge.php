<?php

return [

    /**
     * Singlular and plural name of the module
     */
    'name' => 'Knowledge',
    'names' => 'Knowledges',
    'user_name' => 'My <span>Knowledge</span>',
    'user_names' => 'My <span>Knowledges</span>',
    'create' => 'Create My Knowledge',
    'edit' => 'Update My Knowledge',

    /**
     * Options for select/radio/check.
     */
    'status_options' => ['hide' => 'Hide', 'show' => 'Show'],

    /**
     * Placeholder for inputs
     */
    'placeholder' => [
        'title' => 'Please enter title',
        'details' => 'Please enter details',
        'category_id' => 'Please select category',
        'image' => 'Please enter image',
        'images' => 'Please enter images',
        'status' => 'Please select status',
    ],

    /**
     * Labels for inputs.
     */
    'label' => [
        'title' => 'Title',
        'details' => 'Details',
        'category_id' => 'Category',
        'image' => 'Image',
        'images' => 'Images',
        'status' => 'Status',
        'status' => 'Status',
        'created_at' => 'Created at',
        'updated_at' => 'Updated at',
    ],

    /**
     * Tab labels
     */
    'tab' => [
        'name' => 'Name',
    ],

    /**
     * Texts  for the module
     */
    'text' => [
        'preview' => 'Click on the below list for preview',
    ],
];
